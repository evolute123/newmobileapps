package com.eps.epslomerchantapp.main;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.bluetooth.BluetoothComm;
import com.eps.epslomerchantapp.support.EPSTransaction;
import com.leopard.api.MagCard;
import com.leopard.api.Setup;
import com.leopard.api.Printer;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.util.Log;

public class GlobalPool extends Application{
	/**Bluetooth communication connection object*/
	public BluetoothComm mBTcomm = null;
	public static boolean btconnect = false;
	private String TAG="GlobalPool";
	Setup setup;
	boolean bActivate=false;
	Printer printer;
	MagCard mag;
	OutputStream outst=null;
	InputStream inst=null;
	public void onCreate(Context context){
		Log.e(TAG, "In onCreate of Globalpool");
		if (isConnect()==true) {
			try {
				setup = new Setup();
				bActivate = setup.blActivateLibrary(context,null);
				if (bActivate == true) {
					Log.d(TAG,"Library Activated......");
				} else if (bActivate == false) {
					Log.d(TAG,"Library Not Activated...");
				}
			} catch (Exception e) {
				e.printStackTrace();
				Log.e(TAG, "Exception in activating library");
			}
		}
	}

	private boolean InstantiateBluetooth()
	{
		BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
		if(!mBtAdapter.isEnabled())
			mBtAdapter.enable();
		SharedPreferences pref =  getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);
		String mac = pref.getString("BT","");
			createConn(mac);
		return isConnect();
	}
	public boolean InstantiatePrinter(){
		try {
			outst = BluetoothComm.mosOut;
			inst = BluetoothComm.misIn;
//			if(setup.iIsDeviceConnected(outst,inst)==Setup.SUCCESS) {
				printer = new Printer(setup, outst, inst);
				Log.e(TAG, "Printer is instantiated");
				return true;
//			}
//			else
//			{
//				if(InstantiateBluetooth())
//				{
//					outst = BluetoothComm.mosOut;
//					inst = BluetoothComm.misIn;
//					printer = new Printer(setup, outst, inst);
//					Log.e(TAG, "Printer is instantiated");
//					return true;
//				}
//				else
//				{
//					return false;
//				}
//			}
		} catch (Exception e) {
			Log.e(TAG, "Printer is not Instantiated");
			return false;
		}
	}

	public boolean InstantiateMagcard(){
		try {
			outst = BluetoothComm.mosOut;
			inst = BluetoothComm.misIn;
//			if(setup.iIsDeviceConnected(outst,inst)==Setup.SUCCESS) {
				mag= new MagCard(setup, outst, inst);
				Log.e(TAG, "Printer is instantiated");
				return true;
//			}
//			else
//			{
//				if(InstantiateBluetooth())
//				{
//					outst = BluetoothComm.mosOut;
//					inst = BluetoothComm.misIn;
//					mag= new MagCard(setup, outst, inst);
//					Log.e(TAG, "Printer is instantiated");
//					return true;
//				}
//				else
//				{
//					return false;
//				}
//			}
		} catch (Exception e) {
			Log.e(TAG, "Printer is not Instantiated");
			return false;
		}
	}

	public String getHardwareSerialNumber()
	{
		if(InstantiatePrinter())
		{
			return printer.sGetSerialNumber();
		}
		else
			return "";
	}
    public int PrintTransaction(Context ctx,EPSTransaction trans, String printType) {
		if(!InstantiatePrinter())
		{
			return 1;
		}
        SharedPreferences pref = getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);
        int iRet;
        //iRet = printer.iFlushBuf();

		printer.iBmpPrint(ctx,R.drawable.spirituspaybmplogo);
		iRet = printer.iFlushBuf();
        //// Please NOTE all LARGE_xxx Fonts are 24 chars per line and all SMALL_xxx Fonts are 42 chars per line
        //////////								 12345678901234567890123x567890123456789012
		//Bank Name
        if (!trans.getBank().isEmpty() && !trans.getBank().equalsIgnoreCase("UNKNOWN")) {
            printc(BluetoothComm.Font.LARGE_BOLD, trans.getBank());//Consumer Bank Name
        }
        //Merchant Name
        if (!pref.getString("merchantName", "").isEmpty())
            printc(BluetoothComm.Font.LARGE_NORMAL, pref.getString("merchantName", ""));//Merchant Display Name
        //SALE
        printc(BluetoothComm.Font.LARGE_NORMAL, "SALE");
        //Date Time Line
        String sDt10 = "";
        String sTm08 = "";
		String tstamp = trans.getTransaction_timestamp("");
		if (!tstamp.isEmpty()) {
				sDt10 =trans.getTransaction_timestamp("dd-MM-yyyy");
				sTm08 = trans.getTransaction_timestamp("HH:mm:ss");
        }
        if (!sDt10.isEmpty())
            sDt10 = "DATE: " + sDt10;
        if (!sTm08.isEmpty())
            sTm08 = "TIME: " + sTm08;
        if (!sDt10.isEmpty())
            print(BluetoothComm.Font.SMALL_NORMAL, sDt10, sTm08);
        else if (sDt10.isEmpty() && !sTm08.isEmpty())
            print(BluetoothComm.Font.SMALL_NORMAL, sTm08);
        //MID and TID
        String mid = "";
        if (!pref.getString("merchantID", "").isEmpty())
            mid = "MID:" + pref.getString("merchantID", "");
        String tid = "";
        if (!pref.getString("tid", "").isEmpty())
            tid = "TID:" + pref.getString("tid", "");
        if (!mid.isEmpty())
            print(BluetoothComm.Font.SMALL_NORMAL, mid, tid);//Merchant ID and Terminal ID
        else if (mid.isEmpty() && !tid.isEmpty())
            print(BluetoothComm.Font.SMALL_NORMAL, tid);
        //INV NO and RRN
        String RRN = "";
        if (!trans.getReference_no().isEmpty()) {
            RRN = "RRN:" + trans.getReference_no();
        }
        String invnum = "";
        if (!trans.getInvoice_number().isEmpty())
            invnum = "INV NUM:" + trans.getInvoice_number();
        if (!invnum.isEmpty())
            print(BluetoothComm.Font.SMALL_NORMAL, invnum, RRN);
        else if (invnum.isEmpty() && !RRN.isEmpty())
            print(BluetoothComm.Font.SMALL_NORMAL, RRN);
        //TXN ID
        if (!trans.getUnique_txn_id().isEmpty())
            print(BluetoothComm.Font.SMALL_NORMAL, "TXN ID:", trans.getUnique_txn_id());
		String pgid = trans.getPGItem("id");
		if(!pgid.isEmpty())
			print(BluetoothComm.Font.SMALL_NORMAL, "PG ID:", pgid);
		String ccno = trans.getPGItem("card_no");
		if(!ccno.isEmpty())
			print(BluetoothComm.Font.SMALL_NORMAL, "CARD NUM:", ccno);
		String ctype = trans.getPGItem("card_type");
		if(!ctype.isEmpty())
			print(BluetoothComm.Font.SMALL_NORMAL, "CARD TYPE:", ctype);

		//BLANK LINE
        print(BluetoothComm.Font.SMALL_NORMAL, "                                          ");
        //TXN STATUS
        if (!trans.getTransaction_state().isEmpty())
            print(BluetoothComm.Font.LARGE_NORMAL, "TXN STATUS:", trans.getTransaction_state().toUpperCase());//or DECLINED
        //BLANK LINE
        print(BluetoothComm.Font.SMALL_NORMAL, "                                          ");
        //AMOUNT
        String amt = trans.getAmount();
		print(BluetoothComm.Font.LARGE_BOLD, "AMOUNT : RS",amt);
		//BLANK LINE
        print(BluetoothComm.Font.SMALL_NORMAL, "                                          ");
        //MESSAGE
        // No need to worry on formating the SMS for printing as it must be printed as it is...
        if (!trans.getTransaction_type().equalsIgnoreCase("cash") && trans.getTransaction_state().equalsIgnoreCase("approved")) {
            //print(BluetoothComm.Font.SMALL_NORMAL, trans.getTransaction_details());
            print(BluetoothComm.Font.SMALL_NORMAL, "                                          ");
           // print(BluetoothComm.Font.LARGE_NORMAL, "     PIN VERIFIED OK    ");
            printc(BluetoothComm.Font.LARGE_NORMAL, "SIGNATURE REQUIRED");
			print(BluetoothComm.Font.SMALL_NORMAL, "                                          ");
			print(BluetoothComm.Font.SMALL_NORMAL, "                                          ");
			print(BluetoothComm.Font.LARGE_NORMAL, "------------------------");
			print(BluetoothComm.Font.SMALL_NORMAL, "                                          ");
			String cname = trans.getPGItem("name_on_card");
			if(!cname.isEmpty())
				printc(BluetoothComm.Font.LARGE_NORMAL,cname.toUpperCase());
            printc(BluetoothComm.Font.SMALL_NORMAL, "I AGREE TO PAY THE ABOVE TOTAL AMOUNT ACCORDING TO CARD ISSUER AGREEMENT");
        } else if(trans.getTransaction_type().equalsIgnoreCase("card")) {

		}
		else {
            print(BluetoothComm.Font.LARGE_NORMAL, trans.getTransaction_details());
        }
        //THANKYOU
        print(BluetoothComm.Font.LARGE_NORMAL, "        THANK YOU       ");
        //PRINT TYPE
        printc(BluetoothComm.Font.LARGE_NORMAL, "*** " + printType + " COPY ***");
        //EMPTY LINES
        print(BluetoothComm.Font.LARGE_BOLD, "\n\n\n");

        iRet = printer.iStartPrinting(1);
        return iRet;
    }
	private void print(BluetoothComm.Font font,String s)
	{
		int limit=24;
		if(font == BluetoothComm.Font.SMALL_NORMAL)
			limit = 42;
		int index = 0;
        byte f = BluetoothComm.getByte(font);
		while (index < s.length()) {
            printer.iPrinterAddData(f,s.substring(index, Math.min(index + limit,s.length())));
			index += limit;
		}
	}
	private void print(BluetoothComm.Font font,String s1,String s2)
	{
		int limit=24;
		if(font == BluetoothComm.Font.SMALL_NORMAL)
			limit = 42;
		int totlen = s1.length() + s2.length();
		int space = limit-totlen;
		StringBuffer outputBuffer = new StringBuffer(space);
		for (int i = 0; i < space; i++){
			outputBuffer.append(" ");
		}
        byte f = BluetoothComm.getByte(font);
        printer.iPrinterAddData(f,s1+outputBuffer.toString()+s2);
	}

	private void printc(BluetoothComm.Font font,String s){
		int limit=24;
		if(font == BluetoothComm.Font.SMALL_NORMAL)
			limit = 42;
		int index = 0;
		int len = s.length();
        byte fb = BluetoothComm.getByte(font);
		if(len <= limit)
		{
			int space = Math.round((limit-len)/2);
			StringBuffer outputBuffer = new StringBuffer(space);
			for (int i = 0; i < space; i++){
				outputBuffer.append(" ");
			}
			String prntxt = outputBuffer.toString()+s+outputBuffer.toString();
            printer.iPrinterAddData(fb,prntxt);
		}
		else {
			String[] words = s.split(" ");
			ArrayList<String> lines = new ArrayList<String>();
			String line = "";
			for(String word:words)
			{
				String tmpline = line + " " + word;
				if(tmpline.length() > limit)
				{
					lines.add(line);
					line = word;
				}
				else
				{
					line = tmpline;
				}
			}
			lines.add(line);
			for(String l : lines)
			{
				int space = Math.round((limit-l.length())/2);
				StringBuffer outputBuffer = new StringBuffer(space);
				for (int i = 0; i < space; i++){
					outputBuffer.append(" ");
				}
				String prntxt = outputBuffer.toString()+l+outputBuffer.toString();
				printer.iPrinterAddData(fb,prntxt);
			}
		}
	}

	public int PrintOperation(){
		int iRetval=printer.iTestPrint();
		Log.e(TAG, "iRetval...."+iRetval);
		return iRetval;
	}
	public int MagOperation(){
		mag.vReadMagCardData(10000);
		int iRetval=mag.iGetReturnCode();
		Log.e(TAG, "iRetval...."+iRetval);
		return iRetval;
	}
	private static final int DEVICE_NOTCONNECTED = -100;
	public String checkiRetval(int iRetval){
		if (iRetval == DEVICE_NOTCONNECTED) {
			Log.e(TAG,"Device not connected");
			return "Device not connected";
		} else if (iRetval == Printer.PR_SUCCESS) {
			Log.e(TAG, "Print Success");
			return "Print Success";
		} else if (iRetval == Printer.PR_PLATEN_OPEN) {
			Log.e(TAG,"Platen open");
			return "Platen open";
		} else if (iRetval == Printer.PR_PAPER_OUT) {
			Log.e(TAG,"Paper out");
			return "Paper out";
		} else if (iRetval == Printer.PR_IMPROPER_VOLTAGE) {
			Log.e(TAG,"Printer at improper voltage");
			return "Printer at improper voltage";
		} else if (iRetval == Printer.PR_FAIL) {
			Log.e(TAG,"Printing failed");
			return "Printing failed";
		} else if (iRetval == Printer.PR_PARAM_ERROR) {
			Log.e(TAG,"parameter error");
			return "parameter error";
		}else if (iRetval == Printer.PR_NO_RESPONSE) {
			Log.e(TAG,"No response from Leopard device");
			return "No response from Leopard device";
		}else if (iRetval== Printer.PR_DEMO_VERSION) {
			Log.e(TAG,"Library in demo version");
			return "Library in demo version";
		}else if (iRetval==Printer.PR_INVALID_DEVICE_ID) {
			Log.e(TAG,"Connected  device is not authenticated");
			return "Connected  device is not authenticated";
		}else{
			Log.e(TAG,"Unknown Response from Device");
			return "Unknown Response from Device";
		}
	}
	public String checkiRetvalMag(int iRetval){
		if(iRetval==DEVICE_NOTCONNECTED){
			return "Device not connected";
		}else if (iRetval == MagCard.MAG_TRACK1_READERROR) {
			return "Track1 data read failed";
		} else if (iRetval == MagCard.MAG_TRACK2_READERROR) {
			return "Track2 data read failed" ;
		} else if ((iRetval == MagCard.MAG_SUCCESS)||(iRetval==MagCard.MAG_TRACK3_READERROR)){
			return "SUCCESS";
		} else if (iRetval == MagCard.MAG_FAIL) {
			return "MagCard Read Error";
		} else if (iRetval == MagCard.MAG_LRC_ERROR) {
			return "LRC Error";

		} else if (iRetval == MagCard.MAG_NO_DATA) {
			return "IMPROPER SWIPE";
		} else if (iRetval == MagCard.MAG_ILLEGAL_LIBRARY) {
			return "Illegal Library";
		} else if (iRetval == MagCard.MAG_DEMO_VERSION) {
			return "API not supported for demo version";
		} else if (iRetval == MagCard.MAG_TIME_OUT) {
			return "Swipe Card TimeOut";
		}else if (iRetval == MagCard.MAG_INACTIVE_PERIPHERAL) {
			return "Peripheral is inactive";
		}else if (iRetval == MagCard.MAG_PARAM_ERROR) {
			return"Passed incorrect parameter";
		}else if (iRetval== Printer.PR_DEMO_VERSION) {
			return "Library is in demo version";
		}else if (iRetval==Printer.PR_INVALID_DEVICE_ID) {
			return"Connected  device is not license authenticated.";
		}else if (iRetval==Printer.PR_ILLEGAL_LIBRARY) {
			return "Library not valid" ;
		}
		else{
			Log.e(TAG,"Unknown Response from Device");
			return "Unknown Response from Device";
		}

	}
	public boolean createConn(String sMac){
		Log.e("Connect","Create Connection");
		if (null == this.mBTcomm) {
			this.mBTcomm = new BluetoothComm(sMac);
			if (this.mBTcomm.createConn()){
				btconnect = true;
				return true;
			} else{
				this.mBTcomm = null;
				btconnect = false;
				return false;
			}
		} else {
			btconnect = false;
			this.closeConn();
		}
		return false;
	}

	/**
	 * Close and release the connection
	 * @return void
	 * */
	public void closeConn(){
		if(mag != null)
			mag = null;
		if (null != this.mBTcomm) {
			this.mBTcomm.closeConn();
			this.mBTcomm = null;
		}
	}
	public Boolean isConnect(){
		Log.d(TAG, "in Isconnect");
		if(this.mBTcomm.isConnect()==true){
			return true;
		}else{
			return false;
		}
	}



}
