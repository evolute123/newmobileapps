package com.eps.epslomerchantapp.main;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.bluetooth.BluetoothComm;
import com.eps.epslomerchantapp.payubiz.PayuPayments;
import com.eps.epslomerchantapp.support.DatabaseHandler;
import com.eps.epslomerchantapp.support.EPSConstant;
import com.eps.epslomerchantapp.support.EPSServerCaller;
import com.eps.epslomerchantapp.support.EPSTransaction;
import com.eps.epslomerchantapp.support.EPSWebURL;
import com.eps.epslomerchantapp.support.Utility;
import com.leopard.api.MagCard;
import com.leopard.api.Printer;
import com.leopard.api.Setup;
import com.payu.india.Extras.PayUChecksum;
import com.payu.india.Interfaces.PaymentRelatedDetailsListener;
import com.payu.india.Interfaces.ValueAddedServiceApiListener;
import com.payu.india.Model.MerchantWebService;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PayuResponse;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.Payu.PayuUtils;
import com.payu.india.PostParams.MerchantWebServicePostParams;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.india.Tasks.GetPaymentRelatedDetailsTask;
import com.payu.india.Tasks.ValueAddedServiceTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class CardPayment extends AppCompatActivity implements View.OnClickListener,PaymentRelatedDetailsListener, ValueAddedServiceApiListener {
    Button submit, cancel,reconnect;

    EditText cardNumberEditText,cardNameEditText,cvvEditText;
    Spinner expiryEditText,expiryEditText2;
    TextView amountTv,cardNameTv;
    String mcc,mexpmon,mexpyr,mcname,mcvv;
    private static Setup setup;
    private BluetoothComm btcomm;
    private Printer prn;
    private MagCard mag;
    String amount;
    SharedPreferences mPref;
    EPSTransaction mTrans;
    ReadMagcardAsyc magcard;
    Boolean bStarted = false;
    Boolean smsPermission = false;
    //PAYUBASEACTIVITY VARIABLES
    PayuUtils mPayuUtils;
    PayuHashes mPayuHashes;
    PayuResponse valueAddedResponse;
    PayuResponse mPayuResponse;
    ValueAddedServiceTask valueAddedServiceTask;
    Boolean isTransactionInProgress = false;

    //MAIN ACTIVITY VARIABLES
    //Intent intent;
    private PaymentParams mPaymentParams;
    private PayuConfig payuConfig;
    private PayUChecksum checksum;
    private PostData postData;
    private int storeOneClickHash;
    private PostData mPostData;
    private boolean viewPortWide = false;
    String[] months = {"MM","01","02","03","04","05","06","07","08","09","10","11","12"};
    int mThisYear,mThisMon;
    ArrayList<String> years = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment);
        mPref =  getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);


        cardNumberEditText = (EditText) findViewById(R.id.cardNumberEditText);
        cardNameEditText = (EditText) findViewById(R.id.cardNameEditText);
        cvvEditText = (EditText) findViewById(R.id.cvvEditText);
        cardNameTv = (TextView) findViewById(R.id.cardNameTv);

        amountTv = (TextView) findViewById(R.id.amountTv);
        amount = getIntent().getStringExtra("amount");
        amountTv.setText(String.format("Amount: Rs. %1$s",amount));

        expiryEditText = (Spinner) findViewById(R.id.expiryEditText);

        ArrayAdapter<String> monthAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, Arrays.asList(months));
        monthAdapter.setDropDownViewResource(R.layout.spinner_item);
        expiryEditText.setAdapter(monthAdapter);

        expiryEditText2 = (Spinner) findViewById(R.id.expiryEditText2);
        Calendar c = Calendar.getInstance();
        mThisYear = c.get(Calendar.YEAR);
        mThisMon = c.get(Calendar.MONTH);
        years.add("YYYY");
        for(int i=0;i<20;i++) {
            int y = mThisYear + i;
            years.add(""+y);
        }
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, years);
        yearAdapter.setDropDownViewResource(R.layout.spinner_item);
        expiryEditText2.setAdapter(yearAdapter);
        expiryEditText2.setPrompt("YYYY");

        submit = (Button) findViewById(R.id.submitButton);
        cancel = (Button) findViewById(R.id.cancelButtonInCardDetails);
        reconnect = (Button) findViewById(R.id.reconnectBTButton);
        submit.setOnClickListener(this);
        cancel.setOnClickListener(this);
        reconnect.setOnClickListener(this);

        //INITIAL DATA FOR TESTING
        mcc="4012001038443335";//4012001038443335(Axis) 5123456789012346 (hdfc)
        mexpmon="05";
        mexpyr="2017";
        mcname="Some Name";
        mcvv="123";

        //cardNameEditText.setText(mcname);
        //List<String> mnts = Arrays.asList(months);
        //expiryEditText.setSelection(mnts.indexOf(mexpmon));
        //expiryEditText2.setSelection(years.indexOf(mexpyr));
        //cvvEditText.setText(mcvv);
        cardNumberEditText.addTextChangedListener(new TextWatcher() {
            private static final int TOTAL_SYMBOLS = 19; // size of pattern 00:00:00:00:00:00
            private static final int TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
            private static final int DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
            private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
            private static final char DIVIDER = '-';
            private Character[] chars = {'0','1','2','3','4','5','6','7','8','9'};
            private List<Character> clist = Arrays.asList(chars);
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // noop
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // noop
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
                    s.replace(0, s.length(), buildCorrecntString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
                }
            }

            private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
                boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
                for (int i = 0; i < s.length(); i++) { // chech that every element is right
                    if (i > 0 && (i + 1) % dividerModulo == 0) {
                        isCorrect &= divider == s.charAt(i);
                    } else {
                        isCorrect &= clist.contains(s.charAt(i));
                    }
                }
                return isCorrect;
            }

            private String buildCorrecntString(char[] digits, int dividerPosition, char divider) {
                final StringBuilder formatted = new StringBuilder();

                for (int i = 0; i < digits.length; i++) {
                    if (digits[i] != 0) {
                        formatted.append(digits[i]);
                        if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
                            formatted.append(divider);
                        }
                    }
                }

                return formatted.toString();
            }

            private char[] getDigitArray(final Editable s, final int size) {
                char[] digits = new char[size];
                int index = 0;

                for (int i = 0; i < s.length() && index < size; i++) {
                    char current = s.charAt(i);
                    if (clist.contains(current)) {
                        digits[index] = current;
                        index++;
                    }
                }
                return digits;
            }
        });
        //String card = mcc.substring(0,4)+ "-" + mcc.substring(4,8)+ "-" + mcc.substring(8,12)+ "-" + mcc.substring(12,16);
        //cardNumberEditText.setText(card);
    }
    @Override
    public void onClick(View view) {
        if (view == reconnect) {
            magcard = new ReadMagcardAsyc();
            magcard.execute();
        } else if (view == cancel) {
            exit();
        } else if (view == submit) {
            //mkey = etMKey.getText().toString();
            String cc = cardNumberEditText.getText().toString();
            String cname = cardNameEditText.getText().toString();
            String expmon = expiryEditText.getSelectedItem().toString();
            String expyr = expiryEditText2.getSelectedItem().toString();
            String cvv = cvvEditText.getText().toString();
            int iexpmon = 0;
            try {
                iexpmon = Integer.parseInt(expmon);
            } catch (Exception ex) {

            }
            if (cc.isEmpty() || expmon.isEmpty() || expyr.isEmpty() || cvv.isEmpty() || expmon.equalsIgnoreCase("MM") || expyr.equalsIgnoreCase("YYYY")) {
                new AlertDialog.Builder(CardPayment.this)
                        .setTitle("Error")
                        .setMessage("All Items are mandatory.")
                        .setPositiveButton("OK", null)
                        .show();
            } else if (cc.length() < 19) {
                new AlertDialog.Builder(CardPayment.this)
                        .setTitle("Error")
                        .setMessage("Credit Card number should be 16 digit number only")
                        .setPositiveButton("OK", null)
                        .show();
            } else if (expyr.equalsIgnoreCase("" + mThisYear) && iexpmon > 0 && iexpmon <= mThisMon) {
                new AlertDialog.Builder(CardPayment.this)
                        .setTitle("Error")
                        .setMessage("This Credit Card seems to have expired. Please verify the expiry date")
                        .setPositiveButton("OK", null)
                        .show();
            } else {
                if (Utility.isNetWorkAvailable(CardPayment.this)) {
                    mcc=cc.replace("-","");
                    mexpmon=expmon;
                    mexpyr=expyr;
                    mcname=cname;
                    mcvv=cvv;
                    isTransactionInProgress = true;
                    mTrans = EPSTransaction.generateTransaction(amount, "CARD", "pending", mPref);
                    new ATCardTransaction(mTrans, mPref.getString("tid", "")).execute();
                } else {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    mTrans = EPSTransaction.generateTransaction(amount, "CASH", "approved", mPref);
                                    long rowid = saveTransactionToDB(mTrans);
                                    Intent in = new Intent(CardPayment.this, EndTransaction.class);
                                    in.putExtra("transactionid", rowid);
                                    startActivity(in);
                                    finish();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    Utility.alert("Transaction Failed", "Cannot Execute a Card Transaction", CardPayment.this);
                                    break;
                            }
                        }
                    };
                    AlertDialog.Builder builder = new AlertDialog.Builder(CardPayment.this);
                    builder.setTitle("Transaction Error").setMessage("Cannot create a card transaction. Internet Failed. Would you want to convert it into a Cash Transaction?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
            }
        }
    }
    @Override
    protected void onStart(){
        super.onStart();
        if(!bStarted) {
            magcard = new ReadMagcardAsyc();
            magcard.execute();
            bStarted = true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Get Result from USSD
        String message = "";
        isTransactionInProgress = false;
        if(data!=null){
            String merchantdata= data.getStringExtra("result");
            String payudata = data.getStringExtra("payu_response");
            String respStatus = "declined";
            String refno = "";
            String bankcode = "";
            try {
                JSONObject payuObj = new JSONObject(payudata);
                String status = payuObj.getString("Error_Message");
                if(status.equalsIgnoreCase("No Error"))
                    respStatus = "approved";
                if(payuObj.has("bank_ref_no"))
                    refno = payuObj.getString("bank_ref_no");
                if(payuObj.has("issuing_bank"))
                    bankcode = payuObj.getString("issuing_bank");
            }catch(Exception ex) {
                ex.printStackTrace();

            }
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String paymentdate = format.format(date);
            mTrans.setPayment_timestamp(paymentdate);
            mTrans.setTransaction_details(payudata);
            mTrans.setFull_response("Payu's Data : " + payudata + "\n\n\n Merchant's Data: " +merchantdata );
            mTrans.setReference_no(refno);
            mTrans.setBank(bankcode);
            mTrans.setTransaction_state(respStatus);
        } else {
            mTrans.setTransaction_state("declined");
        }
        new ATUpdateTransaction(mPref.getString("tid","")).execute();

    }

    @Override
    public void onBackPressed() {
        if(!isTransactionInProgress)
            exit();
    }
    private void exit() {
        DialogInterface.OnClickListener dclCancel = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        //overridePendingTransition(R.anim.right_in, R.anim.right_out);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(CardPayment.this);
        builder.setMessage("Are you sure you want to cancel this transaction?").setPositiveButton("Yes", dclCancel)
                .setNegativeButton("No", dclCancel).show();
    }
    private long saveTransactionToDB(EPSTransaction trans)
    {
        DatabaseHandler dh = new DatabaseHandler(getApplicationContext());
        return dh.createTransactions(trans);
    }

    @Override
    public void onPaymentRelatedDetailsResponse(PayuResponse payuResponse) {

    }

    @Override
    public void onValueAddedServiceApiResponse(PayuResponse payuResponse) {

    }

    private class ATCardTransaction extends AsyncTask<String, String, String> {

        private ProgressDialog mpd = null;
        private EPSTransaction mTrans;
        private String mTID = "";
        public ATCardTransaction(EPSTransaction trans,String tid)
        {
            mTrans=trans;
            mTID = tid;
        }
        @Override
        public void onPreExecute() {
            this.mpd = new ProgressDialog(CardPayment.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.setMessage("Updating Server. Please wait...");
            this.mpd.show();
        }

        @Override
        protected void onProgressUpdate(String... progUpdate) {
            super.onProgressUpdate(progUpdate);
            this.mpd.setMessage(progUpdate[0]);
        }

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Updating Server. Please wait...");
            String serverStatus = "";
            if (Utility.isNetWorkAvailable(CardPayment.this)) {
                try {
                    HashMap<String, String> headers = new HashMap<>();
                    HashMap<String, String> reqparams = new HashMap<>();
                    headers.put("User-Agent", EPSWebURL.LO_USER_AGENT);
                    headers.put("TerminalID", mTID);
                    reqparams.put("amount",mTrans.getAmount());
                    reqparams.put("transactionDate",mTrans.getTransaction_timestamp(""));
                    reqparams.put("invoiceNumber",mTrans.getInvoice_number());
                    reqparams.put("txnId",mTrans.getUnique_txn_id());
                    EPSServerCaller api = new EPSServerCaller(getAssets());
                    String result = api.doPost(EPSWebURL.CARD_TRANSACTION,reqparams, headers);
                    int responseCode = api.getLastResponseCode();
                    //GET RESPONSE FROM SERVER
                    JSONObject json = new JSONObject(result);
                    if (responseCode == 200) {
                        serverStatus = json.optString("status");
                        String code = json.optString("code");
                        //String serverMessage = json.optString("message");
                        if (serverStatus.equalsIgnoreCase(EPSConstant.SERVER_SUCCESS)) {
                            JSONObject trans = json.getJSONObject("transaction");
                            mTrans.setTransaction_id(trans.optString("id"));
                            mTrans.setInvoice_number(trans.optString("invoiceNumber"));
                            mTrans.setAmount(trans.optString("amount"));
                            mTrans.setTransaction_type(trans.optString("transactionType"));
                            mTrans.setTransaction_timestamp(trans.optString("transactionTimestamp"));
                            mTrans.setPayment_timestamp(trans.optString("paymentTimestamp"));
                            mTrans.setTransaction_details(trans.optString("transactionDetails"));
                            mTrans.setFull_response(trans.optString("transactionDetails2"));
                            mTrans.setUnique_txn_id(trans.optString("txnId"));
                            mTrans.setReference_no(trans.optString("refNo"));
                            mTrans.setBank(trans.optString("bankCode"));
                            mTrans.setTransaction_state(trans.optString("transactionStatus"));
                            mTrans.setSync_state("SUCCESS");
                            JSONObject d = json.getJSONObject("device");
                            mTrans.setPg_key(d.optString("pgKey"));
                        } else if (code.equalsIgnoreCase(EPSConstant.DEVICE_INVALID)) {
                            serverStatus = EPSConstant.DEVICE_INVALID;
                        } else if (code.isEmpty()) {
                            serverStatus = EPSConstant.SERVER_UNKNOWN;
                        } else {
                            serverStatus = code;
                        }
                    }
                    else
                    {
                        serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
                    }
                } catch (Exception e) {
                    serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
                }
            } else {
                serverStatus = EPSConstant.SERVER_INTERNET_FAILED;
            }
            publishProgress("Device Validation Complete");

            //RETURN STATUS FOR ERROR HANDLING
            if (serverStatus.equalsIgnoreCase(EPSConstant.SERVER_SUCCESS))
                //Transaction got created and completed. Next step is to execute payment
                return EPSConstant.VALIDATION_COMPLETE;
            else
                return serverStatus;
        }

        @Override
        public void onPostExecute(String result) {

            //long rowid = saveTransactionToDB(mTrans);
            if (EPSConstant.VALIDATION_COMPLETE.equalsIgnoreCase(result)) {
                navigateToBaseActivity();
            }
            else if (EPSConstant.VALIDATION_COMPLETE.equalsIgnoreCase(result)) {
                SharedPreferences.Editor edit = mPref.edit();
                edit.putString("deviceStatus","inactive");
                edit.apply();
                startActivity(new Intent(CardPayment.this, DeviceDetails.class));
                overridePendingTransition( R.anim.left_in, R.anim.left_out);
                finish();
            } else if (result.equalsIgnoreCase(EPSConstant.DEVICE_INVALID)) {
                Utility.alert("Invalid Device","This device is not active to execute Transactions. Please contact Support.",CardPayment.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INTERNET_FAILED)) {
                Utility.alert("Network Error","Cannot connect to Server. No Internet found",CardPayment.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_CONNECTION_FAILED)) {
                Utility.alert("Network Error","Server not responding. Cannot validate the device",CardPayment.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_TRANSACTION_FAILED)) {
                Utility.alert("Validation Error","Server could not save the Transaction. Please try again",CardPayment.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INVALID_REQUEST)) {
                Utility.alert("Validation Error","Device Validation Failed. Invalid Request",CardPayment.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_AUTH_FAILED)) {
                Utility.alert("Validation Error","Device Validation Failed. Authentication Error",CardPayment.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_FORMAT_ERROR)) {
                Utility.alert("Validation Error","Device Validation Failed. Incorrect Data Sent",CardPayment.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_UNKNOWN)) {
                Utility.alert("Validation Error","Device Validation Failed. Unknown Error",CardPayment.this);
            } else {
                Utility.alert("Validation Error", "Device Validation Failed. Unknown Error", CardPayment.this);
            }
            if (this.mpd.isShowing())
                this.mpd.dismiss();
        }
    }

    private void navigateToBaseActivity() {
        mPaymentParams = new PaymentParams();
        payuConfig = new PayuConfig();
        mPaymentParams.setKey(mTrans.getPg_key()); //gtKFFx
        mPaymentParams.setAmount(mTrans.getAmount());
        mPaymentParams.setProductInfo("Leopard Transaction");
        mPaymentParams.setFirstName("");
        mPaymentParams.setEmail("");
        mPaymentParams.setTxnId(mTrans.getUnique_txn_id());
        mPaymentParams.setSurl(EPSWebURL.PG_SUCCESS);
        mPaymentParams.setFurl(EPSWebURL.PG_FAILURE);
        mPaymentParams.setUdf1("");
        mPaymentParams.setUdf2("");
        mPaymentParams.setUdf3("");
        mPaymentParams.setUdf4("");
        mPaymentParams.setUdf5("");
        mPaymentParams.setUserCredentials("");
        //mPaymentParams.setOfferKey(inputData);
        //intent.putExtra(PayuConstants.SALT, "eCwWELxi");
        //salt = "eCwWELxi";
        payuConfig.setEnvironment(PayuConstants.MOBILE_STAGING_ENV);
        storeOneClickHash = 0;
        smsPermission = Boolean.parseBoolean("false");

        StringBuffer postParamsBuffer = new StringBuffer();
        postParamsBuffer.append(concatParams(PayuConstants.KEY, mPaymentParams.getKey()));
        postParamsBuffer.append(concatParams(PayuConstants.AMOUNT, mPaymentParams.getAmount()));
        postParamsBuffer.append(concatParams(PayuConstants.TXNID, mPaymentParams.getTxnId()));
        postParamsBuffer.append(concatParams(PayuConstants.EMAIL, null == mPaymentParams.getEmail() ? "" : mPaymentParams.getEmail()));
        postParamsBuffer.append(concatParams(PayuConstants.PRODUCT_INFO, mPaymentParams.getProductInfo()));
        postParamsBuffer.append(concatParams(PayuConstants.FIRST_NAME, null == mPaymentParams.getFirstName() ? "" : mPaymentParams.getFirstName()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF1, mPaymentParams.getUdf1() == null ? "" : mPaymentParams.getUdf1()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF2, mPaymentParams.getUdf2() == null ? "" : mPaymentParams.getUdf2()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF3, mPaymentParams.getUdf3() == null ? "" : mPaymentParams.getUdf3()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF4, mPaymentParams.getUdf4() == null ? "" : mPaymentParams.getUdf4()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF5, mPaymentParams.getUdf5() == null ? "" : mPaymentParams.getUdf5()));
        postParamsBuffer.append(concatParams(PayuConstants.USER_CREDENTIALS, mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials()));

        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();
        // make api call
        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
        getHashesFromServerTask.execute(postParams);
    }

    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }

    class GetHashesFromServerTask extends AsyncTask<String, String, PayuHashes> {
        private ProgressDialog mpd = null;

        @Override
        public void onPreExecute() {
            this.mpd = new ProgressDialog(CardPayment.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.setMessage("Updating Payment Gateway Parameters");
            this.mpd.show();
        }
        @Override
        protected PayuHashes doInBackground(String... postParams) {
            PayuHashes payuHashes = new PayuHashes();
            try {

                // get the payuConfig first
                String postParam = postParams[0];
                EPSServerCaller api = new EPSServerCaller(getAssets());
                String result = api.doPost(EPSWebURL.GEN_HASH,postParam);
                int responseCode = api.getLastResponseCode();
                //GET RESPONSE FROM SERVER
                JSONObject response = new JSONObject(result);
                if (responseCode == 200) {

//                URL url = new URL(EPSWebURL.GEN_HASH);
//                byte[] postParamsByte = postParam.getBytes("UTF-8");
//
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setRequestMethod("POST");
//                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
//                conn.setDoOutput(true);
//                conn.getOutputStream().write(postParamsByte);
//
//                InputStream responseInputStream = conn.getInputStream();
//                StringBuffer responseStringBuffer = new StringBuffer();
//                byte[] byteContainer = new byte[1024];
//                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
//                    responseStringBuffer.append(new String(byteContainer, 0, i));
//                }
//
//                JSONObject response = new JSONObject(responseStringBuffer.toString());


                    Iterator<String> payuHashIterator = response.keys();
                    while (payuHashIterator.hasNext()) {
                        String key = payuHashIterator.next();
                        switch (key) {
                            case "payment_hash":
                                payuHashes.setPaymentHash(response.getString(key));
                                break;
                            case "get_merchant_ibibo_codes_hash": //
                                payuHashes.setMerchantIbiboCodesHash(response.getString(key));
                                break;
                            case "vas_for_mobile_sdk_hash":
                                payuHashes.setVasForMobileSdkHash(response.getString(key));
                                break;
                            case "payment_related_details_for_mobile_sdk_hash":
                                payuHashes.setPaymentRelatedDetailsForMobileSdkHash(response.getString(key));
                                break;
                            case "delete_user_card_hash":
                                payuHashes.setDeleteCardHash(response.getString(key));
                                break;
                            case "get_user_cards_hash":
                                payuHashes.setStoredCardsHash(response.getString(key));
                                break;
                            case "edit_user_card_hash":
                                payuHashes.setEditCardHash(response.getString(key));
                                break;
                            case "save_user_card_hash":
                                payuHashes.setSaveCardHash(response.getString(key));
                                break;
                            case "check_offer_status_hash":
                                payuHashes.setCheckOfferStatusHash(response.getString(key));
                                break;
                            case "check_isDomestic_hash":
                                payuHashes.setCheckIsDomesticHash(response.getString(key));
                                break;
                            default:
                                break;
                        }
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }catch(Exception ex) {
                ex.printStackTrace();
            }
            return payuHashes;
        }

        @Override
        protected void onPostExecute(PayuHashes payuHashes) {
            super.onPostExecute(payuHashes);
            mPayuHashes = payuHashes;

            //PAYUBASEACTIVITY CODE BASE
            mPayuUtils = new PayuUtils();

            MerchantWebService merchantWebService = new MerchantWebService();
            merchantWebService.setKey(mPaymentParams.getKey());
            merchantWebService.setCommand(PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK);
            merchantWebService.setVar1("default");
            merchantWebService.setHash(mPayuHashes.getPaymentRelatedDetailsForMobileSdkHash());

            // fetching for the first time.
            PostData postData = new MerchantWebServicePostParams(merchantWebService).getMerchantWebServicePostParams();
            if (postData.getCode() == PayuErrors.NO_ERROR) {
                // ok we got the post params, let make an api call to payu to fetch the payment related details
                payuConfig.setData(postData.getResult());
                GetPaymentRelatedDetailsTask paymentRelatedDetailsForMobileSdkTask = new GetPaymentRelatedDetailsTask(CardPayment.this);
                paymentRelatedDetailsForMobileSdkTask.execute(payuConfig);
                makePaymentByCreditCard();
            }
            if (this.mpd.isShowing())
                this.mpd.dismiss();
        }

        private void makePaymentByCreditCard() {

            mPostData = null;

            mPaymentParams.setHash(mPayuHashes.getPaymentHash());

            mPaymentParams.setStoreCard(0);
            mPaymentParams.setEnableOneClickPayment(0);
            // lets try to get the post params


            mPaymentParams.setCardNumber(mcc);
            mPaymentParams.setNameOnCard(mcname);
            mPaymentParams.setExpiryMonth(mexpmon);
            mPaymentParams.setExpiryYear(mexpyr);
            mPaymentParams.setCvv(mcvv);
            Payu.setInstance(CardPayment.this);
            try {
                mPostData = new PaymentPostParams(mPaymentParams, PayuConstants.CC).getPaymentPostParams();
            } catch (Exception e) {
                e.printStackTrace();
            }

            payuConfig.setData(mPostData.getResult());

            Intent intent = new Intent(CardPayment.this, PayuPayments.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            intent.putExtra(PayuConstants.STORE_ONE_CLICK_HASH, storeOneClickHash);
            intent.putExtra(PayuConstants.SMS_PERMISSION, smsPermission);
            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        }


    }


    private class ReadMagcardAsyc extends AsyncTask<Integer, Integer, String> {
        String sMagStatus = null;
        private ProgressDialog mpd = null;
        MagCard mag;
        @Override
        protected void onPreExecute() {
            cardNameEditText.setVisibility(View.VISIBLE);
            cardNameTv.setVisibility(View.VISIBLE);
            this.mpd = new ProgressDialog(CardPayment.this);
            this.mpd.setMessage("Swipe Card or Select Manual Entry");
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.setButton(DialogInterface.BUTTON_POSITIVE,"Manual Entry",new Dialog.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    cardNumberEditText.requestFocus();
                    cardNameEditText.setVisibility(View.INVISIBLE);
                    cardNameTv.setVisibility(View.INVISIBLE);
                    mag = null;
                }
            });
            this.mpd.show();
        }

        @Override
        protected String doInBackground(Integer... arg0) {
            try {
                if (ConnectActivity.mgp.isConnect()) {
                    ConnectActivity.mgp.InstantiateMagcard();
                    mag = ConnectActivity.mgp.mag;
                    mag.vReadMagCardData(30000);
                    int iretval = ConnectActivity.mgp.mag.iGetReturnCode();
                    sMagStatus = ConnectActivity.mgp.checkiRetvalMag(iretval);
                    if (!sMagStatus.equalsIgnoreCase("Success")) {
                        return sMagStatus;
                    }
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return sMagStatus;
        }

        @Override
        protected void onPostExecute(String result) {
            if (this.mpd.isShowing())
                this.mpd.dismiss();
            if(mag !=null) {
                if (result.equalsIgnoreCase("Success")) {
                    String track1 = mag.sGetTrack1Data();
                    String track2 = mag.sGetTrack2Data();
                    String track3 = mag.sGetTrack3Data();
                    //Toast.makeText(CardPayment.this,track1+track2+track3,Toast.LENGTH_LONG).show();
                    //Utility.alert("Card Details", "Details of Card are \nTrack1: "+track1+"\nTrack2: "+track2+"\nTrack3: "+track3, CardPayment.this);
                    processCardDetails(track1, track2, track3);
                } else
                    Utility.alert("Card Failed", "Could not read the card", CardPayment.this);
                mag = null;
            }
        }

        private void processCardDetails(String t1,String t2, String t3)
        {
            String[] tele;
            String cno="";
            String cname = "";
            String cmon = "";
            String cyr = "";
            if(t1 != null)
            {
                tele = t1.split("\\^");
                String t1cno = tele[0];
                t1cno = t1cno.replaceAll("[^\\d]", "");
                cno = t1cno.trim();
                cname = tele[1].replaceAll("[^a-zA-Z0-9]", "");
                cname = cname.trim();
                String t1cdt = tele[2];
                t1cdt = t1cdt.replaceAll("[^\\d]", "");
                t1cdt = t1cdt.substring(0,4);
                cyr = t1cdt.substring(0,2);
                cmon = t1cdt.substring(2,4);
                cyr = "20"+cyr;
            }
            else if(t2 != null)
            {
                tele = t2.split("\\=");
                String t1cno = tele[0];
                t1cno = t1cno.replaceAll("[^\\d]", "");
                cno = t1cno.trim();
                String t1cdt = tele[1];
                t1cdt = t1cdt.replaceAll("[^\\d]", "");
                t1cdt = t1cdt.substring(0,4);
                cyr = t1cdt.substring(0,2);
                cmon = t1cdt.substring(2,4);
                cyr = "20"+cyr;
            }
            if(!cno.isEmpty()) {
                String card = cno.substring(0, 4) + "-" + cno.substring(4, 8) + "-" + cno.substring(8, 12) + "-" + cno.substring(12, 16);
                cardNumberEditText.setText(card);
            }
            else
            {
                cardNumberEditText.setText("");
            }
            cardNameEditText.setText(cname);
            cvvEditText.setText("");
            List<String> mnts = Arrays.asList(months);
            expiryEditText.setSelection(mnts.indexOf(cmon));
            expiryEditText2.setSelection(years.indexOf(cyr));
            cvvEditText.requestFocus();

        }
    }

    public class ATUpdateTransaction extends AsyncTask<String, String, String> {
        String mCode;
        String mStatus;
        String mMessage;
        String mTransStatus;
        String mTransactionID;
        String mTransactionDetails;
        HashMap<String, String> reqparams = new HashMap<>();
        Boolean mForceDisconnect = false;
        private ProgressDialog mpd = null;
        String mTID;

        public ATUpdateTransaction(String tid) {
            mTID = tid;
            reqparams.put("transactionID", mTrans.getTransaction_id());
            reqparams.put("status", mTrans.getTransaction_state());
            reqparams.put("details", mTrans.getTransaction_details());
            reqparams.put("details2", mTrans.getFull_response());
            reqparams.put("paymentDate",mTrans.getPayment_timestamp(""));
            reqparams.put("forceUpdate", "false");
            reqparams.put("transactionType",mTrans.getTransaction_type());
            reqparams.put("refNo", mTrans.getReference_no());
            reqparams.put("bankCode", mTrans.getBank());

            this.mpd = new ProgressDialog(CardPayment.this);
            this.mpd.setMessage("Updating Transaction. Please Wait...");
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            mCode = "";
            mStatus = "FAILED";
            mMessage = "";
            try {
                if (Utility.isNetWorkAvailable(CardPayment.this)) {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("User-Agent", EPSWebURL.LO_USER_AGENT);
                    headers.put("TerminalID", mTID);
                    EPSServerCaller api = new EPSServerCaller(getAssets());
                    String result = api.doPost(EPSWebURL.UPDATE_TRANSACTION,reqparams, headers);
                    int responseCode = api.getLastResponseCode();
                    if (responseCode == 200) {
                        try {
                            JSONObject json = new JSONObject(result);
                            mStatus = json.optString("status");
                            mCode = json.optString("code");
                            mMessage = json.optString("message");
                        } catch (JSONException e) {
                            return "CODE_ERROR";
                        }
                    } else {
                        return EPSConstant.SERVER_CONNECTION_FAILED;
                    }
                } else {
                    return EPSConstant.SERVER_INTERNET_FAILED;
                }
            } catch (Exception ex) {
                return "UNKNOWN";
            }
            return mStatus;
        }

        @Override
        protected void onProgressUpdate(String... progUpdate) {
            super.onProgressUpdate(progUpdate);
            this.mpd.setMessage(progUpdate[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //mDialog_progress.dismiss();
            if (mCode.isEmpty())
                mCode = result;
            try {
                long rowid = saveTransactionToDB(mTrans);
                if (mStatus.equalsIgnoreCase("SUCCESS") || mForceDisconnect) {
                    Intent in = new Intent(CardPayment.this,EndTransaction.class);
                    in.putExtra("transactionid",rowid);
                    startActivity(in);
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                    setResult(312);
                    finish();

                } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INTERNET_FAILED)) {
                    alert(rowid,"Network Error","Cannot connect to Server. No Internet found",CardPayment.this);
                } else if (result.equalsIgnoreCase(EPSConstant.SERVER_CONNECTION_FAILED)) {
                    alert(rowid,"Network Error","Server not responding. Cannot validate the device",CardPayment.this);
                } else if (result.equalsIgnoreCase(EPSConstant.SERVER_TRANSACTION_FAILED)) {
                    alert(rowid,"Transaction Error","Server could not save the Transaction. Please try again",CardPayment.this);
                } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INVALID_REQUEST)) {
                    alert(rowid,"Transaction Error","Device Validation Failed. Invalid Request",CardPayment.this);
                } else if (result.equalsIgnoreCase(EPSConstant.SERVER_AUTH_FAILED)) {
                    alert(rowid,"Transaction Error","Device Validation Failed. Authentication Error",CardPayment.this);
                } else if (result.equalsIgnoreCase(EPSConstant.SERVER_FORMAT_ERROR)) {
                    alert(rowid,"Transaction Error","Device Validation Failed. Incorrect Data Sent",CardPayment.this);
                } else if (result.equalsIgnoreCase(EPSConstant.SERVER_UNKNOWN)) {
                    alert(rowid,"Transaction Error","Device Validation Failed. Unknown Error",CardPayment.this);
                } else {
                    alert(rowid,"Transaction Error", "Transaction Failed. Unknown Error", CardPayment.this);
                }
            } catch (Exception e) {
                Utility.alert("Transaction Error","Transaction Failed. Unknown Error",CardPayment.this);
            }

            if (this.mpd.isShowing())
                this.mpd.dismiss();
        }
        private void alert(long lrowid,String title,String message, Context ctx)
        {
            final long rowid = lrowid;
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent in = new Intent(CardPayment.this,EndTransaction.class);
                            in.putExtra("transactionid",rowid);
                            startActivity(in);
                            overridePendingTransition( R.anim.left_in, R.anim.left_out);
                            finish();
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder.setTitle(title).setMessage(message).setPositiveButton("OK", dialogClickListener).show();
        }
    }

}
