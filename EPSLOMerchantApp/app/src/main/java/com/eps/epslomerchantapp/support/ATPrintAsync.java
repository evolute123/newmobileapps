package com.eps.epslomerchantapp.support;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.main.ConnectActivity;
import com.eps.epslomerchantapp.main.GlobalPool;
import com.eps.epslomerchantapp.main.MainScreen;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by satish_kota on 01-09-2016.
 */
public class ATPrintAsync extends AsyncTask<String, String, String> {

    private ProgressDialog mpd = null;
    private EPSTransaction mTrans;
    private String mType = "";
    private Context mContext;
    GlobalPool mGP;

    public ATPrintAsync(GlobalPool mgp, Context ctx, EPSTransaction trans, String type) {
        mGP = mgp;
        mContext = ctx;
        mTrans = trans;
        mType = type;
    }


    @Override
    public void onPreExecute() {
        this.mpd = new ProgressDialog(mContext);
        this.mpd.setMessage("Printing in Progress. Please Wait");
        this.mpd.setCancelable(false);
        this.mpd.setCanceledOnTouchOutside(false);
        this.mpd.show();
    }

    @Override
    protected void onProgressUpdate(String... progUpdate) {
        super.onProgressUpdate(progUpdate);
        this.mpd.setMessage(progUpdate[0]);
    }

    @Override
    protected String doInBackground(String... params) {
        if (mType == "TESTPRINT") {
            try{
                if (ConnectActivity.mgp.isConnect()) {
                    ConnectActivity.mgp.InstantiatePrinter();
                    int iretval=ConnectActivity.mgp.PrintOperation();
                    String sPrintStatus=ConnectActivity.mgp.checkiRetval(iretval);
                }else{
                    Utility.alert("Printer Test Failed", "Error Occured while connecting to the printer",mContext);
                }

            }catch(Exception e)
            {
                Utility.alert("Printer Test Failed", "Error Occured while connecting to the printer",mContext);
            }
        } else
        {
            mGP.PrintTransaction(mContext, mTrans, mType);
        }

        return null;
    }

    @Override
    public void onPostExecute(String result) {
        if (this.mpd.isShowing())
            this.mpd.dismiss();
    }
}

