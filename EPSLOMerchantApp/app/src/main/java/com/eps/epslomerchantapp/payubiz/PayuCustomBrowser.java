package com.eps.epslomerchantapp.payubiz;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.payu.custombrowser.CBActivity;
import com.payu.custombrowser.PayUCustomBrowserCallback;
import com.payu.custombrowser.bean.CustomBrowserConfig;
import com.payu.custombrowser.bean.CustomBrowserData;

/**
 * Created by satish_kota on 13-09-2016.
 */
public class PayuCustomBrowser {
    public PayuCustomBrowser() {
    }

    public void addCustomBrowser(Activity activity, @NonNull CustomBrowserConfig cbCustomBrowserConfig, @NonNull PayUCustomBrowserCallback cbPayUCustomBrowserCallback) {
        CustomBrowserData.SINGLETON.setPayuCustomBrowserCallback(cbPayUCustomBrowserCallback);
        CustomBrowserData.SINGLETON.setPayuCustomBrowserConfig(cbCustomBrowserConfig);
        Intent intent = new Intent(activity, CBActivity.class);
        activity.startActivity(intent);
    }
}
