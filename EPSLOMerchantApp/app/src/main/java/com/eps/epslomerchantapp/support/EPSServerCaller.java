package com.eps.epslomerchantapp.support;

/**
 * Created by satish_kota on 14-05-2016.
 */
import android.content.res.AssetManager;
import android.os.Build;
import android.util.Log;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Scanner;


/**
 * client-side interface to the back-end application.
 */
public class EPSServerCaller {

    private SSLContext sslContext;
    private HostnameVerifier hostnameVerifier;
    private int lastResponseCode;

    public int getLastResponseCode() {
        return lastResponseCode;
    }

    public EPSServerCaller(AssetManager assets) {
        try {

            //CODE TO READ CERTIFICATE FILE
//            CertificateFactory cf = CertificateFactory.getInstance("X.509");
//            InputStream caInput = new BufferedInputStream(assets.open("spirituspay.crt"));
//            Certificate ca = cf.generateCertificate(caInput);
//            Log.i("CA Contents", ca.toString());
//            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//            caInput.close();

            //LOAD DEFAULT KEYSTORE
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);

            //LOAD KEYSTORE FROM THE FILE GENERATED USING THE CERFICIATE
//            InputStream keyStoreInputStream = assets.open("keystore.bks");
//            KeyStore keyStore             = KeyStore.getInstance("BKS");
//            keyStore.load(keyStoreInputStream, "Ev0luT3".toCharArray());

            //SET CERTIFICATE TO KEYSTORE - REQUIRED IF CERTIFICATE IS AVAILABLE
            //keyStore.setCertificateEntry("ca", ca);

            //SETUP TRUST MANAGER USING DEFAULT ALGORITHM
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            //SETUP TRUST MANAGER FROM KEYSTORE FILE
//            TrustManagerFactory tmf = TrustManagerFactory.getInstance("X509");
//            tmf.init(keyStore);
//
//            //CREATE SSLCONTEXT
            sslContext = SSLContext.getInstance("TLS");

            //INITIALIZE DEFAULT SSLCONTEXT
            sslContext.init(null, null, null);
            //INITIALIZE SSLCONTEXT USING GENERATED TRUST MANAGER
            //sslContext.init(null, tmf.getTrustManagers(), null);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String doGet(String url,HashMap<String,String> headers)  throws Exception {
        String result = null;

        HttpsURLConnection urlConnection = null;
        try {
            URL requestedUrl = new URL(url);
            //urlConnection = (HttpURLConnection) requestedUrl.openConnection();
            urlConnection = (HttpsURLConnection)requestedUrl.openConnection();
            //urlConnection.setHostnameVerifier(hostnameVerifier);
            urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());

//            if(urlConnection instanceof HttpsURLConnection && sslContext != null) {
//                ((HttpsURLConnection)urlConnection).setSSLSocketFactory(sslContext.getSocketFactory());
//            }
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Content-Language", "en-US");
            //myURLConnection.setRequestProperty ("Authorization", basicAuth);
            if(headers !=null && headers.size() > 0)
            {
                for(String key:headers.keySet())
                {
                    urlConnection.setRequestProperty(key,headers.get(key));
                }
            }
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setUseCaches(false);
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            if (Build.VERSION.SDK != null
                    && Build.VERSION.SDK_INT > 13) {
                urlConnection.setRequestProperty("Connection", "close");
            }
            lastResponseCode = urlConnection.getResponseCode();
            result = Utility.readData(urlConnection.getInputStream());

        } catch(Exception ex) {
            lastResponseCode = 0;
            //result = ex.toString();
        } finally {
            if(urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return result;
    }

    public String doPost(String url, HashMap<String,String> params,HashMap<String,String> headers)  throws Exception {
        HttpsURLConnection urlConnection = null;
        String result = null;
        String param= "";
        for (String key : params.keySet()) {
            if(param.equalsIgnoreCase(""))
                param = key + "=" + URLEncoder.encode(params.get(key),"UTF-8");
            else
                param = param + "&"+ key + "=" + URLEncoder.encode(params.get(key),"UTF-8");
        }
        try {
            URL requestedUrl = new URL(url);
            //urlConnection = (HttpURLConnection) requestedUrl.openConnection();
            urlConnection = (HttpsURLConnection)requestedUrl.openConnection();
            //urlConnection.setHostnameVerifier(hostnameVerifier);
            urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());

//            if(urlConnection instanceof HttpsURLConnection && sslContext != null) {
//                ((HttpsURLConnection)urlConnection).setSSLSocketFactory(sslContext.getSocketFactory());
//            }
            urlConnection.setRequestMethod("POST");
            urlConnection.setFixedLengthStreamingMode(param.getBytes().length);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Content-Language", "en-US");
            //myURLConnection.setRequestProperty ("Authorization", basicAuth);
            if(headers != null && headers.size() > 0)
            {
                for(String key:headers.keySet())
                {
                    urlConnection.setRequestProperty(key,headers.get(key));
                }
            }
            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setUseCaches(false);
            //urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            if (Build.VERSION.SDK != null
                    && Build.VERSION.SDK_INT > 13) {
                urlConnection.setRequestProperty("Connection", "close");
            }

            DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
            dStream.writeBytes(param); //Writes out the string to the underlying output stream as a sequence of bytes
            dStream.flush(); // Flushes the data output stream.
            dStream.close(); // Closing the output stream.

            lastResponseCode = urlConnection.getResponseCode();

            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = "";
            StringBuilder responseOutput = new StringBuilder();
            while((line = br.readLine()) != null ) {
                responseOutput.append(line);
            }
            br.close();
            //result = Utility.readData(urlConnection.getInputStream());
            result = responseOutput.toString();

        } catch(Exception ex) {
            ex.printStackTrace();
            result = "{}";
            lastResponseCode = 0;
        } finally{
            if(urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return result;
    }

    public String doPost(String url, String params)  throws Exception {
        HttpsURLConnection urlConnection = null;
        String result = null;

        try {
            URL requestedUrl = new URL(url);
            //urlConnection = (HttpURLConnection) requestedUrl.openConnection();
            urlConnection = (HttpsURLConnection)requestedUrl.openConnection();
            //urlConnection.setHostnameVerifier(hostnameVerifier);
            urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());

//            if(urlConnection instanceof HttpsURLConnection && sslContext != null) {
//                ((HttpsURLConnection)urlConnection).setSSLSocketFactory(sslContext.getSocketFactory());
//            }
            urlConnection.setRequestMethod("POST");
            urlConnection.setFixedLengthStreamingMode(params.getBytes().length);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Content-Language", "en-US");
            //myURLConnection.setRequestProperty ("Authorization", basicAuth);

            urlConnection.setConnectTimeout(10000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setUseCaches(false);
            //urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            if (Build.VERSION.SDK != null
                    && Build.VERSION.SDK_INT > 13) {
                urlConnection.setRequestProperty("Connection", "close");
            }

            DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
            dStream.writeBytes(params); //Writes out the string to the underlying output stream as a sequence of bytes
            dStream.flush(); // Flushes the data output stream.
            dStream.close(); // Closing the output stream.

            lastResponseCode = urlConnection.getResponseCode();

            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = "";
            StringBuilder responseOutput = new StringBuilder();
            while((line = br.readLine()) != null ) {
                responseOutput.append(line);
            }
            br.close();
            //result = Utility.readData(urlConnection.getInputStream());
            result = responseOutput.toString();

        } catch(Exception ex) {
            ex.printStackTrace();
            result = "{}";
            lastResponseCode = 0;
        } finally{
            if(urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return result;
    }
}