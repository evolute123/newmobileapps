package com.eps.epslomerchantapp.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eps.epslomerchantapp.bluetooth.BluetoothComm;
import com.eps.epslomerchantapp.support.ATPrintAsync;
import com.eps.epslomerchantapp.support.ATValidateDevice;
import com.eps.epslomerchantapp.support.Utility;
import com.eps.epslomerchantapp.R;
import com.leopard.api.MagCard;
import com.leopard.api.Printer;
import com.leopard.api.Setup;

import java.io.CharArrayWriter;
import java.util.Set;

public class HardwareTest extends AppCompatActivity implements View.OnClickListener {

    Button CardTestButton, PrinterTestButton, internetTestButton, deviceValidationTestButton, changeDeviceButton;
    ImageView btnBackIvInDeviceSettings,btnExitIv;
    Boolean setupMode = false;
    TextView activityTitleInDeviceSettings;
    SharedPreferences mPref;
    Printer prn;
    MagCard mag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hardware_test);

        mPref = getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);

        CardTestButton = (Button) findViewById(R.id.CardTestButton);
        CardTestButton.setOnClickListener(this);

        PrinterTestButton = (Button) findViewById(R.id.PrinterTestButton);
        PrinterTestButton.setOnClickListener(this);

        internetTestButton = (Button) findViewById(R.id.internetTestButton);
        internetTestButton.setOnClickListener(this);

        deviceValidationTestButton = (Button) findViewById(R.id.deviceValidationTestButton);
        deviceValidationTestButton.setOnClickListener(this);

        changeDeviceButton = (Button) findViewById(R.id.changeDeviceButton);
        changeDeviceButton.setOnClickListener(this);

        btnBackIvInDeviceSettings = (ImageView) findViewById(R.id.btnBackIvInDeviceSettings);
        btnBackIvInDeviceSettings.setOnClickListener(this);

        btnExitIv = (ImageView) findViewById(R.id.btnExitIv);
        btnExitIv.setOnClickListener(this);

        activityTitleInDeviceSettings = (TextView) findViewById(R.id.activityTitleInDeviceSettings);

        if (mPref.getString("tid", "").length() == 0) {
            deviceValidationTestButton.setVisibility(View.GONE);
            changeDeviceButton.setVisibility(View.GONE);
            btnBackIvInDeviceSettings.setVisibility(View.GONE);
            activityTitleInDeviceSettings.setText(R.string.ht_screen_heading);
            setupMode = true;
        }
        else
        {
            btnExitIv.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) { //TODO
        if (keyCode == KeyEvent.KEYCODE_BACK) {
           exit();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View view) {
        if (view == CardTestButton) {
            ReadMagcardAsyc magcard = new ReadMagcardAsyc();
            magcard.execute(0);
        } else if (view == PrinterTestButton) {
            new ATPrintAsync(ConnectActivity.mgp,HardwareTest.this,null,"TESTPRINT").execute();
        } else if (view == internetTestButton) {
            if (Utility.isNetWorkAvailable(HardwareTest.this)) {
                Utility.alert(s(R.string.ht_internet_test_head), s(R.string.ht_internet_test), HardwareTest.this);
            } else {
                Utility.alert(s(R.string.ht_internet_test_head), s(R.string.ht_internet_test_error), HardwareTest.this);
            }
        } else if (view == deviceValidationTestButton) {
            new ATValidateDevice(HardwareTest.this, HardwareTest.this, mPref.getString("tid", ""), "HardwareTest").execute();
        } else if (view == changeDeviceButton) {
            Intent in = new Intent(HardwareTest.this, ValidateDevice.class);
            in.putExtra("ChangeMode", true);
            startActivity(in);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        } else if (view == btnBackIvInDeviceSettings) {
            if (setupMode) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                finish();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(HardwareTest.this);
                builder.setMessage(R.string.vd_exit_msg).setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

            } else {
                finish();
                overridePendingTransition(R.anim.right_in, R.anim.right_out);
            }
        } else if(view == btnExitIv)
        {
            exit();
        }
    }

    private void exit(){
        if (setupMode) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            setResult(101);
                            finish();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(HardwareTest.this);
            builder.setMessage(R.string.vd_exit_msg).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        } else {
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }
    }
    private String s(int i) {
        return getResources().getString(i);
    }
    private class ReadMagcardAsyc extends AsyncTask<Integer, Integer, String> {
        String sMagStatus = null;
        private ProgressDialog mpd = null;
        MagCard mag;
        @Override
        protected void onPreExecute() {
            this.mpd = new ProgressDialog(HardwareTest.this);
            this.mpd.setMessage("Please swipe the card");
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.show();
        }

        @Override
        protected String doInBackground(Integer... arg0) {
            try {
                if (ConnectActivity.mgp.isConnect()) {
                    ConnectActivity.mgp.InstantiateMagcard();
                    mag = ConnectActivity.mgp.mag;
                    mag.vReadMagCardData(10000);
                    int iretval = ConnectActivity.mgp.mag.iGetReturnCode();
                    sMagStatus = ConnectActivity.mgp.checkiRetvalMag(iretval);
                    if (!sMagStatus.equalsIgnoreCase("Success")) {
                        return sMagStatus;
                    }
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return sMagStatus;
        }

        @Override
        protected void onPostExecute(String result) {
            if (this.mpd.isShowing())
                this.mpd.dismiss();
            if (result.equalsIgnoreCase("Success")) {
                String track1 = mag.sGetTrack1Data();
                String track2 = mag.sGetTrack2Data();
                String track3 = mag.sGetTrack3Data();
                String carddetails = "Details of Card are \nTrack1: "+track1+"\nTrack2: "+track2+"\nTrack3: "+track3;
                Log.i("Card Details", carddetails);
                //Toast.makeText(HardwareTest.this,track1+track2+track3,Toast.LENGTH_LONG).show();
                Utility.alert("Card Details",carddetails , HardwareTest.this);
            }
            else
                Utility.alert("Card Failed","Could not read the card",HardwareTest.this);
        }
    }
}
