package com.eps.epslomerchantapp.main;

import android.graphics.Color;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.support.ATPrintAsync;
import com.eps.epslomerchantapp.support.Utility;
import com.eps.epslomerchantapp.support.DatabaseHandler;
import com.eps.epslomerchantapp.support.EPSTransaction;

import org.json.JSONObject;

import java.util.Iterator;

public class EndTransaction extends AppCompatActivity {
    Button merchantBtn,dupBtn, closeBtn;
    private EPSTransaction mTrans;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_transaction);

        DatabaseHandler dh = new DatabaseHandler(getApplicationContext());
        long rowid = getIntent().getLongExtra("transactionid",0);
        mTrans = dh.getTransaction(""+rowid);

        TextView tvtransid = (TextView) findViewById(R.id.tvtransidtext);
        tvtransid.setText(mTrans.getUnique_txn_id());

        TextView tvinvnum = (TextView) findViewById(R.id.tvinvnumtext);
        tvinvnum.setText(mTrans.getInvoice_number());

        TextView tvamt = (TextView) findViewById(R.id.tvamttext);
        tvamt.setText("Rs. " + mTrans.getAmount());

        TextView tvttype = (TextView) findViewById(R.id.tvttypetext);
        tvttype.setText(mTrans.getTransaction_type());

        TextView tvttime = (TextView) findViewById(R.id.tvttimetext);
        tvttime.setText(mTrans.getTransaction_timestamp("yyyy-MM-dd HH:mm:ss"));

        TextView tvptime = (TextView) findViewById(R.id.tvptimetext);
        tvptime.setText(mTrans.getPayment_timestamp("yyyy-MM-dd HH:mm:ss"));

        TextView tvref = (TextView) findViewById(R.id.tvreftext);
        tvref.setText(mTrans.getReference_no());


        TextView tvtdet = (TextView) findViewById(R.id.tvtdettext);
        tvtdet.setText(mTrans.getCardSuccessDetails());
        TextView tvtcardno = (TextView) findViewById(R.id.tvtcardnotext);
        TextView tvtctype = (TextView) findViewById(R.id.tvtctypetext);
        TextView tvtpgid = (TextView) findViewById(R.id.tvtpgidtext);
        TableRow trdet = (TableRow) findViewById(R.id.trdet);
        TableRow trcardno = (TableRow) findViewById(R.id.trcardno);
        TableRow trctype = (TableRow) findViewById(R.id.trctype);
        TableRow trpgid = (TableRow) findViewById(R.id.trpgid);

        if(mTrans.getTransaction_type().equalsIgnoreCase("cash")) {
            tvtdet.setText(mTrans.getTransaction_details());
            trpgid.setVisibility(View.GONE);
            trcardno.setVisibility(View.GONE);
            trctype.setVisibility(View.GONE);
        }
        else
        {
            tvtcardno.setText(mTrans.getPGItem("card_no"));
            tvtctype.setText(mTrans.getPGItem("card_type"));
            tvtpgid.setText(mTrans.getPGItem("id"));
            trdet.setVisibility(View.GONE);
        }

        TextView tvtstate = (TextView) findViewById(R.id.tvtstatetext);
        String state = mTrans.getTransaction_state().toUpperCase();
        tvtstate.setText(state.toUpperCase());
        if(state.equalsIgnoreCase("APPROVED")) {
            tvtstate.setTextColor(Color.parseColor("#A4C639"));
        }else if(state.equalsIgnoreCase("CANCELLED") || state.equalsIgnoreCase("DECLINED")) {
            tvtstate.setTextColor(Color.parseColor("#FF0000"));
        }

        merchantBtn = (Button) findViewById(R.id.printReceiptButton);
        closeBtn = (Button) findViewById(R.id.closeButton);
        dupBtn = (Button) findViewById(R.id.printDuplicateButton);
        merchantBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ATPrintAsync(ConnectActivity.mgp,EndTransaction.this,mTrans,"MERCHANT").execute();
            }
        });
        dupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ATPrintAsync(ConnectActivity.mgp,EndTransaction.this,mTrans,"DUPLICATE").execute();
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(mTrans == null)
            Utility.alert("Transaction Error!","Could not retrieve the transaction. Please check if the transaction is valid.",EndTransaction.this);
    }
    private void printTransaction()
    {

    }
    @Override
    public void onStart(){
        super.onStart();
        new ATPrintAsync(ConnectActivity.mgp,EndTransaction.this,mTrans,"CUSTOMER").execute();
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
