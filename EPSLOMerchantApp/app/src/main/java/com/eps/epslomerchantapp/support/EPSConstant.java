package com.eps.epslomerchantapp.support;

/**
 * Created by satish_kota on 26-05-2016.
 */
public class EPSConstant {
    public static final String PROGRESS_CANCELLED = "PROGRESS_BAR_CANCELLED";
    public static final String DEVICE_INVALID = "DEVICE_NOT_VALID";
    public static final String DEVICE_ALREADY_ASSOCIATED = "DEVICE_ALREADY_ASSOCIATED";
    public static final String DEVICE_TRANSACTION_TIMEOUT = "DEVICE_TRANSACTION_TIMEOUT";
    public static final String BT_FAILED_TO_START = "BT_FAILED_TO_START";
    public static final String BT_SCAN_DONE = "BT_SCAN_DONE";
    public static final String BT_SCAN_TIMEOUT = "BT_SCAN_TIMEOUT";
    public static final String BT_CONNECTION_FAILED = "BT_CONNECTION_FAILED";
    public static final String DEVICE_CANCELLED = "CANCELLED";
    public static final String VALIDATION_COMPLETE = "VALIDATION_DONE";
    public static final String SERVER_SUCCESS = "SUCCESS";
    public static final String SERVER_UNKNOWN = "UNKNOWN";
    public static final String SERVER_INTERNET_FAILED = "INTERNET_FAILED";
    public static final String SERVER_CONNECTION_FAILED = "CONNECTION_FAILED";
    public static final String SERVER_INVALID_REQUEST = "INVALID_REQUEST";
    public static final String SERVER_AUTH_FAILED = "AUTHENTICATION_FAILED";
    public static final String SERVER_DEVICE_NOT_VALID = "DEVICE_NOT_VALID";
    public static final String SERVER_TRANSACTION_FAILED = "TRANSACTION_FAILED";
    public static final String SERVER_FORMAT_ERROR = "FORMAT_ERROR";
    public static final String SERVER_FAILED = "SERVER_FAILED";
    public static final String DEVICE_NAME = "ESFP0002"; // ESYS12345       ESFP0002
    public static final int BT_WAIT_TIME = 20;
    public static final int BT_SLEEP_TIME = 100;
    public static final int SMS_WAIT_TIME = 60; //120
    public static final int SMS_SLEEP_TIME = 100;

    public static final int APPLICATION_TRANSACTION_TIMEOUT = 61;

    public static final String BT_CONN_FAIL = "CONN_FAIL";//0x04;
    public static final String BT_CONN_SUCCESS = "CONN_SUCCESS"; //0x05;
    public static final String BT_RET_BOND_FAIL = "RET_BOND_FAIL"; //0x06;

    public static final String SMS_TIMEOUT = "SMS_TIMEOUT";
    public static final String SMS_SUCCESS = "SMS_SUCCESS";

    public enum TransactionState {
        InitialState,
        BTSwitchedOn,
        BTPaired,
        TransactionInitiatedOnDevice,
        TransactionInitiatedOnServer,
        WaitingForMakePayment,
        TransactionDeclined,
        USSDStarted,
        USSDFinished,
        SMSReceived,
        USSDTimeout
    }
}
