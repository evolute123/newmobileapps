package com.eps.epslomerchantapp.support;

import android.content.SharedPreferences;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by satish_kota on 23-08-2016.
 */
public class EPSTransaction {
    private String id = ""; // Primary Key
    private  String transaction_id = "";
    private  String invoice_number= "";
    private  String amount = "";
    private String transaction_type = "";
    private String transaction_timestamp = "";
    private String payment_timestamp = "";
    private String transaction_details = "";

    private String transaction_state = "";
    private String sync_state = "";

    private String full_response="";
    private String reference_no="";
    private String unique_txn_id="";
    private String bank_code = "";
    private String pg_key = "";

    public EPSTransaction()
    {

    }
    public EPSTransaction(String id,String tid, String inv_num, String amount, String ttype, String ttime, String ptime, String txnid, String tdet, String tstate)
    {
        this.id = id;
        this.transaction_id = tid;
        this.invoice_number=inv_num;
        this.amount = amount;
        this.transaction_type = ttype;
        this.transaction_timestamp = ttime;
        this.payment_timestamp = ptime;
        this.transaction_details = tdet;
        this.transaction_state = tstate;
        this.full_response="";
        this.reference_no = "";
        this.unique_txn_id=txnid;
        this.sync_state = "UNSYNCED";
        this.setPg_key("");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }

    public String getAmount() {
        String amt = amount;
        if (!amt.isEmpty()) {
            if(amt.contains(".")) {
                char[] chars = amt.toCharArray();
                if (chars[chars.length - 2] == '.')
                    amt = amt + "0";
            }
            else
            {
                amt = amt + ".00";
            }
        }
        return amt;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getTransaction_timestamp(String format) {
        if(format.isEmpty()||transaction_timestamp.isEmpty())
            return transaction_timestamp;
        else
        {
            try {
                SimpleDateFormat f1;
                if(transaction_timestamp.contains("T"))
                    f1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                else
                    f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = f1.parse(transaction_timestamp);
                SimpleDateFormat f2 = new SimpleDateFormat(format);
                return f2.format(d);
            }catch(Exception ex)
            {
                return transaction_timestamp;
            }
        }
    }

    public void setTransaction_timestamp(String transaction_timestamp) {
        this.transaction_timestamp = transaction_timestamp;
    }

    public String getPayment_timestamp(String format) {
        if(format.isEmpty() || payment_timestamp.isEmpty())
            return payment_timestamp;
        else
        {
            try {
                SimpleDateFormat f1;
                if(payment_timestamp.contains("T"))
                   f1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                else
                    f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = f1.parse(payment_timestamp);
                SimpleDateFormat f2 = new SimpleDateFormat(format);
                return f2.format(d);
            }catch(Exception ex)
            {
                return payment_timestamp;
            }
        }
    }

    public void setPayment_timestamp(String payment_timestamp) {
        this.payment_timestamp = payment_timestamp;
    }

    public String getTransaction_details() {
        return transaction_details;
    }

    public void setTransaction_details(String transaction_details) {
        this.transaction_details = transaction_details;
    }

    public String getTransaction_state() {
        return transaction_state;
    }

    public void setTransaction_state(String transaction_state) {
        this.transaction_state = transaction_state;
    }

    public String getSync_state() {
        return sync_state;
    }

    public void setSync_state(String sync_state) {
        this.sync_state = sync_state;
    }

    public String getFull_response()
    {
        return full_response;
    }
    public void setFull_response(String full_response)
    {
        this.full_response=full_response;
    }

    public static EPSTransaction generateTransaction(String amount,String type,String status,SharedPreferences mPref)
    {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String transactionDate = format.format(date);
        int inv_num = mPref.getInt("NextInvNum",0);
        SimpleDateFormat format2 = new SimpleDateFormat("yyMMddHHmmss");
        String tid = mPref.getString("tid","");
        String uid = format2.format(date);
        String txnid =  tid.trim()+uid.trim();
        String s_inv_date = mPref.getString("NextInvDate","");
        if(s_inv_date.equalsIgnoreCase(transactionDate.split(" ")[0]))
            inv_num++;
        else
            inv_num = 1;
        SharedPreferences.Editor edit = mPref.edit();
        edit.putString("NextInvDate",transactionDate.split(" ")[0]);
        edit.putInt("NextInvNum",inv_num);
        edit.apply();

        return new EPSTransaction("", mPref.getString("tid",""), ""+inv_num, amount,type,transactionDate,
                (type=="CASH" ? transactionDate : ""),txnid, (type=="CASH" ? "CASH PAYMENT" : ""),
                status);
    }

    public String getPGItem(String itemname)
    {
        String retval = "";
        if(this.transaction_type.equalsIgnoreCase("CARD") && !this.transaction_details.isEmpty())
        {
            try {
                JSONObject jo = new JSONObject(this.transaction_details);
                retval = jo.get(itemname).toString();
            }catch (Exception ex)
            {
                retval = "";
            }
        }
        return retval;
    }

    public String getCardSuccessDetails()
    {
        String retval = "";
        if(this.transaction_type.equalsIgnoreCase("CARD") && !this.transaction_details.isEmpty())
        {
            try {
                JSONObject jo = new JSONObject(this.transaction_details);
                retval = retval + "PG ID: " + jo.get("id").toString();
                retval = retval + "\n";
                retval = retval + "CARD NUM: " +jo.get("card_no").toString();
                retval = retval + "\n";
                retval = retval + "CARD TYPE: " + jo.get("card_type").toString();
            }catch (Exception ex)
            {
                retval = "";
            }
        }
        return retval;
    }
    public String getReference_no() {
        return reference_no;
    }

    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }

    public String getUnique_txn_id() {
        return unique_txn_id;
    }

    public void setUnique_txn_id(String unique_txn_id) {
        this.unique_txn_id = unique_txn_id;
    }

    public String getBank() {
        return bank_code;
    }

    public void setBank(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getPg_key() {
        return pg_key;
    }

    public void setPg_key(String pg_key) {
        this.pg_key = pg_key;
    }
}
