package com.eps.epslomerchantapp.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.support.Utility;
import com.eps.epslomerchantapp.support.DatabaseHandler;
import com.eps.epslomerchantapp.support.EPSConstant;
import com.eps.epslomerchantapp.support.EPSServerCaller;
import com.eps.epslomerchantapp.support.EPSTransaction;
import com.eps.epslomerchantapp.support.EPSWebURL;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StartTransaction extends AppCompatActivity implements View.OnClickListener{
    Button cashBtn, cardBtn, cancelBtn;
    EditText amt;
    ImageView btnBackIv;
    SharedPreferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_transaction);

        mPref =  getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);
        cashBtn = (Button) findViewById(R.id.cashButton);
        cardBtn = (Button) findViewById(R.id.cardButton);
        cancelBtn = (Button) findViewById(R.id.cancelButton);
        btnBackIv = (ImageView) findViewById(R.id.btnBackIv);
        amt = (EditText) findViewById(R.id.amountEditText);
        amt.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(7,2)});

        cashBtn.setOnClickListener(this);
        cardBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        btnBackIv.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Get Result from USSD
        String message = "";

        if(requestCode==312)
            finish();
    }

    @Override
    public void onClick(View view) {
        if(view == cashBtn)
        {
            final String amount  = amt.getText().toString();
            if(amount.isEmpty())
            {
                Utility.alert("Error", "Please enter a valid amount to execute the transaction",StartTransaction.this);
            }
            else {

                if (Utility.isNetWorkAvailable(StartTransaction.this)) {
                    EPSTransaction trans = EPSTransaction.generateTransaction(amount,"CASH","approved",mPref);
                    new ATCashTransaction(trans,mPref.getString("tid","")).execute();
                } else {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    EPSTransaction trans = EPSTransaction.generateTransaction(amount,"CASH","approved",mPref);
                                    long rowid = saveTransactionToDB(trans);
                                    Intent in = new Intent(StartTransaction.this,EndTransaction.class);
                                    in.putExtra("transactionid",rowid);
                                    startActivity(in);
                                    finish();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    Utility.alert("Transaction Failed", "Cannot Execute a Card Transaction",StartTransaction.this);
                                    break;
                            }
                        }
                    };
                    AlertDialog.Builder builder = new AlertDialog.Builder(StartTransaction.this);
                    builder.setTitle("Network Erro").setMessage("Cannot Update the Transaction to the server. No Internet Found. Saving Locally. Please sync transactions when internet is available").setPositiveButton("OK", dialogClickListener).show();
                }
            }
        }
        else if(view==cardBtn)
        {
            final String amount  = amt.getText().toString();
            if(amount.isEmpty())
            {
                Utility.alert("Error", "Please enter a valid amount to execute the transaction",StartTransaction.this);
            }
            else {
                if (Utility.isNetWorkAvailable(StartTransaction.this)) {
                    Intent cardIntent = new Intent(StartTransaction.this, CardPayment.class);
                    cardIntent.putExtra("amount",amount);
                    startActivityForResult(cardIntent,312);
                } else {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which){
                                case DialogInterface.BUTTON_POSITIVE:
                                    EPSTransaction trans = EPSTransaction.generateTransaction(amount,"CASH","approved",mPref);
                                    long rowid = saveTransactionToDB(trans);
                                    Intent in = new Intent(StartTransaction.this,EndTransaction.class);
                                    in.putExtra("transactionid",rowid);
                                    startActivity(in);
                                    finish();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    Utility.alert("Transaction Failed", "Cannot Execute a Card Transaction",StartTransaction.this);
                                    break;
                            }
                        }
                    };
                    AlertDialog.Builder builder = new AlertDialog.Builder(StartTransaction.this);
                    builder.setTitle("Transaction Error").setMessage("Cannot create a card transaction. Internet Failed. Would you want to convert it into a Cash Transaction?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
            }
        }
        else if(view==cancelBtn || view == btnBackIv)
        {
            exit();
        }

    }
    @Override
    public void onBackPressed() {
        exit();
    }

    public class DecimalDigitsInputFilter implements InputFilter {
        Pattern mPattern;
        public DecimalDigitsInputFilter(int digitsBeforeZero,int digitsAfterZero) {
            mPattern=Pattern.compile("[0-9]{0," + (digitsBeforeZero-1) + "}+((\\.[0-9]{0," + (digitsAfterZero-1) + "})?)||(\\.)?");
        }
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            Matcher matcher=mPattern.matcher(dest);
            if(!matcher.matches())
                return "";
            return null;
        }
    }

    private void exit() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        //overridePendingTransition(R.anim.right_in, R.anim.right_out);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(StartTransaction.this);
        builder.setTitle("Exit Transaction").setMessage("Are you sure you want to exit this transaction?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    private long saveTransactionToDB(EPSTransaction trans)
    {
       DatabaseHandler dh = new DatabaseHandler(getApplicationContext());
       return dh.createTransactions(trans);
    }


    private class ATCashTransaction extends AsyncTask<String, String, String> {

        private ProgressDialog mpd = null;
        private EPSTransaction mTrans;
        private String mTID = "";
        public ATCashTransaction(EPSTransaction trans,String tid)
        {
            mTrans=trans;
            mTID = tid;
        }
        @Override
        public void onPreExecute() {
            this.mpd = new ProgressDialog(StartTransaction.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.show();
        }

        @Override
        protected void onProgressUpdate(String... progUpdate) {
            super.onProgressUpdate(progUpdate);
            this.mpd.setMessage(progUpdate[0]);
        }

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Updating Server. Please wait...");
            String serverStatus = "";
            if (Utility.isNetWorkAvailable(StartTransaction.this)) {
                try {
                    HashMap<String, String> headers = new HashMap<>();
                    HashMap<String, String> reqparams = new HashMap<>();
                    headers.put("User-Agent", EPSWebURL.LO_USER_AGENT);
                    headers.put("TerminalID", mTID);
                    reqparams.put("amount",mTrans.getAmount());
                    reqparams.put("transactionDate",mTrans.getTransaction_timestamp(""));
                    reqparams.put("invoiceNumber",mTrans.getInvoice_number());
                    reqparams.put("txnId",mTrans.getUnique_txn_id());
                    EPSServerCaller api = new EPSServerCaller(getAssets());
                    String result = api.doPost(EPSWebURL.CASH_TRANSACTION,reqparams, headers);
                    int responseCode = api.getLastResponseCode();
                    //GET RESPONSE FROM SERVER
                    JSONObject json = new JSONObject(result);
                    if (responseCode == 200) {
                        serverStatus = json.optString("status");
                        String code = json.optString("code");
                        //String serverMessage = json.optString("message");
                        if (serverStatus.equalsIgnoreCase(EPSConstant.SERVER_SUCCESS)) {
                            JSONObject trans = json.getJSONObject("transaction");
                            mTrans.setTransaction_id(trans.optString("id"));
                            mTrans.setInvoice_number(trans.optString("invoiceNumber"));
                            mTrans.setAmount(trans.optString("amount"));
                            mTrans.setTransaction_type(trans.optString("transactionType"));
                            mTrans.setTransaction_timestamp(trans.optString("transactionTimestamp"));
                            mTrans.setPayment_timestamp(trans.optString("paymentTimestamp"));
                            mTrans.setTransaction_details(trans.optString("transactionDetails"));
                            mTrans.setFull_response(trans.optString("transactionDetails2"));
                            mTrans.setUnique_txn_id(trans.optString("txnId"));
                            mTrans.setReference_no(trans.optString("refNo"));
                            mTrans.setBank(trans.optString("bankCode"));
                            mTrans.setTransaction_state(trans.optString("transactionStatus"));
                            mTrans.setSync_state("SUCCESS");
                        } else if (code.equalsIgnoreCase(EPSConstant.DEVICE_INVALID)) {
                            serverStatus = EPSConstant.DEVICE_INVALID;
                        } else if (code.isEmpty()) {
                            serverStatus = EPSConstant.SERVER_UNKNOWN;
                        } else {
                            serverStatus = code;
                        }
                    }
                    else
                    {
                        serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
                    }
                } catch (Exception e) {
                    serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
                }
            } else {
                serverStatus = EPSConstant.SERVER_INTERNET_FAILED;
            }
            publishProgress("Device Validation Complete");

            //RETURN STATUS FOR ERROR HANDLING
            if (serverStatus.equalsIgnoreCase(EPSConstant.SERVER_SUCCESS))
                //Transaction got created and completed. Next step is to execute payment
                return EPSConstant.VALIDATION_COMPLETE;
            else
                return serverStatus;
        }

        @Override
        public void onPostExecute(String result) {
            if (this.mpd.isShowing())
                this.mpd.dismiss();
            long rowid = saveTransactionToDB(mTrans);
            if (EPSConstant.VALIDATION_COMPLETE.equalsIgnoreCase(result)) {
                Intent in = new Intent(StartTransaction.this,EndTransaction.class);
                in.putExtra("transactionid",rowid);
                startActivity(in);
                overridePendingTransition( R.anim.left_in, R.anim.left_out);
                finish();
            }
            else if (EPSConstant.DEVICE_INVALID.equalsIgnoreCase(result)) {
                SharedPreferences.Editor edit = mPref.edit();
                edit.putString("deviceStatus","inactive");
                edit.apply();
                startActivity(new Intent(StartTransaction.this, DeviceDetails.class));
                overridePendingTransition( R.anim.left_in, R.anim.left_out);
                finish();
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INTERNET_FAILED)) {
                alert(rowid,"Network Error","Cannot connect to Server. No Internet found",StartTransaction.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_CONNECTION_FAILED)) {
                alert(rowid,"Network Error","Server not responding. Cannot validate the device",StartTransaction.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_TRANSACTION_FAILED)) {
                alert(rowid,"Validation Error","Server could not save the Transaction. Please try again",StartTransaction.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INVALID_REQUEST)) {
                alert(rowid,"Validation Error","Device Validation Failed. Invalid Request",StartTransaction.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_AUTH_FAILED)) {
                alert(rowid,"Validation Error","Device Validation Failed. Authentication Error",StartTransaction.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_FORMAT_ERROR)) {
                alert(rowid,"Validation Error","Device Validation Failed. Incorrect Data Sent",StartTransaction.this);
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_UNKNOWN)) {
                alert(rowid,"Validation Error","Device Validation Failed. Unknown Error",StartTransaction.this);
            } else {
                alert(rowid,"Validation Error", "Device Validation Failed. Unknown Error", StartTransaction.this);
            }
        }
        private void alert(long lrowid,String title,String message, Context ctx)
        {
            final long rowid = lrowid;
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent in = new Intent(StartTransaction.this,EndTransaction.class);
                            in.putExtra("transactionid",rowid);
                            startActivity(in);
                            overridePendingTransition( R.anim.left_in, R.anim.left_out);
                            finish();
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder.setTitle(title).setMessage(message).setPositiveButton("OK", dialogClickListener).show();
        }
    }
}
