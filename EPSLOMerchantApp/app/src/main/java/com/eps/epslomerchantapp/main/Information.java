package com.eps.epslomerchantapp.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.eps.epslomerchantapp.R;

public class Information extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        ImageView ivExit = (ImageView) findViewById(R.id.ivExit);
        if (ivExit != null)
            ivExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                    overridePendingTransition( R.anim.right_in, R.anim.right_out);
                }
            });

        RelativeLayout rlInstructions = (RelativeLayout) findViewById(R.id.rlinstructions);
        if(rlInstructions != null)
            rlInstructions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,Instructions.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });

        RelativeLayout rlFAQ = (RelativeLayout) findViewById(R.id.rlfaq);
        if(rlFAQ != null)
            rlFAQ.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,FAQ.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });

        RelativeLayout rlTAC = (RelativeLayout) findViewById(R.id.rltos);
        if(rlTAC != null)
            rlTAC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,TermsConditions.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });

        RelativeLayout rlPrivacy = (RelativeLayout) findViewById(R.id.rlpp);
        if(rlPrivacy != null)
            rlPrivacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,PrivacyPolicy.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });

        RelativeLayout rlContact = (RelativeLayout) findViewById(R.id.rlcus);
        if(rlContact != null)
            rlContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Information.this,Contact.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
            });


    }
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition( R.anim.right_in, R.anim.right_out);
    }
}
