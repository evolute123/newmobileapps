package com.eps.epslomerchantapp.main;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.support.EPSConstant;
import com.eps.epslomerchantapp.support.EPSServerCaller;
import com.eps.epslomerchantapp.support.EPSWebURL;
import com.eps.epslomerchantapp.support.Utility;
import com.eps.epslomerchantapp.support.DatabaseHandler;
import com.eps.epslomerchantapp.support.EPSTransaction;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class ShowTransactions extends AppCompatActivity implements View.OnClickListener{

    ImageView back_image,search_button;
    Button btnSync;
    ListView listView;
    Context ctx=null;
    TransactionItemAdapter adapter;
    private int year;
    private int month;
    private int day;
    Button bf=null;
    Button bt=null;
    boolean b=false;
    String start,end;
    int from=0;
    int to=0;
    int conf=0,conto=0;
    RelativeLayout rllistfooter;
    LinearLayout lllistheader;
    List<EPSTransaction> transactions;
    List<EPSTransaction> unsyncedTrans;
    Date mToday,mTomorrow,mLastMonth;
    DatabaseHandler dh;
    TextView transHeading,notrans;
    SharedPreferences mPref;
    String mStartDate="";
    String mEndDate = "";
    String mWhere="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_show_transactions);
        ctx=getApplicationContext();
        dh = new DatabaseHandler(ctx);
        mPref =  getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);

        back_image = (ImageView) findViewById(R.id.btnBackIv);
        back_image.setOnClickListener(this);

        btnSync = (Button) findViewById(R.id.btnSync);
        btnSync.setOnClickListener(this);

        Intent in = getIntent();
        mWhere = in.getStringExtra("WHERESQL");
        String header = in.getStringExtra("HEAD");
        notrans = (TextView)findViewById(R.id.notrans);
        transHeading = (TextView)findViewById(R.id.transHeading);
        transHeading.setText(header);
        rllistfooter = (RelativeLayout)findViewById(R.id.rllistfooter);
        lllistheader = (LinearLayout) findViewById(R.id.lllistheader);
        listView=(ListView)findViewById(R.id.list_submit);
        transactions = dh.getTransactionsBySQL(mWhere);
        adapter=new TransactionItemAdapter(this,transactions);
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //View hv = inflater.inflate(R.layout.submitted_list_header, null);
        //listView.addHeaderView(hv);
        //View fv = inflater.inflate(R.layout.submitted_list_footer, null);
        //listView.addFooterView(fv);
        if(transactions.size() > 0) {
            try {
                notrans.setVisibility(View.GONE);
                rllistfooter.setVisibility(View.VISIBLE);
                lllistheader.setVisibility(View.VISIBLE);
                listView.setVisibility(View.VISIBLE);
                listView.setAdapter(adapter);
                // Click event for single list row
            }
            catch(NullPointerException ex)
            {
                Log.i("Show Transaction",ex.getMessage());
                ex.printStackTrace();
            }
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    int item = 0;
                    try {
                        item = (int) view.getTag();
                    }catch(Exception ex) {
                        item = 0;
                    }
                    if(item !=0) {
                        EPSTransaction transaction = dh.getTransaction(""+item);
                        Intent in = new Intent(getApplicationContext(), TransactionView.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        in.putExtra("transaction_id", transaction.getId());
                        startActivity(in);
                        overridePendingTransition( R.anim.left_in, R.anim.left_out);
                    }
                }
            });
        }
        else{
            notrans.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            rllistfooter.setVisibility(View.GONE);
            lllistheader.setVisibility(View.GONE);
        }
        if(mWhere.contains("UNSYNCED") || !mWhere.contains("transaction_state")) {
            unsyncedTrans = dh.getUnsyncedTransactions();
            if (unsyncedTrans.size() == 0)
                btnSync.setVisibility(View.INVISIBLE);
        }
        else
        {
            btnSync.setVisibility(View.INVISIBLE);
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition( R.anim.right_in, R.anim.right_out);
    }

    @Override
    public void onClick(View view) {
        if (view == back_image) {
            finish();
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
        }
        else if(view == btnSync)
        {
            new ATSyncTransactions().execute();
        }
    }

    private class ATSyncTransactions extends AsyncTask<String, String, String> {

        private ProgressDialog mpd = null;
        private long successTrans=0;
        private long failedTrans = 0;
        public ATSyncTransactions()
        {

        }
        @Override
        public void onPreExecute() {
            this.mpd = new ProgressDialog(ShowTransactions.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.show();
        }

        @Override
        protected void onProgressUpdate(String... progUpdate) {
            super.onProgressUpdate(progUpdate);
            this.mpd.setMessage(progUpdate[0]);
        }

        @Override
        protected String doInBackground(String... params) {
            publishProgress("Updating server with "+unsyncedTrans.size()+" UnSynced Transactions. Please wait...");
            String serverStatus = EPSConstant.VALIDATION_COMPLETE;
            if (Utility.isNetWorkAvailable(ShowTransactions.this)) {
                int cnt = 1;
                for (EPSTransaction mTrans : unsyncedTrans) {
                    publishProgress("Updating "+(cnt++)+" out of "+unsyncedTrans.size()+" transactions. Please wait...");
                    if (Utility.isNetWorkAvailable(ShowTransactions.this)) {
                        try {
                            HashMap<String, String> headers = new HashMap<>();
                            HashMap<String, String> reqparams = new HashMap<>();
                            EPSServerCaller api = new EPSServerCaller(getAssets());
                            headers.put("User-Agent", EPSWebURL.LO_USER_AGENT);
                            headers.put("TerminalID", mPref.getString("tid", ""));
                            String result = "";
                            if (mTrans.getTransaction_type().equalsIgnoreCase("cash")) {
                                reqparams.put("amount", mTrans.getAmount());
                                reqparams.put("transactionDate", mTrans.getTransaction_timestamp(""));
                                reqparams.put("invoiceNumber", mTrans.getInvoice_number());
                                reqparams.put("txnId",mTrans.getUnique_txn_id());
                                result = api.doPost(EPSWebURL.CASH_TRANSACTION, reqparams, headers);
                            } else {
                                reqparams.put("amount", mTrans.getAmount());
                                reqparams.put("transactionDate", mTrans.getTransaction_timestamp(""));
                                reqparams.put("invoiceNumber", mTrans.getInvoice_number());
                                reqparams.put("txnId",mTrans.getUnique_txn_id());
                                result = api.doPost(EPSWebURL.CARD_TRANSACTION, reqparams, headers);
                            }
                            int responseCode = api.getLastResponseCode();
                            //GET RESPONSE FROM SERVER
                            JSONObject json = new JSONObject(result);
                            if (responseCode == 200) {
                                String status = json.optString("status");
                                String code = json.optString("code");
                                //String serverMessage = json.optString("message");
                                if (status.equalsIgnoreCase(EPSConstant.SERVER_SUCCESS)) {
                                    JSONObject trans = json.getJSONObject("transaction");

                                    mTrans.setTransaction_id(trans.optString("id"));
                                    mTrans.setInvoice_number(trans.optString("invoiceNumber"));
                                    mTrans.setAmount(trans.optString("amount"));
                                    mTrans.setTransaction_type(trans.optString("transactionType"));
                                    mTrans.setTransaction_timestamp(trans.optString("transactionTimestamp"));
                                    mTrans.setPayment_timestamp(trans.optString("paymentTimestamp"));
                                    mTrans.setTransaction_details(trans.optString("transactionDetails"));
                                    mTrans.setFull_response(trans.optString("transactionDetails2"));
                                    mTrans.setUnique_txn_id(trans.optString("txnId"));
                                    mTrans.setReference_no(trans.optString("refNo"));
                                    mTrans.setBank(trans.optString("bankCode"));
                                    mTrans.setTransaction_state(trans.optString("transactionStatus"));
                                    mTrans.setSync_state("SUCCESS");
                                    dh.updateTransaction(mTrans);
                                    successTrans++;
                                } else if (code.equalsIgnoreCase(EPSConstant.DEVICE_INVALID)) {
                                    serverStatus = EPSConstant.DEVICE_INVALID;
                                    break;
                                } else {
                                    failedTrans++;
                                    //serverStatus = code;
                                }
                            } else {
                                failedTrans++;
                                //serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
                            }
                        } catch (Exception e) {
                            failedTrans++;
                            //serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
                        }

                    } else {
                        failedTrans++;
                        //serverStatus = EPSConstant.SERVER_INTERNET_FAILED;
                    }
                    publishProgress("Device Validation Complete");
                }
            }
            else{
                serverStatus = EPSConstant.SERVER_INTERNET_FAILED;
            }
            return serverStatus;
        }

        @Override
        public void onPostExecute(String result) {
            if (this.mpd.isShowing())
                this.mpd.dismiss();
            if (EPSConstant.VALIDATION_COMPLETE.equalsIgnoreCase(result)) {
                String syncmsg = "Attempted syncing ";
                if(unsyncedTrans.size() == 1)
                    syncmsg = syncmsg + "1 transaction to the server. ";
                else
                    syncmsg = syncmsg + unsyncedTrans.size()+" transactions to the server. ";
                if(successTrans==0)
                    syncmsg = syncmsg + "No transactions completed successfully and ";
                else if(successTrans==0)
                    syncmsg = syncmsg + "1 transaction completed successfully and ";
                else
                    syncmsg = syncmsg + successTrans+" transactions completed successfully and ";
                if(failedTrans==0)
                    syncmsg = syncmsg + "none failed.";
                else if (failedTrans==1)
                    syncmsg = syncmsg + "1 transaction failed to update.";
                else
                    syncmsg = syncmsg + failedTrans +  " transactions failed to update";

                Utility.alert("Sync Complete",syncmsg,ShowTransactions.this);
                List<EPSTransaction> transactions = dh.getTransactionsBySQL(mWhere);
                if(transactions.size()>0) {
                    adapter = new TransactionItemAdapter(ShowTransactions.this, transactions);
                    listView.setAdapter(adapter);
                }
                else {
                    notrans.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    rllistfooter.setVisibility(View.GONE);
                    lllistheader.setVisibility(View.GONE);
                }
                unsyncedTrans = dh.getUnsyncedTransactions();
                if(unsyncedTrans.size()==0)
                    btnSync.setVisibility(View.INVISIBLE);
            } else if (result.equalsIgnoreCase(EPSConstant.DEVICE_INVALID)) {
                SharedPreferences.Editor edit = mPref.edit();
                edit.putString("deviceStatus","inactive");
                edit.apply();
                startActivity(new Intent(ShowTransactions.this, DeviceDetails.class));
                overridePendingTransition( R.anim.left_in, R.anim.left_out);
                finish();
            } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INTERNET_FAILED)) {
                Utility.alert("Network Error","Cannot Sync Transactions to Server. No Internet found",ShowTransactions.this);
            } else {
                Utility.alert("Error", "Something went wrong. Couldnot Sync the transactions to the server", ShowTransactions.this);
            }
        }
    }
}

