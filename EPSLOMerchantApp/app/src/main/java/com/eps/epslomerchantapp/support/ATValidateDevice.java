package com.eps.epslomerchantapp.support;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.main.MainScreen;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by satish_kota on 01-09-2016.
 */
public class ATValidateDevice extends AsyncTask<String, String, String> {

    private ProgressDialog mpd = null;
    private String mTID;
    private String mStatus="";
    private Context mContext;
    private SharedPreferences mPref;
    private boolean mSetup;
    private Activity mActivity;
    private String mClassName;

    public ATValidateDevice(Context ctx,Activity act,String tid, String className)
    {
        mContext = ctx;
        mActivity = act;
        mClassName = className;
        mTID = tid;
        mPref = mContext.getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);
        if(mPref.getString("tid", "").length() == 0)
            mSetup=true;
    }


    @Override
    public void onPreExecute() {
        this.mpd = new ProgressDialog(mContext);
        this.mpd.setCancelable(false);
        this.mpd.setCanceledOnTouchOutside(false);
        this.mpd.show();
    }

    @Override
    protected void onProgressUpdate(String... progUpdate) {
        super.onProgressUpdate(progUpdate);
        this.mpd.setMessage(progUpdate[0]);
    }

    @Override
    protected String doInBackground(String... params) {
        publishProgress("Device being validated. Please wait...");
        String serverStatus = "";
        if (Utility.isNetWorkAvailable(mContext)) {
            try {
                HashMap<String, String> headers = new HashMap<>();
                HashMap<String, String> reqparams = new HashMap<>();
                headers.put("User-Agent", EPSWebURL.LO_USER_AGENT);
                headers.put("TerminalID", mTID);
                reqparams.put("hardwareSerial", mPref.getString("HSN",""));
                reqparams.put("tabletSerial",mPref.getString("TSN",""));
                reqparams.put("phoneNumber",mPref.getString("PHONE",""));

                EPSServerCaller api = new EPSServerCaller(mContext.getAssets());
                String result = api.doPost(EPSWebURL.VALIDATE_DEVICE,reqparams,headers);
                int responseCode = api.getLastResponseCode();

                //GET RESPONSE FROM SERVER
                JSONObject json = new JSONObject(result);
                if (responseCode == 200) {
                    serverStatus = json.optString("status");
                    String code = json.optString("code");
                    //String serverMessage = json.optString("message");
                    if (serverStatus.equalsIgnoreCase(EPSConstant.SERVER_SUCCESS)) {
                        SharedPreferences.Editor tedit = mPref.edit();
                        JSONObject device = json.getJSONObject("device");
                        mStatus = device.optString("status");
                        if(mStatus.equalsIgnoreCase("active")) {
                            tedit.putString("tid", mTID);
                            tedit.putString("displayName", checknull(device.optString("displayName")));
                            tedit.putString("hardwareSerial",checknull( device.optString("hardwareSerial")));
                            tedit.putString("tabletSerial", checknull(device.optString("tabletSerial")));
                            tedit.putString("phoneNumber", checknull(device.optString("phoneNumber")));
                            tedit.putString("merchantID", checknull(device.optString("merchantID")));
                            tedit.putString("merchantName", checknull(device.optString("merchantName")));
                            tedit.putString("deviceStatus", checknull(mStatus));
                            tedit.apply();
                        }

                    } else if (code.equalsIgnoreCase(EPSConstant.DEVICE_INVALID)) {
                        serverStatus = EPSConstant.DEVICE_INVALID;
                    }else if(code.equalsIgnoreCase(EPSConstant.DEVICE_ALREADY_ASSOCIATED)){
                        serverStatus = EPSConstant.DEVICE_ALREADY_ASSOCIATED;
                    } else if (code.isEmpty()) {
                        serverStatus = EPSConstant.SERVER_UNKNOWN;
                    } else {
                        serverStatus = code;
                    }
                }
                else
                {
                    serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
                }
            } catch (Exception e) {
                serverStatus = EPSConstant.SERVER_CONNECTION_FAILED;
            }
        } else {
            serverStatus = EPSConstant.SERVER_INTERNET_FAILED;
        }
        publishProgress("Device Validation Complete");

        //RETURN STATUS FOR ERROR HANDLING
        if (serverStatus.equalsIgnoreCase(EPSConstant.SERVER_SUCCESS))
            //Transaction got created and completed. Next step is to execute payment
            return EPSConstant.VALIDATION_COMPLETE;
        else
            return serverStatus;
    }

    @Override
    public void onPostExecute(String result) {
        if (this.mpd.isShowing())
            this.mpd.dismiss();
        if (EPSConstant.VALIDATION_COMPLETE.equalsIgnoreCase(result) && mStatus.equalsIgnoreCase("active") ) {
            if(mSetup) {
                //Validate Device
                mActivity.startActivity(new Intent(mContext, MainScreen.class));
                mActivity.finish();
            }
            else if(mClassName.equalsIgnoreCase("DeviceDetails") || mClassName.equalsIgnoreCase("HardwareTest"))
            {
                Utility.alert("Device Validated","Device is in Active State",mContext);
            }
            else{
                //Change Device
                DatabaseHandler dh = new DatabaseHandler(mContext);
                dh.deleteAllTransactions();
                SharedPreferences.Editor tedit = mPref.edit();
                tedit.putInt("NextInvNum",0);
                tedit.apply();
                mActivity.finish();
                mActivity.overridePendingTransition( R.anim.left_in, R.anim.left_out);
            }
        }
        else if (EPSConstant.VALIDATION_COMPLETE.equalsIgnoreCase(result)) {
            Utility.alert("Invalid Device","This device is not active to execute Transactions. Please contact Support.",mContext);
        } else if (result.equalsIgnoreCase(EPSConstant.DEVICE_INVALID)) {
            Utility.alert("Invalid Device","This device is not active to execute Transactions. Please contact Support.",mContext);
        } else if (result.equalsIgnoreCase(EPSConstant.DEVICE_ALREADY_ASSOCIATED)) {
            Utility.alert("Invalid Device","This Terminal ID has already been associated to some other hardware device. Please contact Support.",mContext);
        }else if (result.equalsIgnoreCase(EPSConstant.SERVER_INTERNET_FAILED)) {
            Utility.alert("Network Error","Cannot connect to Server. No Internet found",mContext);
        } else if (result.equalsIgnoreCase(EPSConstant.SERVER_CONNECTION_FAILED)) {
            Utility.alert("Network Error","Server not responding. Cannot validate the device",mContext);
        } else if (result.equalsIgnoreCase(EPSConstant.SERVER_INVALID_REQUEST)) {
            Utility.alert("Validation Error","Device Validation Failed. Invalid TerminalID found",mContext);
        } else if (result.equalsIgnoreCase(EPSConstant.SERVER_AUTH_FAILED)) {
            Utility.alert("Validation Error","Device Validation Failed. Authentication Error",mContext);
        } else if (result.equalsIgnoreCase(EPSConstant.SERVER_FORMAT_ERROR)) {
            Utility.alert("Validation Error","Device Validation Failed. Incorrect Data Sent",mContext);
        } else if (result.equalsIgnoreCase(EPSConstant.SERVER_UNKNOWN)) {
            Utility.alert("Validation Error","Device Validation Failed. Unknown Error",mContext);
        } else {
            Utility.alert("Validation Error","Device Validation Failed. Unknown Error",mContext);
        }
    }
    private String checknull(String input){
        if(input == null || input == "null")
            return "";
        else
            return input;

    }
}

