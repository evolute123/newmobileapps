package com.eps.epslomerchantapp.main;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.support.DatabaseHandler;
import com.eps.epslomerchantapp.support.Utility;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class TransactionSelector extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rltoday;
    Button btnselectdate;
    ImageView btnsubmit,ivselectdate;
    TextView tvselectdate;
    Spinner spntype, spnstatus;
    ImageView ivBack;

    SimpleDateFormat format;
    String mStartDate = "";
    String mEndDate = "";
    Date mYesterday, mToday, mTomorrow, mLastMonth;
    private int year;
    private int month;
    private int day;
    TextView bf = null;
    TextView bt = null;
    boolean b = false;
    String start, end;
    int from = 0;
    int to = 0;
    int conf = 0, conto = 0;
    String[] statuses = {"ALL","APPROVED","DECLINED","TO SYNC"};
    String[] types = {"ALL","CASH","CARD"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_selector);

        rltoday = (RelativeLayout) findViewById(R.id.rltodays);
        btnselectdate = (Button) findViewById(R.id.btnselectdate);
        btnsubmit = (ImageView) findViewById(R.id.btnsubmit);
        tvselectdate = (TextView) findViewById(R.id.tvselectdate);
        ivselectdate = (ImageView) findViewById(R.id.ivselectdate);
        spntype = (Spinner) findViewById(R.id.spntype);
        spnstatus = (Spinner) findViewById(R.id.spnstatus);
        ivBack = (ImageView) findViewById(R.id.ivBack);

        rltoday.setOnClickListener(this);
        btnselectdate.setOnClickListener(this);
        btnsubmit.setOnClickListener(this);
        tvselectdate.setOnClickListener(this);
        ivselectdate.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        tvselectdate.setVisibility(View.GONE);
        ivselectdate.setVisibility(View.GONE);

        ArrayAdapter<String> typesAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, Arrays.asList(types));
        typesAdapter.setDropDownViewResource(R.layout.spinner_item);
        spntype.setAdapter(typesAdapter);

        ArrayAdapter<String> statusAdapter = new ArrayAdapter<>(this,
                R.layout.spinner_item, Arrays.asList(statuses));
        statusAdapter.setDropDownViewResource(R.layout.spinner_item);
        spnstatus.setAdapter(statusAdapter);

        format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        mToday = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        mTomorrow = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, -2);
        mYesterday = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, -30);
        mLastMonth = calendar.getTime();

        //Delete older transactions
        DatabaseHandler dh = new DatabaseHandler(getApplicationContext());
        dh.deleteOldTransactions(format.format(mLastMonth));
    }

    @Override
    public void onClick(View v) {
        if (v == rltoday) {
            Intent in = new Intent(TransactionSelector.this, ShowTransactions.class);
            in.putExtra("HEAD",getResources().getString(R.string.sht_todays_transaction));
            String d1 = format.format(mYesterday);
            String d2= format.format(mTomorrow);
            in.putExtra("WHERESQL", " where transaction_timestamp > '" + d1 + "' and transaction_timestamp < '" + d2 + "';");
            startActivity(in);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        } else if (v == btnselectdate || v == tvselectdate) {
            if (bf != null) {
                if (mStartDate.isEmpty() || bf == null)
                    bf.setText("Start Date");
                else
                    bf.setText(mStartDate);
            }
            if (bt != null) {
                if (mEndDate.isEmpty())
                    bt.setText("End Date");
                else
                    bt.setText(mEndDate);
            }
            showDialog(1);
        } else if(v == ivselectdate)
        {
            btnselectdate.setVisibility(View.VISIBLE);
            tvselectdate.setVisibility(View.GONE);
            ivselectdate.setVisibility(View.GONE);
            mStartDate = "";
            mEndDate = "";
            start = "";
            end = "";
        }
        else if (v == btnsubmit) {
            Intent in = new Intent(TransactionSelector.this, ShowTransactions.class);
            String where = "";
            String header = "Listing All Transactions";
            if(!mStartDate.isEmpty()) {
                String msd = getDate(mStartDate,-1);
                String[] dates = msd.split("-");
                String med = getDate(mEndDate,1);
                String[] d2 = med.split("-");
                header = header + " between " + mStartDate + " and "+ mEndDate;
                where = where + " transaction_timestamp > '" + dates[2] + "-" + dates[1] + "-" + dates[0]+ "' and transaction_timestamp < '" + d2[2] + "-" + d2[1] + "-" + d2[0]+ "'";
            }
            if(spntype.getSelectedItemPosition() != 0)
            {
                if(!where.isEmpty())
                {
                    where = where + " and ";
                    header = header + " and ";
                }
                where = where + " transaction_type='"+spntype.getSelectedItem().toString()+"' ";
                header = header + " having " + spntype.getSelectedItem().toString()+" type";
            }
            if(spnstatus.getSelectedItemPosition() != 0)
            {
                if(!where.isEmpty())
                {
                    where = where + " and ";
                    header = header + " and ";
                }
                if(spnstatus.getSelectedItem().toString().equalsIgnoreCase("TO SYNC"))
                    where = where + " sync_state='UNSYNCED' ";
                else
                    where = where + " transaction_state='"+spnstatus.getSelectedItem().toString().toLowerCase()+"' and sync_state='SUCCESS'";
                header = header + " having " + spnstatus.getSelectedItem().toString() + " Status";
            }
            if(where.isEmpty())
                where = ";";
            else
                where = " where "+ where + ";";
            //prepare sql from all conditions
            in.putExtra("HEAD",header);
            in.putExtra("WHERESQL", where);
            startActivity(in);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        } else if (v == ivBack) {
            exit();
        }
    }

    private String getDate(String today, int increment)
    {
        try {
            final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            final Date date = format.parse(today);
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, increment);
            return format.format(calendar.getTime());
        }catch (Exception ex)
        {
            return "";
        }
    }
    @Override
    public void onBackPressed() {
        exit();
    }

    private void exit() {
        finish();
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
    }

    //New Date picker Application code
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                LayoutInflater li = getLayoutInflater();
                View v1 = li.inflate(R.layout.viewdate, null, false);
                bf = (TextView) v1.findViewById(R.id.fromdate1);
                bt = (TextView) v1.findViewById(R.id.todate1);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                //alertDialogBuilder.setIcon(R.drawable.ic_launcher);
                alertDialogBuilder.setTitle("Search Transactions by Date");
                alertDialogBuilder.setView(v1);

                alertDialogBuilder.setPositiveButton("CONFIRM", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                        String bfr = bf.getText().toString();
                        String bto = bt.getText().toString();
                        bfr = bfr.replace(" ", "");
                        bto = bto.replace(" ", "");
                        try {
                            if (!bfr.equalsIgnoreCase("Select Valid Date".replace(" ", "")) &&
                                    !bto.equalsIgnoreCase("Select Valid Date".replace(" ", ""))
                                    && !bfr.equalsIgnoreCase("StartDate") && !bto.equalsIgnoreCase("EndDate")) {
                                Date stdate = df.parse(start);
                                Date enddate = df.parse(end);
                                int a = enddate.compareTo(stdate);
                                if (a >= 0) {
                                    mStartDate = start;
                                    mEndDate = end;
                                } else {
                                    mStartDate = end;
                                    mEndDate = start;
                                }
                                tvselectdate.setText(mStartDate+" to "+mEndDate);
                                tvselectdate.setVisibility(View.VISIBLE);
                                ivselectdate.setVisibility(View.VISIBLE);
                                btnselectdate.setVisibility(View.GONE);
                            } else {
                                dialog.dismiss();
                                Utility.alert("Date Selection Error!", "Select Valid Start and End Dates", TransactionSelector.this);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        System.out.println("onCLick of dialog");
                    }
                });
                alertDialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dalog, int which) {
                        dalog.dismiss();
                    }
                });
                bf.setOnClickListener(new ItemClick());
                bt.setOnClickListener(new ItemClick());
                return alertDialogBuilder.create();
            case 2:
                DatePickerDialog DPD = new DatePickerDialog(TransactionSelector.this, datePickerListener, year, month, day);
                DatePicker dtpick = DPD.getDatePicker();
                dtpick.setMinDate(mLastMonth.getTime());
                dtpick.setMaxDate(mTomorrow.getTime());
                dtpick.setCalendarViewShown(true);
                dtpick.setSpinnersShown(false);
                CalendarView cv = dtpick.getCalendarView();
                cv.setDateTextAppearance(R.style.Widget_CalendarView_Custom);
                return DPD;
            case 3:
                DatePickerDialog DPD2 = new DatePickerDialog(TransactionSelector.this, datePickerListener, year, month, day);
                DatePicker dtpick2 = DPD2.getDatePicker();
                dtpick2.setMinDate(mLastMonth.getTime());
                dtpick2.setMaxDate(mTomorrow.getTime());
                dtpick2.setCalendarViewShown(true);
                dtpick2.setSpinnersShown(false);
                return DPD2;
            default:
                break;
        }
        return null;
    }

    class ItemClick implements android.view.View.OnClickListener {
        @Override
        public void onClick(View v) {
            final Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
            switch (v.getId()) {
                case R.id.fromdate1:
                    from = 1;
                    to = 0;
                    b = false;
                    System.out.print("fromdate1");
                    showDialog(2);
                    break;
                case R.id.todate1:
                    from = 0;
                    to = 1;
                    b = true;
                    System.out.print("todate1");
                    showDialog(3);
                    break;
                default:
                    break;
            }
        }


    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            Calendar chk_in = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = df.format(chk_in.getTime());
            try {
                Date ds = df.parse(day + "-" + (month + 1) + "-" + year);
                Date dc = df.parse(formattedDate);
                if (ds.compareTo(dc) > 0) {
                    if (from == 1) {
                        bf.setText("Select Valid Date");
                        from = 5;
                    }
                    if (to == 1) {
                        bt.setText("Select Valid Date");
                        to = 5;
                    }
                }
            } catch (Exception e) {
            }
            String sm = "";
            String sd = "";
            if (from == 1) {
                String dt = day + "-" + (month + 1) + "-" + year;
                bf.setText(dt);
                b = false;
                if ((month + 1) < 10 || day < 10) {
                    if ((month + 1) < 10) {
                        sm = "0" + (month + 1);
                    } else {
                        sm = "" + (month + 1);
                    }
                    if (day < 10) {

                        sd = "0" + (day);
                    } else {
                        sd = "" + (day);
                    }
                    start = sd.trim() + "-" + sm + "-" + year;
                } else {
                    if ((month + 1) >= 10 || day > 10) {

                        if ((month + 1) < 10) {

                            sm = "0" + (month + 1);
                        } else {
                            sm = "" + (month + 1);
                        }
                        if (day <= 9) {

                            sd = "0" + (day);
                        } else {
                            sd = "" + (day);
                        }
                        start =  sd.trim() + "-" + sm + "-" +year;
                    }
                }
                conf = 1;
            }
            if (to == 1) {
                String dt = day + "-" + (month + 1) + "-" + year;
                bt.setText(dt);
                // day=day+1;
                if ((month + 1) < 10 || day < 10) {

                    if ((month + 1) < 10) {

                        sm = "0" + (month + 1);
                    } else {
                        sm = "" + (month + 1);
                    }
                    if (day < 9) {

                        sd = "0" + (day);
                    } else {
                        sd = "" + (day);
                    }
                    end =  sd.trim() + "-" + sm + "-" + year;
                } else {
                    // day=day+1;
                    if ((month + 1) >= 10 || day > 10) {
                        if ((month + 1) < 10) {

                            sm = "0" + (month + 1);
                        } else {
                            sm = "" + (month + 1);
                        }
                        if (day <= 9) {

                            sd = "0" + (day);
                        } else {
                            sd = "" + (day);
                        }
                        end =  sd.trim() + "-" + sm + "-" + year;
                    }
                }
                conto = 1;
            }
        }
    };
}
