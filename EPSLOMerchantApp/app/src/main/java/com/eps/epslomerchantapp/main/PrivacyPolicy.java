package com.eps.epslomerchantapp.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import com.eps.epslomerchantapp.R;

public class PrivacyPolicy extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        ImageView ivExit = (ImageView) findViewById(R.id.ivExit);
        if (ivExit != null)
            ivExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                    overridePendingTransition( R.anim.right_in, R.anim.right_out);
                }
            });
    }
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition( R.anim.right_in, R.anim.right_out);
    }
}
