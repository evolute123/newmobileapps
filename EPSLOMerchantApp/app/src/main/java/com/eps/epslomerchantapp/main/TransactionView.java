package com.eps.epslomerchantapp.main;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.support.ATPrintAsync;
import com.eps.epslomerchantapp.support.DatabaseHandler;
import com.eps.epslomerchantapp.support.EPSTransaction;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TransactionView extends AppCompatActivity {
    EPSTransaction trans;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_view);
        DatabaseHandler dh = new DatabaseHandler(getApplicationContext());
        String transaction_id = getIntent().getStringExtra("transaction_id");
        trans = dh.getTransaction(transaction_id);

        TextView tvtransid = (TextView) findViewById(R.id.tvtransidtext);
        tvtransid.setText(trans.getUnique_txn_id());

        TextView tvinvnum = (TextView) findViewById(R.id.tvinvnumtext);
        tvinvnum.setText(trans.getInvoice_number());

        TextView tvamt = (TextView) findViewById(R.id.tvamttext);
        tvamt.setText("Rs. " + trans.getAmount());

        TextView tvttype = (TextView) findViewById(R.id.tvttypetext);
        tvttype.setText(trans.getTransaction_type());

        TextView tvttime = (TextView) findViewById(R.id.tvttimetext);
        tvttime.setText(trans.getTransaction_timestamp("yyyy-MM-dd HH:mm:ss"));

        TextView tvptime = (TextView) findViewById(R.id.tvptimetext);
        tvptime.setText(trans.getPayment_timestamp("yyyy-MM-dd HH:mm:ss"));

        TextView tvref = (TextView) findViewById(R.id.tvreftext);
        tvref.setText(trans.getReference_no());

        TextView tvtdet = (TextView) findViewById(R.id.tvtdettext);
        TextView tvtcardno = (TextView) findViewById(R.id.tvtcardnotext);
        TextView tvtctype = (TextView) findViewById(R.id.tvtctypetext);
        TextView tvtpgid = (TextView) findViewById(R.id.tvtpgidtext);
        TableRow trdet = (TableRow) findViewById(R.id.trdet);
        TableRow trcardno = (TableRow) findViewById(R.id.trcardno);
        TableRow trctype = (TableRow) findViewById(R.id.trctype);
        TableRow trpgid = (TableRow) findViewById(R.id.trpgid);


//        String td = trans.getTransaction_details();
//        try {
//            JSONObject jo = new JSONObject(td);
//            String det ="";
//            Iterator temp = jo.keys();
//            while (temp.hasNext()) {
//                String jokey = (String) temp.next();
//                det = det + jokey +" - "+ jo.get(jokey).toString()+"\n";
//
//            }
//            td = det;
//        }catch (Exception ex)
//        {
//
//        }
//        Log.v("Transaction",td);
        if(trans.getTransaction_type().equalsIgnoreCase("cash")) {
            tvtdet.setText(trans.getTransaction_details());
            trpgid.setVisibility(View.GONE);
            trcardno.setVisibility(View.GONE);
            trctype.setVisibility(View.GONE);
        }
        else
        {
            tvtcardno.setText(trans.getPGItem("card_no"));
            tvtctype.setText(trans.getPGItem("card_type"));
            tvtpgid.setText(trans.getPGItem("id"));
            trdet.setVisibility(View.GONE);
        }

        TextView tvtstate = (TextView) findViewById(R.id.tvtstatetext);
        String state = trans.getTransaction_state().toUpperCase();
        tvtstate.setText(state.toUpperCase());
        if(state.equalsIgnoreCase("APPROVED")) {
            tvtstate.setTextColor(Color.parseColor("#A4C639"));
        }else if(state.equalsIgnoreCase("CANCELLED") || state.equalsIgnoreCase("DECLINED")) {
            tvtstate.setTextColor(Color.parseColor("#FF0000"));
        }

        TextView tvsync = (TextView) findViewById(R.id.tvsynctext);
        String sync = trans.getSync_state().toUpperCase();
        tvsync.setText(sync.toUpperCase());
        if(sync.equalsIgnoreCase("UNSYNCED")) {
            tvsync.setTextColor(Color.parseColor("#FFA500"));
        }else if(sync.equalsIgnoreCase("SUCCESS")) {
            tvsync.setTextColor(Color.parseColor("#A4C639"));
        }


        ImageView back = (ImageView) findViewById(R.id.btnBackIv);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition( R.anim.right_in, R.anim.right_out);
            }
        });

        Button printBtn = (Button) findViewById(R.id.printReceiptButton);
        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ATPrintAsync(ConnectActivity.mgp,TransactionView.this,trans,"DUPLICATE").execute();
            }
        });
    }
    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition( R.anim.right_in, R.anim.right_out);
    }
}
