package com.eps.epslomerchantapp.main;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.support.EPSTransaction;

public class TransactionItemAdapter extends BaseAdapter{
	private Activity activity;
	 private static LayoutInflater inflater=null;
	List<EPSTransaction> items;
   public TransactionItemAdapter(Activity  a, List<EPSTransaction> listitem) {
	// TODO Auto-generated constructor stub
		 activity = a;
	   inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	items = listitem;
}

@Override
	public int getCount() {
		// TODO Auto-generated method stub
		 return items.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.submitted_list_item, null);
        TextView dtitem = (TextView)vi.findViewById(R.id.date_item);
		TextView invitem = (TextView)vi.findViewById(R.id.inv_item);
        TextView amitem = (TextView)vi.findViewById(R.id.amount_item);
		TextView titem = (TextView)vi.findViewById(R.id.type_item);
		//TextView tsitem = (TextView)vi.findViewById(R.id.status_item);
		ImageView syitem = (ImageView)vi.findViewById(R.id.sync_item);

//		if(position %2==0)
//		{
//			vi.setBackgroundColor(Color.parseColor("#ffffff"));
//		}
//		else
//		{
//			vi.setBackgroundColor(Color.parseColor("#FFDEF7D9"));
//		}
        // Setting all values in listview

		dtitem.setText(items.get(position).getTransaction_timestamp("dd-MM-yyyy HH:mm"));
		invitem.setText(items.get(position).getInvoice_number());
		String amt = items.get(position).getAmount();
		amitem.setText(amt);
		titem.setText(items.get(position).getTransaction_type());

		String ts = items.get(position).getTransaction_state();
//		tsitem.setText(ts.toUpperCase());
//		if(ts.equalsIgnoreCase("paid"))
//        {
//			tsitem.setTextColor(Color.parseColor("#A4C639"));
//        }else if(ts.equalsIgnoreCase("SUCCESS"))
//        {
//			tsitem.setTextColor(Color.parseColor("#FF0000"));
//        }

		String sync = items.get(position).getSync_state();
		String state = items.get(position).getTransaction_state().toUpperCase();

		if(sync.equalsIgnoreCase("UNSYNCED"))
		{
			syitem.setImageResource(R.drawable.to_sync);
		}else if(state.equalsIgnoreCase("APPROVED")) {
			syitem.setImageResource(R.drawable.tick);
		}else if(state.equalsIgnoreCase("CANCELLED") || state.equalsIgnoreCase("DECLINED"))
		{
			syitem.setImageResource(R.drawable.cancel_red);
		}
		vi.setTag(Integer.parseInt(items.get(position).getId()));
		return vi;
	}
}
