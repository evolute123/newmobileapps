package com.eps.epslomerchantapp.support;

/**
 * Created by satish_kota on 22-08-2016.
 */
public class EPSWebURL {
    public static final String WEBSITE = "https://www.spirituspay.com/api/v1/leopard";
    public static final String VALIDATE_DEVICE = WEBSITE+"/validate_device";
    public static final String CASH_TRANSACTION = WEBSITE+"/create_cash_transaction";
    public static final String CARD_TRANSACTION = WEBSITE+"/create_card_transaction";
    public static final String GENERATE_HASH = WEBSITE+"/generate_hash";
    public static final String PAYU_SUCCESS = WEBSITE+"/payu_success";
    public static final String PAYU_FAILURE = WEBSITE+"/payu_failure";
    public static final String GET_TRANSACTION= WEBSITE + "/get_transaction_by_id";
    public static final String UPDATE_TRANSACTION = WEBSITE + "/update_transaction_by_id";
    public static final String CLIENT_CERTIFICATE_PASSWORD = "";
    public static final String CLIENT_CERTIFICATE_NAME ="";
    public static final String CERTIFICATE_NAME="";
    public static final String LO_USER_AGENT="EPSLeopardOmni/1.0";
    public static final String GEN_HASH =  WEBSITE + "/generate_hash";
    public static final String PG_SUCCESS = WEBSITE + "/payu_success";
    public static final String PG_FAILURE = WEBSITE + "/payu_failure";
    //public static final String GEN_HASH =  "https://payu.herokuapp.com/get_hash";
    //public static final String PG_SUCCESS = "https://payu.herokuapp.com/success";
    //public static final String PG_FAILURE = "https://payu.herokuapp.com/failure";
}
