package com.eps.epslomerchantapp.support;


import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	public DatabaseHandler(Context context)
			 {
				super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "leopardomni.sqlite";

	// Table Names
	private static final String TABLE_TRANSACTIONS = "Transactions";

	// Common column names
	private static final String KEY_ID = "id"; // Primary Key
	private static final String TID = "transaction_id";
	private static final String INVOICE_NUMBER= "invoice_number";
	private static final String AMOUNT = "amount";
	private static final String TRANSACTION_TYPE = "transaction_type";
	private static final String TRANSACTION_TIMESTAMP = "transaction_timestamp";
	private static final String PAYMENT_TIMESTAMP = "payment_timestamp";
	private static final String TRANSACTION_DETAILS = "transaction_details";
	private static final String TRANSACTION_DETAILS2 = "transaction_details2";
	private static final String REFERENCE_NO = "reference_no";
	private static final String TXN_ID = "txn_id";
	private static final String BANK_CODE = "bank_code";
	private static final String TRANSACTION_STATE = "transaction_state";
	private static final String SYNC_STATE = "sync_state";



	private static final String CREATE_TABLE_TRANSACTIONS= "CREATE TABLE IF NOT EXISTS "
				+ TABLE_TRANSACTIONS
				+ "("
				+ KEY_ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ TID
				+ " TEXT,"
				+ INVOICE_NUMBER
			    + " TEXT,"
				+ AMOUNT
				+ " TEXT ,"
				+ TRANSACTION_TYPE
				+ " TEXT ,"
				+ TRANSACTION_TIMESTAMP
				+ " DATE ,"
				+ PAYMENT_TIMESTAMP
				+ " DATE ,"
				+ TRANSACTION_DETAILS
				+ " TEXT ,"
			+ TRANSACTION_DETAILS2
			+ " TEXT ,"
			+ REFERENCE_NO
			+ " TEXT ,"
			+ TXN_ID
			+ " TEXT ,"
			+ BANK_CODE
			+ " TEXT ,"
			+ TRANSACTION_STATE
			+ " TEXT ,"
			+ SYNC_STATE
			+ " TEXT"
				+")";

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_TRANSACTIONS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onCreate(db);
	}

	public long createTransactions(EPSTransaction tq)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(TID,tq.getTransaction_id());
		values.put(INVOICE_NUMBER,tq.getInvoice_number());
		values.put(AMOUNT,tq.getAmount());
		values.put(TRANSACTION_TYPE,tq.getTransaction_type());
		values.put(TRANSACTION_TIMESTAMP,tq.getTransaction_timestamp(""));
		values.put(PAYMENT_TIMESTAMP,tq.getPayment_timestamp(""));
		values.put(TRANSACTION_DETAILS,tq.getTransaction_details());
		values.put(TRANSACTION_DETAILS2,tq.getFull_response());
		values.put(REFERENCE_NO,tq.getReference_no());
		values.put(TXN_ID,tq.getUnique_txn_id());
		values.put(BANK_CODE,tq.getBank());
		values.put(TRANSACTION_STATE,tq.getTransaction_state());
		values.put(SYNC_STATE,tq.getSync_state());

		long row_id = db.insert(TABLE_TRANSACTIONS, null, values);
		tq.setId(""+row_id);
		db.close();
		return row_id;
	}


	public long updateTransaction(EPSTransaction tq)
	{
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(TID,tq.getTransaction_id());
		values.put(INVOICE_NUMBER,tq.getInvoice_number());
		values.put(AMOUNT,tq.getAmount());
		values.put(TRANSACTION_TYPE,tq.getTransaction_type());
		values.put(TRANSACTION_TIMESTAMP,tq.getTransaction_timestamp(""));
		values.put(PAYMENT_TIMESTAMP,tq.getPayment_timestamp(""));
		values.put(TRANSACTION_DETAILS,tq.getTransaction_details());
		values.put(TRANSACTION_DETAILS2,tq.getFull_response());
		values.put(REFERENCE_NO,tq.getReference_no());
		values.put(TXN_ID,tq.getUnique_txn_id());
		values.put(BANK_CODE,tq.getBank());
		values.put(TRANSACTION_STATE,tq.getTransaction_state());
		values.put(SYNC_STATE,tq.getSync_state());

		long row_id = db.update(TABLE_TRANSACTIONS, values, KEY_ID + " = ?", new String[] { tq.getId()});
		db.close();
		return row_id;
	}

	public void deleteOldTransactions(String priorTo)
	{
		//DELETE TRANSACTIONS OLDER THAN 31 Days.
		SQLiteDatabase db = this.getReadableDatabase();
		String deleteQuery = "DELETE FROM "+TABLE_TRANSACTIONS+" WHERE "+SYNC_STATE+" ='SUCCESS' AND "+TRANSACTION_TIMESTAMP+" < '"+priorTo+"';";
		try {
			db.execSQL(deleteQuery);
		}
		catch(SQLException se)
		{
			se.printStackTrace();
		}
	}
	public void deleteAllTransactions()
	{
		//DELETE TRANSACTIONS OLDER THAN 31 Days.
		SQLiteDatabase db = this.getReadableDatabase();
		String deleteQuery = "DELETE FROM "+TABLE_TRANSACTIONS+";";
		try {
			db.execSQL(deleteQuery);
		}
		catch(SQLException se)
		{
			se.printStackTrace();
		}
	}
	public EPSTransaction getTransaction(String Id) {
		//changed string to long
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTIONS + " WHERE "
				+ "id" + " = " + Id;
		Cursor c = db.rawQuery(selectQuery, null);
		EPSTransaction r = null;
		if (c != null) {
			//c.moveToFirst();
			if (c.moveToFirst()) {
				r = new EPSTransaction();
				r.setId(Id);
				r.setTransaction_id(c.getString(c.getColumnIndex(TID)));
				r.setInvoice_number(c.getString(c.getColumnIndex(INVOICE_NUMBER)));
				r.setAmount(c.getString(c.getColumnIndex(AMOUNT)));
				r.setTransaction_type(c.getString(c.getColumnIndex(TRANSACTION_TYPE)));
				r.setTransaction_timestamp(c.getString(c.getColumnIndex(TRANSACTION_TIMESTAMP)));
				r.setPayment_timestamp(c.getString(c.getColumnIndex(PAYMENT_TIMESTAMP)));
				r.setTransaction_details(c.getString(c.getColumnIndex(TRANSACTION_DETAILS)));
				r.setFull_response(c.getString(c.getColumnIndex(TRANSACTION_DETAILS2)));
				r.setReference_no(c.getString(c.getColumnIndex(REFERENCE_NO)));
				r.setUnique_txn_id(c.getString(c.getColumnIndex(TXN_ID)));
				r.setBank(c.getString(c.getColumnIndex(BANK_CODE)));
				r.setTransaction_state(c.getString(c.getColumnIndex(TRANSACTION_STATE)));
				r.setSync_state(c.getString(c.getColumnIndex(SYNC_STATE)));
			}
		}
		db.close();
		return r;
	}

	public List<EPSTransaction> getAllTransactions() {

		List<EPSTransaction> transQ = new ArrayList<EPSTransaction>();
		String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTIONS;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				EPSTransaction r = new EPSTransaction();
				int id = c.getInt(c.getColumnIndex(KEY_ID));
				r.setId(""+id);
				r.setTransaction_id(c.getString(c.getColumnIndex(TID)));
				r.setInvoice_number(c.getString(c.getColumnIndex(INVOICE_NUMBER)));
				r.setAmount(c.getString(c.getColumnIndex(AMOUNT)));
				r.setTransaction_type(c.getString(c.getColumnIndex(TRANSACTION_TYPE)));
				r.setTransaction_timestamp(c.getString(c.getColumnIndex(TRANSACTION_TIMESTAMP)));
				r.setPayment_timestamp(c.getString(c.getColumnIndex(PAYMENT_TIMESTAMP)));
				r.setTransaction_details(c.getString(c.getColumnIndex(TRANSACTION_DETAILS)));
				r.setFull_response(c.getString(c.getColumnIndex(TRANSACTION_DETAILS2)));
				r.setReference_no(c.getString(c.getColumnIndex(REFERENCE_NO)));
				r.setUnique_txn_id(c.getString(c.getColumnIndex(TXN_ID)));
				r.setBank(c.getString(c.getColumnIndex(BANK_CODE)));
				r.setTransaction_state(c.getString(c.getColumnIndex(TRANSACTION_STATE)));
				r.setSync_state(c.getString(c.getColumnIndex(SYNC_STATE)));
				transQ.add(r);
			} while (c.moveToNext());
		}
		db.close();
		return transQ;
	}

	public List<EPSTransaction> getTransactionsBySQL(String wheresql)
	{
		List<EPSTransaction> transQ = new ArrayList<EPSTransaction>();
		String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTIONS + wheresql;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				EPSTransaction r = new EPSTransaction();
				int id = c.getInt(c.getColumnIndex(KEY_ID));
				r.setId(""+id);
				r.setTransaction_id(c.getString(c.getColumnIndex(TID)));
				r.setInvoice_number(c.getString(c.getColumnIndex(INVOICE_NUMBER)));
				r.setAmount(c.getString(c.getColumnIndex(AMOUNT)));
				r.setTransaction_type(c.getString(c.getColumnIndex(TRANSACTION_TYPE)));
				r.setTransaction_timestamp(c.getString(c.getColumnIndex(TRANSACTION_TIMESTAMP)));
				r.setPayment_timestamp(c.getString(c.getColumnIndex(PAYMENT_TIMESTAMP)));
				r.setTransaction_details(c.getString(c.getColumnIndex(TRANSACTION_DETAILS)));
				r.setFull_response(c.getString(c.getColumnIndex(TRANSACTION_DETAILS2)));
				r.setReference_no(c.getString(c.getColumnIndex(REFERENCE_NO)));
				r.setUnique_txn_id(c.getString(c.getColumnIndex(TXN_ID)));
				r.setBank(c.getString(c.getColumnIndex(BANK_CODE)));
				r.setTransaction_state(c.getString(c.getColumnIndex(TRANSACTION_STATE)));
				r.setSync_state(c.getString(c.getColumnIndex(SYNC_STATE)));
				transQ.add(r);
			} while (c.moveToNext());
		}
		db.close();
		return transQ;
	}

	public List<EPSTransaction> getTransactionsByDate(String dtStart, String dtEnd) {

		List<EPSTransaction> transQ = new ArrayList<EPSTransaction>();
		String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTIONS + " where " +TRANSACTION_TIMESTAMP+ " between '" + dtStart+ " 00:00:00' and '" + dtEnd +" 23:59:59';";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				EPSTransaction r = new EPSTransaction();
				int id = c.getInt(c.getColumnIndex(KEY_ID));
				r.setId(""+id);
				r.setTransaction_id(c.getString(c.getColumnIndex(TID)));
				r.setInvoice_number(c.getString(c.getColumnIndex(INVOICE_NUMBER)));
				r.setAmount(c.getString(c.getColumnIndex(AMOUNT)));
				r.setTransaction_type(c.getString(c.getColumnIndex(TRANSACTION_TYPE)));
				r.setTransaction_timestamp(c.getString(c.getColumnIndex(TRANSACTION_TIMESTAMP)));
				r.setPayment_timestamp(c.getString(c.getColumnIndex(PAYMENT_TIMESTAMP)));
				r.setTransaction_details(c.getString(c.getColumnIndex(TRANSACTION_DETAILS)));
				r.setFull_response(c.getString(c.getColumnIndex(TRANSACTION_DETAILS2)));
				r.setReference_no(c.getString(c.getColumnIndex(REFERENCE_NO)));
				r.setUnique_txn_id(c.getString(c.getColumnIndex(TXN_ID)));
				r.setBank(c.getString(c.getColumnIndex(BANK_CODE)));
				r.setTransaction_state(c.getString(c.getColumnIndex(TRANSACTION_STATE)));
				r.setSync_state(c.getString(c.getColumnIndex(SYNC_STATE)));
				transQ.add(r);
			} while (c.moveToNext());
		}
		db.close();
		return transQ;
	}

	public List<EPSTransaction> getUnsyncedTransactions() {

		List<EPSTransaction> transQ = new ArrayList<EPSTransaction>();
		String selectQuery = "SELECT  * FROM " + TABLE_TRANSACTIONS + " where sync_state='UNSYNCED';";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				EPSTransaction r = new EPSTransaction();
				int id = c.getInt(c.getColumnIndex(KEY_ID));
				r.setId(""+id);
				r.setTransaction_id(c.getString(c.getColumnIndex(TID)));
				r.setInvoice_number(c.getString(c.getColumnIndex(INVOICE_NUMBER)));
				r.setAmount(c.getString(c.getColumnIndex(AMOUNT)));
				r.setTransaction_type(c.getString(c.getColumnIndex(TRANSACTION_TYPE)));
				r.setTransaction_timestamp(c.getString(c.getColumnIndex(TRANSACTION_TIMESTAMP)));
				r.setPayment_timestamp(c.getString(c.getColumnIndex(PAYMENT_TIMESTAMP)));
				r.setTransaction_details(c.getString(c.getColumnIndex(TRANSACTION_DETAILS)));
				r.setFull_response(c.getString(c.getColumnIndex(TRANSACTION_DETAILS2)));
				r.setReference_no(c.getString(c.getColumnIndex(REFERENCE_NO)));
				r.setUnique_txn_id(c.getString(c.getColumnIndex(TXN_ID)));
				r.setBank(c.getString(c.getColumnIndex(BANK_CODE)));
				r.setTransaction_state(c.getString(c.getColumnIndex(TRANSACTION_STATE)));
				r.setSync_state(c.getString(c.getColumnIndex(SYNC_STATE)));
				transQ.add(r);
			} while (c.moveToNext());
		}
		db.close();
		return transQ;
	}

	// closing database
		public void closeDB() {
			SQLiteDatabase db = this.getReadableDatabase();
			if (db != null && db.isOpen())
				db.close();
		}
}
