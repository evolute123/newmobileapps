package com.eps.epslomerchantapp.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.bluetooth.BluetoothComm;
import com.eps.epslomerchantapp.support.Utility;
import com.leopard.api.MagCard;
import com.leopard.api.Printer;
import com.leopard.api.Setup;

import java.lang.reflect.GenericArrayType;
import java.util.Arrays;
import java.util.List;

public class ConnectActivity extends AppCompatActivity {
    private BluetoothComm mBTcomm = null;
    String sBtname = "00:06:66:4D:20:54";//"00:04:3E:94:B2:6A";
    public static BluetoothAdapter mBT = BluetoothAdapter.getDefaultAdapter();
    private String btnTOopen;
    public static GlobalPool mgp=null;
    String sMac;
    Context context=this;
    private boolean _active = true;
    private int _splashTime = 1000;
    private static final int SPLASH_THREAD = 0;
    public static BluetoothAdapter mBtAdapter;
    int iRetVal=0;
    private String DEVICE_NAME = "ESYS04650";
    private static String TAG = "HardwareTest";
    public static Setup setup;
    public static Printer printer;
    public static MagCard magCard;
    private static boolean mSetup = false;
    SharedPreferences mPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
        mPref = getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);
        mgp = ((GlobalPool)this.getApplicationContext());
        mSetup = false;
        if (mPref.getString("BT", "").length() == 0) {
            mSetup = true;
            final Dialog dlgBT = new Dialog(ConnectActivity.this);
            dlgBT.setCanceledOnTouchOutside(false);
            dlgBT.setCancelable(false);
            dlgBT.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dlgBT.setContentView(R.layout.bt_view);
            final EditText et1 = (EditText) dlgBT.findViewById(R.id.etbt1);
            et1.addTextChangedListener(new TextWatcher() {
                private static final int TOTAL_SYMBOLS = 17; // size of pattern 00:00:00:00:00:00
                private static final int TOTAL_DIGITS = 12; // max numbers of digits in pattern: 00 x 6
                private static final int DIVIDER_MODULO = 3; // means divider position is every 5th symbol beginning with 1
                private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
                private static final char DIVIDER = ':';
                private Character[] chars = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
                private List<Character> clist = Arrays.asList(chars);
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // noop
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    // noop
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
                        s.replace(0, s.length(), buildCorrecntString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
                    }
                }

                private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
                    boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
                    for (int i = 0; i < s.length(); i++) { // chech that every element is right
                        if (i > 0 && (i + 1) % dividerModulo == 0) {
                            isCorrect &= divider == s.charAt(i);
                        } else {
                            isCorrect &= clist.contains(s.charAt(i));
                        }
                    }
                    return isCorrect;
                }

                private String buildCorrecntString(char[] digits, int dividerPosition, char divider) {
                    final StringBuilder formatted = new StringBuilder();

                    for (int i = 0; i < digits.length; i++) {
                        if (digits[i] != 0) {
                            formatted.append(digits[i]);
                            if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
                                formatted.append(divider);
                            }
                        }
                    }

                    return formatted.toString();
                }

                private char[] getDigitArray(final Editable s, final int size) {
                    char[] digits = new char[size];
                    int index = 0;

                    for (int i = 0; i < s.length() && index < size; i++) {
                        char current = s.charAt(i);
                        if (clist.contains(current)) {
                            digits[index] = current;
                            index++;
                        }
                    }
                    return digits;
                }
            });


                    //dlgMPIN.setTitle(getResources().getString(R.string.Trans_USSD_Popup_for_multi_banks_title));
            // Prepare grid view
            Button btnOK = (Button) dlgBT.findViewById(R.id.btnOK);
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String sbt1 = et1.getText().toString();

                    if(sbt1.isEmpty() && sbt1.length() != 17) {
                        Utility.alert("Input Error","All BT fields should be 2 character Hexa Decimal Number only.",ConnectActivity.this);
                    }else{
                        sMac = sbt1;
                        ConnecAsync connectasync = new ConnecAsync(dlgBT);
                        connectasync.execute();
                    }
                }
            });

            Button btnCancel = (Button) dlgBT.findViewById(R.id.btnCancel);

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    dlgBT.dismiss();
                                    finish();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    };
                    Utility.dlgYesNo("", "Are you sure you want to exit the setup process?", dialogClickListener, ConnectActivity.this);
                }
            });
            dlgBT.show();
        }
        else {
            sMac = mPref.getString("BT", "");
            ConnecAsync connectasync = new ConnecAsync(null);
            connectasync.execute();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Get Result from USSD
        if (requestCode==101 || requestCode == 121 || requestCode == 111 || requestCode == 131) {
            mgp.closeConn();
            finish();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) { //TODO
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            mgp.closeConn();
           finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                        ConnecAsync connectasync = new ConnecAsync(null);
                        connectasync.execute();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                        finish();
                    break;
            }
        }
    };

    private class ConnecAsync extends AsyncTask<String, String, String> {
        private ProgressDialog mpd = null;
        private Dialog dlg;
        public ConnecAsync(Dialog dlg)
        {
            this.dlg = dlg;
        }
        @Override
        public void onPreExecute() {
            this.mpd = new ProgressDialog(ConnectActivity.this);
            this.mpd.setMessage("Initiating Bluetooth. Please Wait...");
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.show();
        }

       @Override
        protected void onProgressUpdate(String... progUpdate) {
            super.onProgressUpdate(progUpdate);
            this.mpd.setMessage(progUpdate[0]);
        }
        @Override
        protected String doInBackground(String... arg0) {
            Log.e(TAG, "Do in background ConnectAsync");
            //	mgp = ((GlobalPool)this.getClass());
            try{
                BluetoothAdapter  mBtAdapter = BluetoothAdapter.getDefaultAdapter();
                if(!mBtAdapter.isEnabled())
                    mBtAdapter.enable();
                mgp.createConn(sMac);
                if (mgp.isConnect()) {
                    Log.e(TAG, "Bluetooth is  Connected");
                    //progress.setMessage("Bluetooth Connected and Activating the Setup class");
                    SystemClock.sleep(100);
                    publishProgress("Bluetooth Connected and Activating the Setup class");
                    mgp.onCreate(context);
                    Log.e(TAG, "Activation of Library is Done");
                }else{
                    //progress.dismiss();
                    return "FAILED";
                }
            }catch (Exception e) {
                return "FAILED";
            }
            //progress.dismiss();
            return "SUCCESS";
        }


        @Override
        public void onPostExecute(String result){
            if (result.equalsIgnoreCase("FAILED")){
                if(!mPref.getString("BT", "").isEmpty()) {
                    Utility.dlgYesNo("BLUETOOTH SETUP FAILED","Failed to connect to Bluetooth. Please check if you have switched on the device or Contact Support. Do you want to retry?",dialogClickListener,ConnectActivity.this);
                }
                else
                    Utility.alert("BLUETOOTH SETUP FAILED","Failed to connect to Bluetooth. Please check the Bluetooth Mac Address of the device or Contact Support.",ConnectActivity.this);
            } else {
                if(this.dlg != null)
                    this.dlg.dismiss();
                if(mPref.getString("BT","").isEmpty())
                {
                    String hsNumber = "";
                    String tsNumber = "";
                    String phoneNumber = "";
                    //Get HSN
                    if (mgp.isConnect()) {
                        hsNumber = mgp.getHardwareSerialNumber();
                    }
                    try {
                        //Get TSN
                        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        String deviceId = tm.getDeviceId();
                        if (deviceId != null) {
                            tsNumber = deviceId;
                        } else {
                            tsNumber = android.os.Build.SERIAL;
                        }

                        //Get PHONE
                        phoneNumber = tm.getLine1Number();
                        if(phoneNumber == null)
                            phoneNumber = "";
                    } catch (Exception ex) {

                    }

                    SharedPreferences.Editor edit = mPref.edit();
                    edit.putString("BT", sMac);
                    edit.putString("HSN", hsNumber);
                    edit.putString("TSN", tsNumber);
                    edit.putString("PHONE", phoneNumber);
                    edit.apply();
                }

                if(mSetup)
                {
                    startActivityForResult(new Intent(ConnectActivity.this, HardwareTest.class), 101);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
                else
                {
                    if (mPref.getString("tid", "").length() == 0) {
                        startActivityForResult(new Intent(ConnectActivity.this, ValidateDevice.class),111);
                        overridePendingTransition( R.anim.left_in, R.anim.left_out);
                    } else if (!mPref.getString("deviceStatus", "").equalsIgnoreCase("active")) {
                        startActivityForResult(new Intent(ConnectActivity.this, DeviceDetails.class),121);
                        overridePendingTransition( R.anim.left_in, R.anim.left_out);
                    } else {
                        startActivityForResult(new Intent(ConnectActivity.this, MainScreen.class), 131);
                        overridePendingTransition( R.anim.left_in, R.anim.left_out);
                    }
                }

            }
            if (this.mpd.isShowing())
                this.mpd.dismiss();
        }

    }
}
