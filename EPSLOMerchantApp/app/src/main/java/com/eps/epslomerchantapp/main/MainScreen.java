package com.eps.epslomerchantapp.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.eps.epslomerchantapp.R;

public class MainScreen extends AppCompatActivity implements View.OnClickListener{

    TextView termIDContentTv, deviceNameContentTv,merchantNameTV;
    Button startTransactionsButton,showTransationsButton;
    ImageView settingsIv,btnExitIv;
    SharedPreferences mPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        startTransactionsButton = (Button) findViewById(R.id.startTransactionsButton);
        showTransationsButton = (Button) findViewById(R.id.showTransationsButton);
        showTransationsButton.setOnClickListener(this);
        startTransactionsButton.setOnClickListener(this);

        settingsIv = (ImageView) findViewById(R.id.settingsIv);
        settingsIv.setOnClickListener(this);

        btnExitIv = (ImageView) findViewById(R.id.btnExitIv);
        btnExitIv.setOnClickListener(this);

        termIDContentTv = (TextView)findViewById(R.id.termIDContentTv);
        deviceNameContentTv = (TextView) findViewById(R.id.deviceNameContentTv);
        merchantNameTV = (TextView) findViewById(R.id.merchantNameTV);

        mPref = getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);

    }

    @Override
    public void onStart()
    {
        super.onStart();
        termIDContentTv.setText(mPref.getString("tid",""));
        deviceNameContentTv.setText(mPref.getString("displayName",""));
        merchantNameTV.setText(mPref.getString("merchantName",""));
    }

    @Override
    public void onBackPressed() {
        exit();
    }


    @Override
    public void onClick(View view) {
        if(view == settingsIv)
        {
            startActivity(new Intent(MainScreen.this, DeviceDetails.class));
            overridePendingTransition( R.anim.left_in, R.anim.left_out);
        }
        else if(view == btnExitIv)
        {
         exit();
        }
        else if(view == showTransationsButton)
        {
           startActivity(new Intent(MainScreen.this, TransactionSelector.class));
            overridePendingTransition( R.anim.left_in, R.anim.left_out);
        }
        else if(view == startTransactionsButton)
        {
            startActivity(new Intent(MainScreen.this, StartTransaction.class));
            overridePendingTransition( R.anim.left_in, R.anim.left_out);
        }
    }

    private void exit()
    {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        setResult(131);
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(MainScreen.this);
        builder.setMessage(R.string.ms_exit_msg).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }
}
