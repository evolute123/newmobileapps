package com.eps.epslomerchantapp.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.support.ATValidateDevice;
import com.eps.epslomerchantapp.support.Utility;
import com.eps.epslomerchantapp.support.EPSConstant;
import com.eps.epslomerchantapp.support.EPSServerCaller;
import com.eps.epslomerchantapp.support.EPSWebURL;

import org.json.JSONObject;

import java.util.HashMap;

public class ValidateDevice extends AppCompatActivity implements View.OnClickListener{

    EditText terminalEditText;
    Button validateButton;
    ImageView btnBackIv,btnExitIv;
    SharedPreferences mPref;
    TextView title_validate,tvHeading;
    Boolean mSetup = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_device);

        terminalEditText = (EditText) findViewById(R.id.terminalEditText);
        validateButton = (Button) findViewById(R.id.btn_search);
        btnBackIv = (ImageView) findViewById(R.id.btnBackIv);
        btnExitIv = (ImageView) findViewById(R.id.btnExitIv);

        validateButton.setOnClickListener(this);
        btnBackIv.setOnClickListener(this);
        btnExitIv.setOnClickListener(this);
        mPref = getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);
        String tid = mPref.getString("tid", "");
        title_validate = (TextView) findViewById(R.id.activityTitle);
        tvHeading = (TextView)findViewById(R.id.terminalTextView);
        if(tid.isEmpty()) {
            btnBackIv.setVisibility(View.GONE);
            mSetup = true;
        }
        else {
            btnExitIv.setVisibility(View.GONE);
            title_validate.setText(R.string.vd_title_change);
            tvHeading.setText(R.string.vd_new_tid_label);
        }
    }

    @Override
    public void onBackPressed() {
       exit();
    }
    @Override
    public void onClick(View view) {
        if (view == validateButton) {
            String et = terminalEditText.getText().toString();
            if (et.isEmpty() ) {
                Utility.alert("Error!", "Enter Terminal ID associated to the device. Refer to your Installation Kit or Contact Support.", ValidateDevice.this);
            }
            else {
                et = et.toUpperCase();
                //Validate Terminal ID
                if (Utility.isNetWorkAvailable(ValidateDevice.this)) {
                    new ATValidateDevice(ValidateDevice.this, ValidateDevice.this, et, "ValidateDevice").execute();
                } else {
                    Utility.alert("Network Error", "Please check the internet connection. Unable to proceed executing the transaction on this device.", ValidateDevice.this);
                }
            }
        } else if (view == btnBackIv) {
            exit();

        } else if (view == btnExitIv)
        {
            exit();
        }
    }

    private void exit(){
        if(mSetup) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            setResult(111);
                            finish();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(ValidateDevice.this);
            builder.setMessage(R.string.vd_exit_msg).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }
        else {
            finish();
            overridePendingTransition( R.anim.right_in, R.anim.right_out);
        }
    }

}
