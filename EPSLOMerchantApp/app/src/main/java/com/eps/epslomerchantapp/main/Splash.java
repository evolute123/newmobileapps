package com.eps.epslomerchantapp.main;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.bluetooth.BluetoothComm;
import com.eps.epslomerchantapp.support.Utility;
import com.leopard.api.MagCard;
import com.leopard.api.Printer;
import com.leopard.api.Setup;

import java.util.Set;

public class Splash extends AppCompatActivity {
    private boolean _active = true;
    private int _splashTime = 1000;
    private static final int SPLASH_THREAD = 0;

    public static BluetoothComm mBT;
    public static BluetoothAdapter mBtAdapter;
    int iRetVal=0;
    private String DEVICE_NAME = "ESYS04650";
    private static String TAG = "HardwareTest";
    public static Setup setup;
    public static Printer printer;
    public static MagCard magCard;
    private static boolean mSetup = false;
    SharedPreferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        Message mMessage = new Message();
        mMessage.what = Splash.SPLASH_THREAD;
        mPref = getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);
        //DEVICE_NAME = mPref.getString("BT", "");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Splash.this, ConnectActivity.class);
                startActivity(i);
                finish();
            }
        }, 3000);
    }
}
