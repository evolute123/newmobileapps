package com.eps.epslomerchantapp.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.eps.epslomerchantapp.R;
import com.eps.epslomerchantapp.support.ATValidateDevice;
import com.eps.epslomerchantapp.support.Utility;
import com.eps.epslomerchantapp.support.EPSConstant;
import com.eps.epslomerchantapp.support.EPSServerCaller;
import com.eps.epslomerchantapp.support.EPSWebURL;

import org.json.JSONObject;

import java.util.HashMap;

public class DeviceDetails extends AppCompatActivity implements View.OnClickListener{


    Button btnDeviceValidation,btnMaintenance;
    ImageView btnBackIv,btnInfoIv,btnExitIv;
    TextView terminalIdContentTv,deviceSerialContentTv, tabletSerialTvContent,simPhoneContentTv;
    TextView merchantdIdContentTv,merchantdNameContentTv,statusContentTv,displayNameContentTv;
    SharedPreferences mPref;
    boolean mInactive = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_details);

        btnBackIv = (ImageView) findViewById(R.id.btnBackIv);
        btnBackIv.setOnClickListener(this);

        btnExitIv = (ImageView) findViewById(R.id.btnExitIv);
        btnExitIv.setOnClickListener(this);

        btnInfoIv = (ImageView) findViewById(R.id.btnInfoIv);
        btnInfoIv.setOnClickListener(this);

        btnDeviceValidation = (Button) findViewById(R.id.btnDeviceValidation);
        btnDeviceValidation.setOnClickListener(this);

        btnMaintenance = (Button) findViewById(R.id.btnMaintenance);
        btnMaintenance.setOnClickListener(this);

        mPref = getSharedPreferences("DevicePreferences", Context.MODE_PRIVATE);

        terminalIdContentTv = (TextView)findViewById(R.id.terminalIdContentTv);
        deviceSerialContentTv = (TextView)findViewById(R.id.deviceSerialContentTv);
        tabletSerialTvContent = (TextView)findViewById(R.id.tabletSerialTvContent);
        simPhoneContentTv = (TextView)findViewById(R.id.simPhoneContentTv);
        merchantdIdContentTv = (TextView)findViewById(R.id.merchantdIdContentTv);
        merchantdNameContentTv = (TextView)findViewById(R.id.merchantNameContentTv);
        statusContentTv = (TextView)findViewById(R.id.statusContentTv);
        displayNameContentTv = (TextView)findViewById(R.id.displayNameContentTv);


    }

    @Override
    public void onStart()
    {
        super.onStart();
        terminalIdContentTv.setText(mPref.getString("tid",""));
        deviceSerialContentTv.setText(mPref.getString("hardwareSerial",""));
        tabletSerialTvContent.setText(mPref.getString("tabletSerial",""));
        simPhoneContentTv.setText(mPref.getString("phoneNumber",""));
        merchantdIdContentTv.setText(mPref.getString("merchantID",""));
        merchantdNameContentTv.setText(mPref.getString("merchantName",""));
        statusContentTv.setText(mPref.getString("deviceStatus","").toUpperCase());
        displayNameContentTv.setText(mPref.getString("displayName",""));
        mInactive = !mPref.getString("deviceStatus","").equalsIgnoreCase("active");
        if(mInactive) {
            btnBackIv.setVisibility(View.GONE);
            btnExitIv.setVisibility(View.VISIBLE);
        }
        else {
            btnBackIv.setVisibility(View.VISIBLE);
            btnExitIv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        exit();
    }


    @Override
    public void onClick(View view) {
       if(view == btnBackIv)
       {
          exit();
       }
       else if(view == btnExitIv)
       {
           exit();
       }
       else if(view == btnInfoIv)
       {
           startActivity(new Intent(DeviceDetails.this, Information.class));
           overridePendingTransition( R.anim.left_in, R.anim.left_out);
       }
        else if (view == btnDeviceValidation)
       {
           new ATValidateDevice(DeviceDetails.this, DeviceDetails.this,mPref.getString("tid",""),"DeviceDetails").execute();
       }
        else if(view == btnMaintenance)
       {
            createPasswordDialog();
       }
    }
private void exit()
{
    if(mInactive) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        setResult(121);
                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(DeviceDetails.this);
        builder.setMessage(R.string.dd_exit_msg).setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
    else {
        finish();
        overridePendingTransition( R.anim.right_in, R.anim.right_out);
    }
}
    private void createPasswordDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Authentication Required");
        builder.setMessage("Enter Password to access the Maintenance section");

        // Use an EditText view to get user input.
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        //input.setBackgroundResource(R.drawable.btn_lightgray);
        input.setPadding(30,10,30,10);
        input.setTransformationMethod(PasswordTransformationMethod.getInstance());
        input.setId(0);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                if(true) //(value.equals("EPSLEOPARD"))
                {
                    startActivity(new Intent(DeviceDetails.this, HardwareTest.class));
                    overridePendingTransition( R.anim.left_in, R.anim.left_out);
                }
                else
                {
                    input.setText("");
                    Utility.alert("Wrong Password","The Password you have entered is wrong. Please retype your password", DeviceDetails.this);
                }
                return;
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.show();
    }
}
