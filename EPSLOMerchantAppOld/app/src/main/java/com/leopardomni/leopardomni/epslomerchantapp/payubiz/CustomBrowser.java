package com.leopardomni.leopardomni.epslomerchantapp.payubiz;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.payu.custombrowser.CBActivity;
import com.payu.custombrowser.PayUCustomBrowserCallback;
import com.payu.custombrowser.bean.CustomBrowserConfig;
import com.payu.custombrowser.bean.CustomBrowserData;

/**
 * Created by heurion on 2/8/16.
 */
public class CustomBrowser {
    public CustomBrowser() {
    }

    public void addCustomBrowser(Activity activity, @NonNull CustomBrowserConfig cbCustomBrowserConfig, @NonNull PayUCustomBrowserCallback cbPayUCustomBrowserCallback) {
        CustomBrowserData.SINGLETON.setPayuCustomBrowserCallback(cbPayUCustomBrowserCallback);
        CustomBrowserData.SINGLETON.setPayuCustomBrowserConfig(cbCustomBrowserConfig);
        Intent intent = new Intent(activity, CBActivity.class);
        activity.startActivity(intent);
    }
}
