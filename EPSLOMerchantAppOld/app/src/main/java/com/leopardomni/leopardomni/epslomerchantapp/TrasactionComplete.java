package com.leopardomni.leopardomni.epslomerchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TrasactionComplete extends AppCompatActivity {

    Button printBtn, closeBtn;
    EditText etTerminal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trasaction_complete);

        printBtn = (Button) findViewById(R.id.printReceiptButton);
        closeBtn = (Button) findViewById(R.id.closeButton);


        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(TrasactionComplete.this, "Printing ....", Toast.LENGTH_SHORT).show();
                Intent transComplete = new Intent(TrasactionComplete.this, Transactions.class);
                startActivity(transComplete);
            }
        });

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(TrasactionComplete.this, "Closing ....", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
