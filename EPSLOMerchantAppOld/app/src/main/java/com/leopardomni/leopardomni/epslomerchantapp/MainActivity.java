package com.leopardomni.leopardomni.epslomerchantapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.leopardomni.leopardomni.epslomerchantapp.Server.Utility;

public class MainActivity extends AppCompatActivity {

    EditText etTerminal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_main);
        Button validateBtn = (Button) findViewById(R.id.validateButton);
        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etTerminal.getText().toString().isEmpty()) {

                    Utility.alert("Empty Field", "Terminal ID cannot be empty", MainActivity.this);
//                    AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
//                    alert.setTitle("Empty Field");
//                    alert.setMessage("Terminal ID cannot be empty");
//                    alert.setPositiveButton("OK", null);
//                    alert.show();
                } else {
                    new ValidateDevice().execute();
                }
            }
        });

        etTerminal = (EditText) findViewById(R.id.terminalEditText);


        SharedPreferences tmppref = getSharedPreferences("TmpPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor etmp = tmppref.edit();
        etmp.putString("tid", etTerminal.getText().toString());
        etmp.apply();


    }

    public class ValidateDevice extends AsyncTask<String, String, String> {

        private ProgressDialog mpd = null;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.mpd = new ProgressDialog(MainActivity.this);
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.show();

        }

        @Override
        protected void onProgressUpdate(String... progUpdate) {
            super.onProgressUpdate(progUpdate);
            this.mpd.setMessage(progUpdate[0]);
        }

        @Override
        protected String doInBackground(String... voids) {

            publishProgress("Validating Device ...");

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            mpd.dismiss();
            Intent intentDeviceDetails = new Intent(MainActivity.this, Devicedetails.class);
            startActivity(intentDeviceDetails);
        }
    }

}

