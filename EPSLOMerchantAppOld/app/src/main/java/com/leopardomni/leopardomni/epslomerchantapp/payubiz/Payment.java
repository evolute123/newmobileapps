package com.leopardomni.leopardomni.epslomerchantapp.payubiz;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.leopard.api.MagCard;
import com.leopard.api.Printer;
import com.leopard.api.Setup;
import com.leopardomni.leopardomni.epslomerchantapp.bluetooth.BluetoothComm;
import com.payu.india.Extras.PayUChecksum;
import com.payu.india.Interfaces.PaymentRelatedDetailsListener;
import com.payu.india.Interfaces.ValueAddedServiceApiListener;
import com.payu.india.Model.MerchantWebService;
import com.payu.india.Model.PaymentParams;
import com.payu.india.Model.PayuConfig;
import com.payu.india.Model.PayuHashes;
import com.payu.india.Model.PayuResponse;
import com.payu.india.Model.PostData;
import com.payu.india.Payu.Payu;
import com.payu.india.Payu.PayuConstants;
import com.payu.india.Payu.PayuErrors;
import com.payu.india.Payu.PayuUtils;
import com.payu.india.PostParams.MerchantWebServicePostParams;
import com.payu.india.PostParams.PaymentPostParams;
import com.payu.india.Tasks.GetPaymentRelatedDetailsTask;
import com.payu.india.Tasks.ValueAddedServiceTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;

public class Payment extends AppCompatActivity implements PaymentRelatedDetailsListener, ValueAddedServiceApiListener, View.OnClickListener {

    static BluetoothAdapter mBtAdapter;
    private static String TAG = "Payment";
    private static Setup setup;
    EditText etMKey;
    EditText etCC, etMonth, etYear, etName, etCVV;
    Button btnSubmit, btnReadMag;
    SharedPreferences shm, shcc;
    BluetoothComm btcomm;
    int iRetVal = 0;
    Boolean smsPermission = false;
    //PAYUBASEACTIVITY VARIABLES
    PayuUtils mPayuUtils;
    PayuHashes mPayuHashes;
    PayuResponse valueAddedResponse;
    PayuResponse mPayuResponse;
    ValueAddedServiceTask valueAddedServiceTask;
    // Payments Activiy
    String url;
    String txnId = null;
    String merchantKey;
    private String DEVICE_NAME = "ESYS04650";
    private Printer prn;
    private MagCard mag;
    private String mkey;
    private String cc, name, month, year, cvv;
    private String ccname = "testuser";
    private String pg = "CC";
    private String device_type = "1";
    private String bankcode = "CC";
    //MAIN ACTIVITY VARIABLES
    //Intent intent;
    private PaymentParams mPaymentParams;
    private PayuConfig payuConfig;
    private PayUChecksum checksum;
    private PostData postData;
    private int storeOneClickHash;
    private PostData mPostData;
    private boolean viewPortWide = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        etMKey = (EditText) findViewById(R.id.etMKey);
        etCC = (EditText) findViewById(R.id.etCC);
        etName = (EditText) findViewById(R.id.etName);
        etMonth = (EditText) findViewById(R.id.etMonth);
        etYear = (EditText) findViewById(R.id.etYear);
        etCVV = (EditText) findViewById(R.id.etCVV);

        shm = getSharedPreferences("MerchantDetais", MODE_PRIVATE);
        shcc = getSharedPreferences("CCDetails", MODE_PRIVATE);

        etMKey.setText(shm.getString("MKEY", "gtKFFx"));
        etCC.setText(shcc.getString("CC", "5123456789012346"));
        etName.setText(shcc.getString("Name", "Some Card Name"));
        etMonth.setText(shcc.getString("Month", "05"));
        etYear.setText(shcc.getString("Year", "2017"));
        etCVV.setText(shcc.getString("CVV", "123"));

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);

        btnReadMag = (Button) findViewById(R.id.btnReadMag);
        btnReadMag.setOnClickListener(this);

        try {
            // Get the local Bluetooth adapter
            mBtAdapter = BluetoothAdapter.getDefaultAdapter();

            // If the adapter is null, then Bluetooth is not supported
            if (mBtAdapter == null) {
                Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_SHORT).show();
                //finish();
                //return;
            }
            setup = new Setup();
            boolean blActivated = setup.blActivateLibrary(Payment.this, R.raw.licence_heurion);
            if (blActivated)
                Log.e(TAG, "Leopard SDK Activated");
            else
                Log.e(TAG, "Leopard SDK NOT Activated");

            if (btcomm != null && btcomm.isConnect()) {
                btcomm.closeConn();// if connected close existing connection
            }

            //PayU Codebase
            Payu.setInstance(this);


        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mBtAdapter != null && mBtAdapter.isEnabled() && btcomm != null && btcomm.isConnect()) {
            //No Actions
        } else {
            new StartScanConnectTask().execute();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnReadMag) {
            readMacCard();
        } else if (view == btnSubmit) {
            executeTransaction();
        }
    }

    private void executeTransaction() {

        mkey = etMKey.getText().toString();
        cc = etCC.getText().toString();
        name = etName.getText().toString();
        month = etMonth.getText().toString();
        year = etYear.getText().toString();
        cvv = etCVV.getText().toString();

        if (mkey.isEmpty() || cc.isEmpty() || name.isEmpty() || month.isEmpty() || year.isEmpty() || cvv.isEmpty()) {
            new AlertDialog.Builder(Payment.this)
                    .setTitle("Error")
                    .setMessage("All Items are mandatory.")
                    .setPositiveButton("OK", null)
                    .show();
        } else {
            SharedPreferences.Editor edm = shm.edit();
            edm.putString("MKEY", mkey);
            edm.commit();

            SharedPreferences.Editor edcc = shcc.edit();
            edcc.putString("CC", cc);
            edcc.putString("Name", name);
            edcc.putString("Month", month);
            edcc.putString("Year", year);
            edcc.putString("CVV", cvv);
            edcc.commit();

            navigateToBaseActivity();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Get the date here from JSON
        if (requestCode == PayuConstants.PAYU_REQUEST_CODE) {
            if (data != null) {
                String message = "Payu's Data : " + data.getStringExtra("payu_response") + "\n\n\n Merchant's Data: " + data.getStringExtra("result");
                //Print Message
                printMessage(message);
            } else {
                Toast.makeText(this, "Could not receive data", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void navigateToBaseActivity() {
        mPaymentParams = new PaymentParams();
        payuConfig = new PayuConfig();
        mPaymentParams.setKey(mkey);
        mPaymentParams.setAmount("10");
        mPaymentParams.setProductInfo("Leopard Transaction");
        mPaymentParams.setFirstName("");
        mPaymentParams.setEmail("");
        mPaymentParams.setTxnId("" + System.currentTimeMillis());
        mPaymentParams.setSurl("https://payu.herokuapp.com/success");
        mPaymentParams.setFurl("https://payu.herokuapp.com/failure");
        mPaymentParams.setUdf1("");
        mPaymentParams.setUdf2("");
        mPaymentParams.setUdf3("");
        mPaymentParams.setUdf4("");
        mPaymentParams.setUdf5("");
        mPaymentParams.setUserCredentials("");
        //mPaymentParams.setOfferKey(inputData);
        //intent.putExtra(PayuConstants.SALT, "eCwWELxi");
        //salt = "eCwWELxi";
        payuConfig.setEnvironment(PayuConstants.MOBILE_STAGING_ENV);
        storeOneClickHash = 0;
        smsPermission = Boolean.parseBoolean("false");


        //if(null == salt)
        generateHashFromServer(mPaymentParams);
        //else {
        //    generateHashFromSDK(mPaymentParams, intent.getStringExtra(PayuConstants.SALT));
        //}

//        generateHashFromSDK(mPaymentParams, intent.getStringExtra(PayuConstants.SALT));
    }

    public void generateHashFromServer(PaymentParams mPaymentParams) {

        StringBuffer postParamsBuffer = new StringBuffer();
        postParamsBuffer.append(concatParams(PayuConstants.KEY, mPaymentParams.getKey()));
        postParamsBuffer.append(concatParams(PayuConstants.AMOUNT, mPaymentParams.getAmount()));
        postParamsBuffer.append(concatParams(PayuConstants.TXNID, mPaymentParams.getTxnId()));
        postParamsBuffer.append(concatParams(PayuConstants.EMAIL, null == mPaymentParams.getEmail() ? "" : mPaymentParams.getEmail()));
        postParamsBuffer.append(concatParams(PayuConstants.PRODUCT_INFO, mPaymentParams.getProductInfo()));
        postParamsBuffer.append(concatParams(PayuConstants.FIRST_NAME, null == mPaymentParams.getFirstName() ? "" : mPaymentParams.getFirstName()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF1, mPaymentParams.getUdf1() == null ? "" : mPaymentParams.getUdf1()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF2, mPaymentParams.getUdf2() == null ? "" : mPaymentParams.getUdf2()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF3, mPaymentParams.getUdf3() == null ? "" : mPaymentParams.getUdf3()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF4, mPaymentParams.getUdf4() == null ? "" : mPaymentParams.getUdf4()));
        postParamsBuffer.append(concatParams(PayuConstants.UDF5, mPaymentParams.getUdf5() == null ? "" : mPaymentParams.getUdf5()));
        postParamsBuffer.append(concatParams(PayuConstants.USER_CREDENTIALS, mPaymentParams.getUserCredentials() == null ? PayuConstants.DEFAULT : mPaymentParams.getUserCredentials()));

        // for offer_key
        //if(null != mPaymentParams.getOfferKey())
        //    postParamsBuffer.append(concatParams(PayuConstants.OFFER_KEY, mPaymentParams.getOfferKey()));
        // for check_isDomestic
        //if(null != cardBin)
        //    postParamsBuffer.append(concatParams("card_bin", cardBin));

        String postParams = postParamsBuffer.charAt(postParamsBuffer.length() - 1) == '&' ? postParamsBuffer.substring(0, postParamsBuffer.length() - 1).toString() : postParamsBuffer.toString();
        // make api call
        GetHashesFromServerTask getHashesFromServerTask = new GetHashesFromServerTask();
        getHashesFromServerTask.execute(postParams);
    }

    protected String concatParams(String key, String value) {
        return key + "=" + value + "&";
    }

    @Override
    public void onPaymentRelatedDetailsResponse(PayuResponse payuResponse) {
        mPayuResponse = payuResponse;
        MerchantWebService valueAddedWebService = new MerchantWebService();
        valueAddedWebService.setKey(mPaymentParams.getKey());
        valueAddedWebService.setCommand(PayuConstants.VAS_FOR_MOBILE_SDK);
        valueAddedWebService.setHash(mPayuHashes.getVasForMobileSdkHash());
        valueAddedWebService.setVar1(PayuConstants.DEFAULT);
        valueAddedWebService.setVar2(PayuConstants.DEFAULT);
        valueAddedWebService.setVar3(PayuConstants.DEFAULT);

        if ((postData = new MerchantWebServicePostParams(valueAddedWebService).getMerchantWebServicePostParams()) != null && postData.getCode() == PayuErrors.NO_ERROR) {
            payuConfig.setData(postData.getResult());
            valueAddedServiceTask = new ValueAddedServiceTask(this);
            valueAddedServiceTask.execute(payuConfig);
        } else {
            Toast.makeText(this, postData.getResult(), Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onValueAddedServiceApiResponse(PayuResponse payuResponse) {
        valueAddedResponse = payuResponse;
    }

    private void printMessage(String message) {
        prn.iFlushBuf();
        prn.iPrinterAddData(Printer.PR_FONTLARGENORMAL, message);
        prn.iPrinterAddData(Printer.PR_FONTLARGENORMAL, "\n\n\n\n" +
                "\n" +
                "\n" +
                "\n\n" +
                "\n" +
                "\n" +
                "\n\n");
        prn.iStartPrinting(1);

    }

    private void readMacCard() {
        new ReadMagcardAsyc().execute();
    }

    class GetHashesFromServerTask extends AsyncTask<String, String, PayuHashes> {

        @Override
        protected PayuHashes doInBackground(String... postParams) {
            PayuHashes payuHashes = new PayuHashes();
            try {
//                URL url = new URL(PayuConstants.MOBILE_TEST_FETCH_DATA_URL);
//                        URL url = new URL("http://10.100.81.49:80/merchant/postservice?form=2");;

                URL url = new URL("https://payu.herokuapp.com/get_hash");

                // get the payuConfig first
                String postParam = postParams[0];

                byte[] postParamsByte = postParam.getBytes("UTF-8");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Content-Length", String.valueOf(postParamsByte.length));
                conn.setDoOutput(true);
                conn.getOutputStream().write(postParamsByte);

                InputStream responseInputStream = conn.getInputStream();
                StringBuffer responseStringBuffer = new StringBuffer();
                byte[] byteContainer = new byte[1024];
                for (int i; (i = responseInputStream.read(byteContainer)) != -1; ) {
                    responseStringBuffer.append(new String(byteContainer, 0, i));
                }

                JSONObject response = new JSONObject(responseStringBuffer.toString());

                Iterator<String> payuHashIterator = response.keys();
                while (payuHashIterator.hasNext()) {
                    String key = payuHashIterator.next();
                    switch (key) {
                        case "payment_hash":
                            payuHashes.setPaymentHash(response.getString(key));
                            break;
                        case "get_merchant_ibibo_codes_hash": //
                            payuHashes.setMerchantIbiboCodesHash(response.getString(key));
                            break;
                        case "vas_for_mobile_sdk_hash":
                            payuHashes.setVasForMobileSdkHash(response.getString(key));
                            break;
                        case "payment_related_details_for_mobile_sdk_hash":
                            payuHashes.setPaymentRelatedDetailsForMobileSdkHash(response.getString(key));
                            break;
                        case "delete_user_card_hash":
                            payuHashes.setDeleteCardHash(response.getString(key));
                            break;
                        case "get_user_cards_hash":
                            payuHashes.setStoredCardsHash(response.getString(key));
                            break;
                        case "edit_user_card_hash":
                            payuHashes.setEditCardHash(response.getString(key));
                            break;
                        case "save_user_card_hash":
                            payuHashes.setSaveCardHash(response.getString(key));
                            break;
                        case "check_offer_status_hash":
                            payuHashes.setCheckOfferStatusHash(response.getString(key));
                            break;
                        case "check_isDomestic_hash":
                            payuHashes.setCheckIsDomesticHash(response.getString(key));
                            break;
                        default:
                            break;
                    }
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return payuHashes;
        }

        @Override
        protected void onPostExecute(PayuHashes payuHashes) {
            super.onPostExecute(payuHashes);
            mPayuHashes = payuHashes;
//            intent = new Intent(this, PayUBaseActivity.class);
//            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
//            intent.putExtra(PayuConstants.PAYMENT_PARAMS, mPaymentParams);
//            intent.putExtra(PayuConstants.PAYU_HASHES, payuHashes);
//            intent.putExtra(PayuConstants.SALT, salt);
//            intent.putExtra(PayuConstants.STORE_ONE_CLICK_HASH, storeOneClickHash);
//            intent.putExtra(PayuConstants.SMS_PERMISSION, smsPermission);
//            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);

            //PAYUBASEACTIVITY CODE BASE

            mPayuUtils = new PayuUtils();

            MerchantWebService merchantWebService = new MerchantWebService();
            merchantWebService.setKey(mPaymentParams.getKey());
            merchantWebService.setCommand(PayuConstants.PAYMENT_RELATED_DETAILS_FOR_MOBILE_SDK);
            merchantWebService.setVar1("default");
            merchantWebService.setHash(mPayuHashes.getPaymentRelatedDetailsForMobileSdkHash());

            // fetching for the first time.
            PostData postData = new MerchantWebServicePostParams(merchantWebService).getMerchantWebServicePostParams();
            if (postData.getCode() == PayuErrors.NO_ERROR) {
                // ok we got the post params, let make an api call to payu to fetch the payment related details
                payuConfig.setData(postData.getResult());
                GetPaymentRelatedDetailsTask paymentRelatedDetailsForMobileSdkTask = new GetPaymentRelatedDetailsTask(Payment.this);
                paymentRelatedDetailsForMobileSdkTask.execute(payuConfig);
                makePaymentByCreditCard();
            }


        }

        private void makePaymentByCreditCard() {

            mPostData = null;

            mPaymentParams.setHash(mPayuHashes.getPaymentHash());

            mPaymentParams.setStoreCard(0);
            mPaymentParams.setEnableOneClickPayment(0);
            // lets try to get the post params

            mPaymentParams.setCardNumber(cc);
            mPaymentParams.setNameOnCard(name);
            mPaymentParams.setExpiryMonth(month);
            mPaymentParams.setExpiryYear(year);
            mPaymentParams.setCvv(cvv);
            try {
                mPostData = new PaymentPostParams(mPaymentParams, PayuConstants.CC).getPaymentPostParams();
            } catch (Exception e) {
                e.printStackTrace();
            }

            payuConfig.setData(mPostData.getResult());

            Intent intent = new Intent(Payment.this, PaymentsActivity.class);
            intent.putExtra(PayuConstants.PAYU_CONFIG, payuConfig);
            intent.putExtra(PayuConstants.STORE_ONE_CLICK_HASH, storeOneClickHash);
            intent.putExtra(PayuConstants.SMS_PERMISSION, smsPermission);
            startActivityForResult(intent, PayuConstants.PAYU_REQUEST_CODE);
        }


    }

    private class StartScanConnectTask extends AsyncTask<String, String, Integer> {
        private static final int RET_SCAN_DEVICE_FINISHED = 0x0002;
        private static final int RET_SCAN_DEVICE_TIMEOUT = 0x0003;
        private static final int RET_BLUETOOTH_NOT_START = 0x0001;
        private static final int DEVICE_NOT_PAIRED = 0x0009;
        private static final int CONN_FAIL = 0x04;
        private static final int CONN_SUCCESS = 0x05;
        private static final int RET_BOND_FAIL = 0x06;
        private static final int miWAIT_TIME = 20;
        private static final int miSLEEP_TIME = 100;
        private ProgressDialog mpd = null;


        @Override
        public void onPreExecute() {
            this.mpd = new ProgressDialog(Payment.this);
            this.mpd.setMessage("Scaning Device");
            this.mpd.setCancelable(false);
            this.mpd.setCanceledOnTouchOutside(false);
            this.mpd.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            Log.d(TAG, "StartScanConnectTask doInBackground");
            Log.d(TAG, "StartScanConnectTask Starting BT");
            int iWait = miWAIT_TIME * 1000;
            /* BT isEnable */
            if (!mBtAdapter.isEnabled()) {
                mBtAdapter.enable();
                //Wait miSLEEP_TIME seconds, start the Bluetooth device before you start scanning
                while (iWait > 0) {
                    if (!mBtAdapter.isEnabled())
                        iWait -= miSLEEP_TIME;
                    else
                        break;
                    SystemClock.sleep(miSLEEP_TIME);
                }
                if (iWait <= 0)
                    return RET_BLUETOOTH_NOT_START;
            }
            Log.d(TAG, "StartScanConnectTask BT Started, Registering Receivers");
            String sMac = "";
            boolean found = false;
            Set<BluetoothDevice> devices = mBtAdapter.getBondedDevices();
            for (BluetoothDevice device : devices) {
                Log.d(TAG, "Device: " + device.getName() + ", Mac : " + device.getAddress());
                if (DEVICE_NAME.equalsIgnoreCase(device.getName())) {
                    found = true;
                    sMac = device.getAddress();
                }
            }
            if (!found || sMac == "")
                return DEVICE_NOT_PAIRED;

            Log.d(TAG, "StartScanConnectTask Closing connections if any");
            if (btcomm != null && btcomm.isConnect()) {
                // if already connected close the existing connection
                btcomm.closeConn();
            }
            Log.d(TAG, "StartScanConnectTask Initiating connection to : " + sMac);
            btcomm = new BluetoothComm(sMac);
            if (btcomm.createConn()) {
                //return CONN_SUCCESS;
                // Continue...
                Log.d(TAG, "StartScanConnectTask Successfully Connected to : " + DEVICE_NAME);
            } else {
                return CONN_FAIL;
            }
            try {
                prn = new Printer(setup, btcomm.mosOut, btcomm.misIn);
                mag = new MagCard(setup, btcomm.mosOut, btcomm.misIn);
                if (prn != null) {
                    int ix = prn.iPrinterDiagnostics();
                    Log.e(TAG, "Resp : " + sCheckRetVal(ix));
                } else {
                    Log.e(TAG, "Peripheral Not initialized");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return CONN_SUCCESS;
        }

        private String sCheckRetVal(int iRetVal) {
            if (iRetVal == Printer.PR_SUCCESS)
                return "Printer.PR_SUCCESS";
            else
                return "Val = " + iRetVal;
        }

        @Override
        public void onPostExecute(Integer result) {
            if (this.mpd.isShowing())
                this.mpd.dismiss();
            if (mBtAdapter.isDiscovering())
                mBtAdapter.cancelDiscovery();

            if (CONN_SUCCESS == result) {
                Log.e(TAG, "StartScanConnectTask Need to Continue with transaction responses.....");
                readMacCard();
            } else if (CONN_FAIL == result || RET_BOND_FAIL == result) {
                Log.e(TAG, "StartScanConnectTask We must start a RE-SCAN .....");
            } else if (DEVICE_NOT_PAIRED == result) {
                Log.e(TAG, "StartScanConnectTask " + DEVICE_NAME + " not paired .....");
            } else if (RET_BLUETOOTH_NOT_START == result) {
                Log.e(TAG, "StartScanConnectTask BT is OFF");
                Toast.makeText(Payment.this, "BT is OFF", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class ReadMagcardAsyc extends AsyncTask<Integer, Integer, Integer> {
        /* displays the progress dialog untill background task is completed*/
        ProgressDialog dlgPg;

        @Override
        protected void onPreExecute() {
            progressDialog(Payment.this, "Please swipe the card...");
            super.onPreExecute();
        }

        /* Task of ReadMagcard data performing in the background*/
        @Override
        protected Integer doInBackground(Integer... params) {
            try {
                mag.vReadMagCardData(10000);
                iRetVal = mag.iGetReturnCode();
            } catch (NullPointerException e) {
                iRetVal = -100;
                return iRetVal;
            }
            return iRetVal;
        }

        /* This sends message to handler to display the status messages
         * of Diagnose in the dialog box */
        @Override
        protected void onPostExecute(Integer result) {
            dlgPg.dismiss();
            if (iRetVal == MagCard.MAG_SUCCESS) {
                String s1 = mag.sGetTrack1Data();
                String s2 = mag.sGetTrack2Data();
                String s3 = mag.sGetTrack3Data();
                String smerge = s1 + "------" + s2 + "-----" + s3;
                Toast.makeText(Payment.this, smerge, Toast.LENGTH_LONG).show();
                printMessage(smerge);


            } else {
                Toast.makeText(Payment.this, "Could not capture the Card Data", Toast.LENGTH_LONG).show();
            }
//            if(iRetVal==DEVICE_NOTCONNECTED){
//                handler.obtainMessage(DEVICE_NOTCONNECTED,"Device not connected").sendToTarget();
//            }else if (iRetVal == MagCard.MAG_TRACK1_READERROR) {
//                handler.obtainMessage(MESSAGE_BOX,"Track1 data read failed").sendToTarget();
//            } else if (iRetVal == MagCard.MAG_TRACK2_READERROR) {
//                handler.obtainMessage(MESSAGE_BOX,"Track2 data read failed").sendToTarget();
//            } else if (iRetVal == MagCard.MAG_SUCCESS){
//                RadioBox();
//            } else if (iRetVal == MagCard.MAG_FAIL) {
//                handler.obtainMessage(MESSAGE_BOX,"MagCard Read Error").sendToTarget();
//            } else if (iRetVal == MagCard.MAG_LRC_ERROR) {
//                handler.obtainMessage(MESSAGE_BOX,"LRC Error").sendToTarget();
//            } else if (iRetVal == MagCard.MAG_NO_DATA) {
//                handler.obtainMessage(MESSAGE_BOX,"IMPROPER SWIPE").sendToTarget();
//            } else if (iRetVal == MagCard.MAG_ILLEGAL_LIBRARY) {
//                handler.obtainMessage(MESSAGE_BOX,"Illegal Library").sendToTarget();
//            } else if (iRetVal == MagCard.MAG_DEMO_VERSION) {
//                handler.obtainMessage(MESSAGE_BOX,"API not supported for demo version").sendToTarget();
//            } else if (iRetVal == MagCard.MAG_TIME_OUT) {
//                handler.obtainMessage(MESSAGE_BOX,"Swipe Card TimeOut").sendToTarget();
//            }else if (iRetVal == MagCard.MAG_INACTIVE_PERIPHERAL) {
//                handler.obtainMessage(MESSAGE_BOX,"Peripheral is inactive").sendToTarget();
//            }else if (iRetVal == MagCard.MAG_PARAM_ERROR) {
//                handler.obtainMessage(MESSAGE_BOX,"Passed incorrect parameter").sendToTarget();
//            }else if (iRetVal== Printer.PR_DEMO_VERSION) {
//                handler.obtainMessage(MESSAGE_BOX,"Library is in demo version").sendToTarget();
//            }else if (iRetVal==Printer.PR_INVALID_DEVICE_ID) {
//                handler.obtainMessage(MESSAGE_BOX,"Connected  device is not license authenticated.").sendToTarget();
//            }else if (iRetVal==Printer.PR_ILLEGAL_LIBRARY) {
//                handler.obtainMessage(MESSAGE_BOX,"Library not valid").sendToTarget();
//            }
            super.onPostExecute(result);
        }

        /* This performs Progress dialog box to show the progress of operation */
        public void progressDialog(Context context, String msg) {
            dlgPg = new ProgressDialog(context);
            dlgPg.setMessage(msg);
            dlgPg.setIndeterminate(true);
            dlgPg.setCancelable(false);
            dlgPg.show();
        }
    }
}



