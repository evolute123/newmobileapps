package com.leopardomni.leopardomni.epslomerchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Devicedetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_details);
        Button btnDeviceValidation = (Button) findViewById(R.id.btnDeviceValidation);
        Button btnChangeDevice = (Button) findViewById(R.id.btnChangeDevice);

        ImageView backButton = (ImageView) findViewById(R.id.btnBackIv);
        TextView supportTextView = (TextView) findViewById(R.id.supportTv);
        TextView callTextView = (TextView) findViewById(R.id.supportNumberTv);
        supportTextView.setText(Html.fromHtml(getString(R.string.label_support)));
        callTextView.setText(Html.fromHtml(getString(R.string.label_call)));
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnDeviceValidation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startTransaction = new Intent(view.getContext(), CurrentDevice.class);
                startActivity(startTransaction);
            }
        });

        btnChangeDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
