package com.leopardomni.leopardomni.epslomerchantapp;

        import android.content.Intent;
        import android.os.Bundle;
        import android.support.v7.app.AppCompatActivity;
        import android.view.View;
        import android.widget.Button;
        import android.widget.Toast;

public class StartTrasaction extends AppCompatActivity {

    Button cashBtn, cardBtn, cancelBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_trasaction);
        cashBtn = (Button) findViewById(R.id.cashButton);
        cardBtn = (Button) findViewById(R.id.cardButton);
        cancelBtn = (Button) findViewById(R.id.cancelButton);

        cashBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(StartTrasaction.this, "Cash Trasaction", Toast.LENGTH_SHORT).show();
            }
        });

        cardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(StartTrasaction.this, "Card Trasaction", Toast.LENGTH_SHORT).show();
                Intent cardIntent = new Intent(StartTrasaction.this, CardDetails.class);
                startActivity(cardIntent);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(StartTrasaction.this, "Trasaction Cancelled", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
