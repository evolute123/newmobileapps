package com.leopardomni.leopardomni.epslomerchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class CardDetails extends AppCompatActivity {
    Button submit, cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);
        submit = (Button) findViewById(R.id.submitButton);
        cancel = (Button) findViewById(R.id.cancelButtonInCardDetails);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(CardDetails.this, "Submitted", Toast.LENGTH_SHORT).show();
                Intent submit = new Intent(CardDetails.this, TrasactionComplete.class);
                startActivity(submit);
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(CardDetails.this, "Cancelled", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
