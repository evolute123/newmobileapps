package com.leopardomni.leopardomni.epslomerchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class CurrentDevice extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_device);
        ImageView settingImage = (ImageView) findViewById(R.id.settingsIv);

        Button btnStartTransaction = (Button) findViewById(R.id.startTransactionsButton);
        Button btnShowTransaction = (Button) findViewById(R.id.showTransationsButton);

        settingImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent settingsIntent = new Intent(view.getContext(), DeviceSettings.class);
                startActivity(settingsIntent);
            }
        });

        btnStartTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startTransaction = new Intent(CurrentDevice.this, StartTrasaction.class);
                startActivity(startTransaction);
            }
        });

        btnShowTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showTransactions = new Intent(CurrentDevice.this, ViewTransactions.class);
                startActivity(showTransactions);
            }
        });
    }
}
