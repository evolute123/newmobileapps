package com.leopardomni.leopardomni.epslomerchantapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.leopardomni.leopardomni.epslomerchantapp.payubiz.Payment;

public class Splash extends AppCompatActivity {


    private static final int SPLASH_THREAD = 0;
    /**
     * Splash Handler
     */
    Handler handlersplash = new Handler() {

        public void handleMessage(android.os.Message msg) {

            switch (msg.what) {
                case SPLASH_THREAD:
                    splashTread.start();
                    break;
                default:
                    break;
            }
        }

        ;
    };
    private boolean _active = true;
    private int _splashTime = 2000;
    /**
     * Splash thread
     */

    Thread splashTread = new Thread() {
        @Override
        public void run() {
            try {
                int waited = 0;
                while (_active && (waited < _splashTime)) {
                    sleep(100);
                    if (_active) {
                        waited += 100;

                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                startActivity(new Intent(Splash.this, Payment.class));
                finish();

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        /**
         * Message to the Handler
         */
        Message mMessage = new Message();
        mMessage.what = Splash.SPLASH_THREAD;
        Splash.this.handlersplash.sendMessage(mMessage);
    }
}

