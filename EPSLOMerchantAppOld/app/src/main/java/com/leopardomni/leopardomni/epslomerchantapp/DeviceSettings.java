package com.leopardomni.leopardomni.epslomerchantapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DeviceSettings extends AppCompatActivity {
    Button allTests;
    Button hardwareTest;
    Button printerTest;
    Button cardTest;
    Button internetTest;
    Button devicevalidationTest;
    Button changeDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_settings);
        allTests = (Button) findViewById(R.id.allTestsButton);
        hardwareTest = (Button) findViewById(R.id.allHardwareTestButton);
        printerTest = (Button) findViewById(R.id.allPrinterTestButton);
        cardTest = (Button) findViewById(R.id.allCardTestButton);
        internetTest = (Button) findViewById(R.id.internetTestButton);
        devicevalidationTest = (Button) findViewById(R.id.deviceValidationTestButton);
        changeDevice = (Button) findViewById(R.id.changeDeviceButton);
        ImageView backButton = (ImageView) findViewById(R.id.btnBackIvInDeviceSettings);
        TextView supportTextView = (TextView) findViewById(R.id.supportTv);
        TextView callTextView = (TextView) findViewById(R.id.supportNumberTv);
        supportTextView.setText(Html.fromHtml(getString(R.string.label_support)));
        callTextView.setText(Html.fromHtml(getString(R.string.label_call)));
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        allTests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeviceSettings.this, "Running All Tests", Toast.LENGTH_SHORT).show();
            }
        });

        hardwareTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeviceSettings.this, "Running Hardware Tests", Toast.LENGTH_SHORT).show();
            }
        });
        printerTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeviceSettings.this, "Running Printer Test", Toast.LENGTH_SHORT).show();
            }
        });
        cardTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeviceSettings.this, "Running card/swip Test", Toast.LENGTH_SHORT).show();
            }
        });
        devicevalidationTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeviceSettings.this, "Running Device Validation Test", Toast.LENGTH_SHORT).show();
            }
        });
        internetTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeviceSettings.this, "Running internet Tests", Toast.LENGTH_SHORT).show();
            }
        });
        changeDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DeviceSettings.this, "Changing Device", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
