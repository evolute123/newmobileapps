package com.leopardomni.leopardomni.epslomerchantapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class ViewTransactions extends AppCompatActivity {

    ImageView backButtonInTransactionList, searchButton;
    ListView list;
    Context context = null;
    TransactionListAdapter adapter;

    private int year;
    private int month;
    private int day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_list);

        backButtonInTransactionList = (ImageView) findViewById(R.id.btnBackInTransactionListIv);
        searchButton = (ImageView) findViewById(R.id.btnSearch);

        backButtonInTransactionList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ViewTransactions.this, "Searching ...", Toast.LENGTH_SHORT).show();
            }
        });
        context = getApplicationContext();


    }
}
