package com.leopardomni.leopardomni.epslomerchantapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class TransactionList extends AppCompatActivity {

    ImageView backButtonInTransactionList, searchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_list);

        backButtonInTransactionList = (ImageView) findViewById(R.id.btnBackInTransactionListIv);
        searchButton = (ImageView) findViewById(R.id.btnSearch);

        backButtonInTransactionList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(TransactionList.this, "Searching ...", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
