package com.leopardomni.leopardomni.epslomerchantapp.Server;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

/**
 * Created by akshay on 6/8/16.
 */

public class EPSLOServerCaller {

    private SSLContext sslContext;
    private int lastResponseCode;

    public EPSLOServerCaller(AssetManager assets) {
        try {
            EPSLOAuthenticator authParams = new EPSLOAuthenticator();
            authParams.setClientCertificate(Utility.getClientCertFile());
            authParams.setClientCertificatePassword(EPSLOWebURL.CLIENT_CERTIFICATE_PASSWORD);
            authParams.setCaCertificate(Utility.readCaCert(assets));

            File clientCertFile = authParams.getClientCertificate();
            if (clientCertFile != null) {
                sslContext = EPSSslContextFactory.getInstance().makeContext(clientCertFile, authParams.getClientCertificatePassword(), authParams.getCaCertificate());
                CookieHandler.setDefault(new CookieManager());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public EPSLOServerCaller() {

    }

    public int getLastResponseCode() {
        return lastResponseCode;
    }

    public String doGet(String url, HashMap<String, String> headers) throws Exception {
        String result = null;

        HttpURLConnection urlConnection = null;

        try {
            URL requestUrl = new URL(url);
            urlConnection = (HttpURLConnection) requestUrl.openConnection();

            if (urlConnection instanceof HttpURLConnection && sslContext != null) {
                ((HttpsURLConnection) urlConnection).setSSLSocketFactory(sslContext.getSocketFactory());

            }
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Content-Language", "en-US");

            if (headers != null && headers.size() > 0) {
                for (String key : headers.keySet()) {
                    urlConnection.setRequestProperty(key, headers.get(key));
                }
            }

            urlConnection.setConnectTimeout(1500);
            urlConnection.setReadTimeout(1500);
            urlConnection.setUseCaches(false);
            // urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            lastResponseCode = urlConnection.getResponseCode();
            result = Utility.readData(urlConnection.getInputStream());

        } catch (Exception ex) {

        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return result;
    }

    public String doPost(String url, HashMap<String, String> params, HashMap<String, String> headers) throws Exception {
        HttpURLConnection urlConnection = null;
        String result = null;
        String param = "";
        for (String key : params.keySet()) {
            if (param.equalsIgnoreCase(""))
                param = key + "=" + URLEncoder.encode(params.get(key), "UTF-8");
            else
                param = param + "&" + key + "=" + URLEncoder.encode(params.get(key), "UTF-8");

        }
        try {
            URL requestedUrl = new URL(url);
            urlConnection = (HttpURLConnection) requestedUrl.openConnection();
            if (urlConnection instanceof HttpsURLConnection && sslContext != null) {
                ((HttpsURLConnection) urlConnection).setSSLSocketFactory(sslContext.getSocketFactory());
            }
            urlConnection.setRequestMethod("POST");
            urlConnection.setFixedLengthStreamingMode(param.getBytes().length);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Content-Language", "en-US");

            if (headers != null && headers.size() > 0) {
                for (String key : headers.keySet()) {
                    urlConnection.setRequestProperty(key, headers.get(key));
                }
            }
            urlConnection.setConnectTimeout(1500);
            urlConnection.setReadTimeout(1500);
            urlConnection.setUseCaches(false);
            urlConnection.setDoOutput(true);

            DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
            dStream.writeBytes(param);
            dStream.flush();
            dStream.close();

            lastResponseCode = urlConnection.getResponseCode();

            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = "";
            StringBuilder responseOuput = new StringBuilder();
            while ((line = br.readLine()) != null) {
                responseOuput.append(line);
            }
            br.close();
            result = responseOuput.toString();
        } catch (Exception ex) {
            result = "";
            Log.v("EPSServerCaller.doPost", ex.getMessage());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return result;
    }
}
